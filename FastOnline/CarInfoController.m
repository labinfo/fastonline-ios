//
//  CarInfoController.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarInfoController.h"
#import "CarDataCell.h"
#import "CarMaker.h"
#import "CarModel.h"
#import "CarVersion.h"
#import "LocalizationSystem.h"
#import "AppDataManager.h"
#import "ImpostazioniManager.h"
#import "FileTextPicker.h"
#import "NewSSUPageController.h"
#import "DateFieldPicker.h"
#import "MaintenanceCell.h"
#import "Validator.h"

#define MAKER_REQUEST_ID 1
#define MODEL_REQUEST_ID 2
#define VERSION_REQUEST_ID 3


#define MATRICUL_REQUEST_ID 4
#define ARRIVAL_REQUEST_ID 5
#define REVISION_REQUEST_ID 6

#define MATRICULATION_PICKER_ID 2412

@interface CarInfoController ()

@end

@implementation CarInfoController{
    
    //Initialization data
    BOOL isInitialDataEmpty;
    
    
    Company *userCompany;
    
    
    //Car Data
    CarDataCell *carDataCell;
    NSArray* makers;
    CarMaker* selectedMaker;
    CarModel* selectedModel;
    CarVersion* selectedVersion;
    
    FileTextPicker *fuelPicker, *fuelLevelPicker;
    
    DateFieldPicker * revDatePicker, *matrDatePicker, *arrDatePicker;
    
    //Maintenance Cell
    NSMutableDictionary* maintenanceCells;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NewSSUPageController* pageController = (NewSSUPageController*) self.pageController;
    userCompany = pageController.company;
    
    makers = [[AppDataManager get] getCarMakers];
    
    maintenanceCells =[[NSMutableDictionary alloc] init];
    
    if(self.carData == nil){
        isInitialDataEmpty = true;
        self.carData = [[CarData alloc] init];
        [self.carData.maintenances addObject:[Maintenance new]];
    }
    else{
        isInitialDataEmpty = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissKeyboard];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self dismissKeyboard];
}

-(BOOL)validationOk{
    
    NSMutableString* errorMsg=[[NSMutableString alloc] init];
    
    
    if(selectedMaker==nil){
        [errorMsg appendString:AMLocalizedString(@"MissingCarMaker", @"")];
        [errorMsg appendString:@"\n"];
    }
    if(selectedModel==nil){
        [errorMsg appendString:AMLocalizedString(@"MissingCarModel", @"")];
        [errorMsg appendString:@"\n"];
    }
    /*if(selectedVersion==nil){
        [errorMsg appendString:AMLocalizedString(@"MissingCarVersion", @"")];
        [errorMsg appendString:@"\n"];
    }*/
    
    if(![Validator stringNotBlank:carDataCell.plateField.text]){
        [errorMsg appendString:AMLocalizedString(@"MissingPlate", @"")];
        [errorMsg appendString:@"\n"];
    }
    
    if(![Validator stringNotBlank:carDataCell.kmCrossedField.text]){
        [errorMsg appendString:AMLocalizedString(@"MissingKm", @"")];
        [errorMsg appendString:@"\n"];
    }
    
    /*if(matrDatePicker.selectedDate == nil){
        [errorMsg appendString:AMLocalizedString(@"MissingMatrDate", @"")];
        [errorMsg appendString:@"\n"];
    }*/
    
    if(errorMsg.length>0){
        [self.pageController showErrorAlertWithMessage:errorMsg];
        return false;
    }
    
    return true;
}

-(IBAction)selectFuel:(id)sender{
    if(fuelPicker==nil)
    fuelPicker = [[FileTextPicker alloc] initWithFileName:[[ImpostazioniManager get] getListDataFileName] dataListName:@"tipo_carburante" textField:carDataCell.fuelField containerController:self.pageController title:AMLocalizedString(@"FuelChooserTitle", @"")];
    [fuelPicker pickValue];
}

-(IBAction)selectFuelLevel:(id)sender{
    if(fuelLevelPicker==nil)
    fuelLevelPicker = [[FileTextPicker alloc] initWithFileName:[[ImpostazioniManager get] getListDataFileName] dataListName:@"fuel_quantity" textField:carDataCell.fuelLevelField containerController:self.pageController title:AMLocalizedString(@"FuelLevelChooserTitle", @"")];
    [fuelLevelPicker pickValue];
}

-(IBAction)addMaintenance:(id)sender{
    [self.carData.maintenances addObject:[Maintenance new]];
    
    [self.table reloadData];
    [UIView transitionWithView:self.table
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void) {
                        [self.table reloadData];
                    } completion:NULL];
}


#pragma mark DATA MANAGEMENT

-(id)getData{
    
    self.carData.maker = selectedMaker;
    self.carData.model = selectedModel;
    self.carData.version = selectedVersion;
    
    self.carData.plateNumber = [carDataCell.plateField.text uppercaseString];
    self.carData.fuelType = carDataCell.fuelField.text;
    self.carData.color = carDataCell.colorField.text;
    self.carData.park = carDataCell.parkField.text;
    self.carData.fuelLevel = carDataCell.fuelLevelField.text;
    
    
    self.carData.fuelLevelValue = fuelLevelPicker.selectedId * 0.25;
    
    self.carData.matriculationDate = matrDatePicker.selectedDate;
    self.carData.revisionDate = revDatePicker.selectedDate;
    self.carData.arrivalDate = arrDatePicker.selectedDate;
    
    self.carData.kmCrossed = [self getValidatedIntValueFrom: carDataCell.kmCrossedField ];
    self.carData.kw = [self getValidatedIntValueFrom: carDataCell.kwField];
    self.carData.doorsNumber = [self getValidatedIntValueFrom: carDataCell.doorsField];
    self.carData.seatsNumber = [self getValidatedIntValueFrom: carDataCell.seatsField];
    self.carData.cc = [self getValidatedIntValueFrom: carDataCell.ccField];
    
    if(userCompany.isUserAdmin){
        self.carData.userRate = (int) (carDataCell.rateView.rating + 0.5);
    }
    
    int i=0;
    for(MaintenanceCell *cell in maintenanceCells.allValues){
        
        Maintenance* maintenance = self.carData.maintenances[i];
        
        maintenance.kmCrossed = [self getValidatedIntValueFrom: cell.kmField];
        maintenance.desc = cell.descField.text;
        maintenance.date = cell.datePicker.selectedDate;
        
        
        i++;
    }
    
    
    return self.carData;
}

-(int) getValidatedIntValueFrom: (UITextField*) textField{
    
    if(textField.text.length>0)
        return [textField.text intValue];
    
    return -1;
}

-(void)loadData:(id)data{
    self.carData = data;
}

-(void) restoreData:(CarData*)carData{
    
    if(carData.matriculationDate){
        matrDatePicker = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
        matrDatePicker.selectedDate = carData.matriculationDate;
        
    }
    if(carData.revisionDate){
        revDatePicker = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
        revDatePicker.selectedDate = carData.revisionDate;
    }
    if(carData.arrivalDate){
        arrDatePicker = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
        arrDatePicker.selectedDate = carData.arrivalDate;
    }
    
    selectedMaker = carData.maker;
    selectedModel = carData.model;
    selectedVersion = carData.version;
}

#pragma mark - TABLE VIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //tableView.allowsSelection = NO;
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4 + self.carData.maintenances.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* tableCell;
    if(indexPath.row==0){
        static NSString *cellID = @"CarCell";
        carDataCell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        if(!userCompany.isUserAdmin){
            carDataCell.rateLabel.hidden = true;
            carDataCell.rateView.hidden = true;
        }
        
        //If data for SSU is existing yet, load data into cell
        if(!isInitialDataEmpty)
        {
            [carDataCell setCarData:self.carData];
            
            [self restoreData:self.carData];        }
        
        tableCell = carDataCell;
    }
    else if(indexPath.row == 1){
        tableCell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceTitle"];
        
        tableCell.backgroundColor = [UIColor clearColor];
    }
    else if(indexPath.row <= self.carData.maintenances.count + 1 ) {
        MaintenanceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceCell"];
        
        //Keep track of all maintenance rows
        [maintenanceCells setObject:cell forKey: [[NSNumber alloc] initWithInt: (int)indexPath.row]];
        
        //if(self.carData.maintenances && self.carData.maintenances.count>0)
        [cell setMaintentance:self.carData.maintenances[indexPath.row - 2]];
        
        //Cell need a reference to main controller to launch the modal view
        cell.mainController = self.pageController;
        
        tableCell = cell;
    }
    else if(indexPath.row <= self.carData.maintenances.count + 2 ) {
        tableCell = [tableView dequeueReusableCellWithIdentifier:@"AddButton"];
    }
    else{
        tableCell = [tableView dequeueReusableCellWithIdentifier:@"ContinueCell"];
        tableCell.backgroundColor = [UIColor clearColor];
    }
    
    tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return tableCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0)
    //If user is admin, must be shown also rateView
    return userCompany.isUserAdmin ? 900 : 830;
    else if(indexPath.row==1)
    return 70;
    else if(indexPath.row <= self.carData.maintenances.count + 1) //Maintenance cell
    return 155;
    else if(indexPath.row == self.carData.maintenances.count + 2) // Add button
    return 60;
    return 90; //Continue button
}
#pragma mark - VEHICLE SELECTION

-(void)selectMaker:(id)sender{
    [self showListPickerForRequestId:MAKER_REQUEST_ID title:AMLocalizedString(@"MakerChooserTitle", @"")];
}

-(void)selectModel:(id)sender{
    if(selectedMaker)
    [self showListPickerForRequestId:MODEL_REQUEST_ID title:AMLocalizedString(@"ModelChooserTitle", @"")];
    
}
-(void)selectVersion:(id)sender{
    if(selectedModel)
    [self showListPickerForRequestId:VERSION_REQUEST_ID title:AMLocalizedString(@"VersionChooserTitle", @"")];
}

-(void)showListPickerForRequestId: (int) requestId title: (NSString*)title{
    
    
    ListSelectController* listController=[self.storyboard instantiateViewControllerWithIdentifier:@"ListSelect"];
    
    listController.listDelegate=self;
    listController.listDataSource = self;
    listController.requestId = requestId;
    
    [self.pageController presentTransparentModalViewController:listController animated:true withAlpha:1.0];
    listController.title = title;
    
    
}

#pragma mark LIST DELEGATES

-(int)getListLengthForRequest:(int)requestId{
    switch (requestId) {
        
        case MAKER_REQUEST_ID:
        return (int) makers.count;
        
        case MODEL_REQUEST_ID:
        return (int) selectedMaker.models.count;
        
        case VERSION_REQUEST_ID:
        return (int) selectedModel.versions.count;
        
        default:
        return 0;
    }
}

-(NSString *)getTextAtPosition:(int)position forRequest:(int)requestId{
    switch (requestId) {
        
        case MAKER_REQUEST_ID:{
            CarMaker* maker = makers[position];
            return maker.name;
            
        }
        case MODEL_REQUEST_ID:{
            CarModel* model = selectedMaker.models[position];
            return model.name;
            
        }
        case VERSION_REQUEST_ID:{
            CarVersion* version = selectedModel.versions[position];
            return version.name;
            
        }
        default:
        return @"";
    }
    
}

-(void)didSelectItemAtPosition:(int)position forRequest:(int)requestId{
    switch (requestId) {
        
        case MAKER_REQUEST_ID:{
            selectedMaker = makers[position];
            selectedModel = nil;
            selectedVersion = nil;
            
            carDataCell.makerField.text = selectedMaker.name;
            carDataCell.modelField.text = @"";
            carDataCell.versionField.text = @"";
            break;
        }
        case MODEL_REQUEST_ID:{
            selectedModel = selectedMaker.models[position];
            selectedVersion = nil;
            
            carDataCell.modelField.text = selectedModel.name;
            carDataCell.versionField.text = @"";
            break;
        }
        case VERSION_REQUEST_ID:{
            selectedVersion = selectedModel.versions[position];
            
            carDataCell.versionField.text = selectedVersion.name;
            break;
        }
    }
}

#pragma MARK - DATE PICKING

-(IBAction)selectArrivalDate:(id)sender{
    
    if(arrDatePicker==nil)
    arrDatePicker = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
    
    [self launchDatePicker:arrDatePicker textField:carDataCell.arrivalDateField dateAndTime:YES request:ARRIVAL_REQUEST_ID];
    
}
-(IBAction)selectMatriculationDate:(id)sender{
    
    if(matrDatePicker==nil){
        matrDatePicker = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
        matrDatePicker.modalId = MATRICULATION_PICKER_ID;
        matrDatePicker.delegate = self;
    }
    
    [self launchDatePicker:matrDatePicker textField:carDataCell.matriculationDateField dateAndTime:NO request:MATRICUL_REQUEST_ID];
    
}
-(IBAction)selectRevisionDate:(id)sender{
    if(revDatePicker==nil)
    revDatePicker = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
    
    [self launchDatePicker:revDatePicker textField:carDataCell.revisionDateField dateAndTime:NO request:REVISION_REQUEST_ID];
    
    
}

-(void) launchDatePicker: (DateFieldPicker*) picker textField: (UITextField*)textField dateAndTime: (BOOL) dateAndTime request: (int) requestId{
    
    [self dismissKeyboard];
    
    if(picker.selectedDate == nil){
        picker.dateAndTime = dateAndTime;
        [picker pickDateWithTextField:textField containerController:self.pageController];
    }
    else{
        [self.pageController showActionsAlertWithMessage:AMLocalizedString(@"DateDeleteRequest", @"") request:requestId delegate:self];
    }
}

-(void)modalClosed:(TrasparentebaseController *)modalController{
    
    [super modalClosed:modalController];
    
    //View are launched fro page controller
    [self.pageController modalClosed:modalController];
    //IF matriculation date picked, check if date is > NOW()
    
    if(modalController.modalId == MATRICULATION_PICKER_ID){
        if([matrDatePicker.selectedDate timeIntervalSinceNow] > 0){
            
            [self.pageController showErrorAlertWithMessage:AMLocalizedString(@"MatrDateTooHigh", @"")];
            
            matrDatePicker.selectedDate = nil;
            carDataCell.matriculationDateField.text = @"";
        }
    }
    
}

-(void)modifyForRequest:(int)requestId{
    //Get the right picker
    DateFieldPicker* picker= requestId == MATRICUL_REQUEST_ID? matrDatePicker : requestId == REVISION_REQUEST_ID ? revDatePicker : arrDatePicker;
    UITextField* field= requestId == MATRICUL_REQUEST_ID? carDataCell.matriculationDateField : requestId == REVISION_REQUEST_ID ? carDataCell.revisionDateField : carDataCell.arrivalDateField;
    
    [picker pickDateWithTextField:field containerController:self.pageController];
    
}

-(void)deleteForRequest:(int)requestId{
    switch (requestId) {
        
        case REVISION_REQUEST_ID:
        revDatePicker.selectedDate = nil;
        carDataCell.revisionDateField.text = @"";
        break;
        
        case MATRICUL_REQUEST_ID:
        matrDatePicker.selectedDate = nil;
        carDataCell.matriculationDateField.text = @"";
        break;
        
        case ARRIVAL_REQUEST_ID:
        arrDatePicker.selectedDate = nil;
        carDataCell.arrivalDateField.text = @"";
        break;
        
        default:
        break;
    }
}

@end
