//
//  ContinueCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContinueCell : UITableViewCell

-(IBAction)pickAnterior3Q:(id)sender;

@end
