//
//  StatusCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "StatusCell.h"

@implementation StatusCell{
    Status* cellStatus;
}

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setStatus:(Status *)status{
    
    if(cellStatus!=nil)
        [cellStatus removeObserver:self forKeyPath:@"statusCode"];

    cellStatus = status;
    self.statusName.text = status.name;
    
    [status addObserver:self forKeyPath:@"statusCode" options:NSKeyValueObservingOptionNew context:nil];
    
    cellStatus = status;
    
    [self showStatus];

}

-(void)dealloc{
    [cellStatus removeObserver:self forKeyPath:@"statusCode"];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    [self showStatus];
}

-(void) showStatus{
    self.statusImage.hidden = false;

    switch (cellStatus.statusCode) {
        case STATUS_GOOD:
            self.statusImage.image = [UIImage imageNamed:@"stato-buono.png"];
            break;
        case STATUS_MEDIUM:
            self.statusImage.image = [UIImage imageNamed:@"stato-medio.png"];
            break;
        case STATUS_BAD:
            self.statusImage.image = [UIImage imageNamed:@"stato-scarso.png"];
            break;
            
        default:
            self.statusImage.hidden = true;
            break;
    }
}
@end
