//
//  SettingsController.h
//  FastOnline
//
//  Created by Nico Sordoni on 17/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContainerController.h"

@interface SettingsController : BaseController<UIPageViewControllerDataSource, UIPageViewControllerDelegate>


@property (strong, nonatomic) IBOutlet UIView *menu_cointainer;
@property (strong, nonatomic) IBOutlet UIPageViewController *pageViewController;
@property (nonatomic, strong) IBOutlet UIButton *startButton;

@property (strong, nonatomic) IBOutlet PageMenuIcon *loginIcon;
@property (strong, nonatomic) IBOutlet PageMenuIcon *userIcon;
@property (strong, nonatomic) IBOutlet PageMenuIcon *appIcon;
@end
