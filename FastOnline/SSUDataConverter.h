//
//  SSUDataConverter.h
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSUDetail.h"

@interface SSUDataConverter : NSObject

@property (nonatomic, strong)  SSUDetail *SSU;
@property (nonatomic, strong)  NSMutableDictionary *imagesQueue;

-(NSDictionary*) getSSUDataDictionary;
@end
