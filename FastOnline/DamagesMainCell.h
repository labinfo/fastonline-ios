//
//  DamagesMainCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DamagePickerDelegate <NSObject>

-(void) loadCompleted;

@required
-(void) damagePickedWithId: (NSString*) damageId;

@end

@interface DamagesMainCell : UITableViewCell<UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UILabel *cellTitle;
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic) id<DamagePickerDelegate> delegate;


@end
