//
//  DraftsController.m
//  FastOnline
//
//  Created by Nico Sordoni on 07/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DraftsController.h"
#import "AppDataManager.h"
#import "NSBoolean.h"
#import "NewSSUPageController.h"
#import "LeftMenuController.h"
#import "ShowSSUController.h"
#import "LocalizationSystem.h"
#import "SynchronizationHelper.h"
#import "SSUDataConverter.h"
#import "StorageManager.h"
#import "Validator.h"
#import "LIRequestComm.h"
#import "Config.h"
#import "Device.h"

#define REQUEST_DELETE_SELECTED 1000

#define REMOVED_FROM_LIST_ID 101
#define SYNC_ERROR_ID 1012
#define UNINTERESTING_ID 1011



@interface DraftsController ()

@end

@implementation DraftsController{
    NSArray *SSUDetails;
    
    NSMutableArray *filteredDetails;
    //NSMutableArray *selectedDetails;
    
    SSUSynchronizer *synchronizer;
    NSMutableArray* detailsToSynchronize;
    int detailsToSynchronizeCount;
    int failedCount, receivedCount;
    
    DraftCell* openedCell;
    //List used to notify the selection change to cells
    NSMutableArray *selectedBoolList;
    BOOL doingBackup;
    
    //Params which allow to use controller as container for bot drafts and completed SSU
    int actionsHeight;
    int menuMargin;
    
    
}

-(void)dealloc{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(DROPBOX_SHARED_ENABLED){
    /*[[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxFileProgressNotification:)
                                                 name:GSDropboxUploaderDidGetProgressUpdateNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxUploadDidStartNotification:)
                                                 name:GSDropboxUploaderDidStartUploadingFileNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxUploadDidFinishNotification:)
                                                 name:GSDropboxUploaderDidFinishUploadingFileNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxUploadDidFailNotification:)
                                                 name:GSDropboxUploaderDidFailNotification
                                               object:nil];*/
    }
    
    self.messageView.editable = NO;
    self.messageView.dataDetectorTypes = UIDataDetectorTypeAll;
    
    //selectedDetails = [NSMutableArray new];
    openedCell = nil;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = @"";
    
    
    if(self.isArchive){
        [self initAsArchiveController];
    }
    else{
        [self initAsDraftController];
    }
    [self initSSUData];
    
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
    //Enable left menu (may have been disabled from push)
    [LeftMenuController shared].disabled=false;
    
    [self initSSUData];
    [self.table reloadData];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [LeftMenuController shared].disabled=true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)activityDidDismiss{
    /*
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxFileProgressNotification:)
                                                 name:GSDropboxUploaderDidGetProgressUpdateNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxUploadDidStartNotification:)
                                                 name:GSDropboxUploaderDidStartUploadingFileNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxUploadDidFinishNotification:)
                                                 name:GSDropboxUploaderDidFinishUploadingFileNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDropboxUploadDidFailNotification:)
                                                 name:GSDropboxUploaderDidFailNotification
                                               object:nil];*/
}

-(void) initSSUData{
    if(self.isArchive)
        SSUDetails = [[AppDataManager get] getCompletedSSUList];
    else
        SSUDetails = [[AppDataManager get] getSSUDetailsList];
    
    SSUDetails = [SSUDetails sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        SSUDetail *first = (SSUDetail*)a;
        SSUDetail *second = (SSUDetail*)b;
        return [second.localID compare:first.localID];
    }];
    
    filteredDetails = [NSMutableArray arrayWithArray:SSUDetails];
    
    //selectedDetails = [NSMutableArray new];
    selectedBoolList = [NSMutableArray new];
    
    for(int i = 0 ; i < SSUDetails.count; i++){
        NSBoolean *boolean = [NSBoolean new];
        boolean.boolValue = NO;
        [selectedBoolList addObject:boolean];
    }
}

-(void)CellOpened:(DraftCell *)cell{
    if(openedCell != nil && openedCell != cell)
    {
        [openedCell closeMenu: nil];
    }
    
    
    openedCell = cell;
}

#pragma mark - DRAFTS/COMPLETED INITIALIZATION

-(void)initAsDraftController{
    
    actionsHeight = 50;
    menuMargin = -50;
    
    self.topViewArchive.hidden = YES;
    self.topViewDrafts.hidden = NO;
}

-(void)initAsArchiveController{
    
    actionsHeight = 150;
    menuMargin = 0;
    
    self.topViewArchive.hidden = NO;
    self.topViewDrafts.hidden = YES;
}

#pragma mark - ACTIONS

-(void)openActions:(id)sender{
    if(self.actionsView.hidden){
        self.actionsView.hidden = NO;
        self.supportViewHeight.constant = actionsHeight;
        self.actionsViewHeight.constant = actionsHeight;
        self.searchBarView.hidden = true;
    }
    else{
        self.actionsView.hidden=YES;
        self.supportViewHeight.constant = 0;
    }
    [UIView animateWithDuration:0.1
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

-(void)selectAll:(id)sender{
    
    //For
    BOOL newBoolValue = NO;
    
    //If all items are selected, deselect them
    for(NSBoolean *boolean in selectedBoolList){
        if(!boolean.boolValue)
            newBoolValue = YES;
            
    }
    
    
    for(NSBoolean *boolean in selectedBoolList){
        boolean.boolValue = newBoolValue;
    }
    
    
}

-(void)detail:(SSUDetail *)detail selected:(BOOL)selected{
    int index =(int)[filteredDetails indexOfObject:detail];
    NSBoolean *boolean =selectedBoolList[index];
    if(selected && !boolean.boolValue)
        boolean.boolValue = YES;
    else if(!selected && boolean.boolValue)
        boolean.boolValue = NO;
    /*if(selected && ![selectedDetails containsObject:detail])
        [selectedDetails addObject:detail];
    else if(!selected)
        [selectedDetails removeObject:detail];*/
    
}

#pragma mark SEARCH

-(void)startSearch:(id)sender{
    if(self.searchBarView.hidden){
        self.searchBarView.hidden = NO;
        self.supportViewHeight.constant = 40;
        self.actionsView.hidden = true;
    }
    else{
        self.searchBarView.hidden=YES;
        self.supportViewHeight.constant = 0;
        self.searchBar.text = @"";
        [self.searchBar resignFirstResponder];
        [self.table reloadData];
    }
    [UIView animateWithDuration:0.1
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}


#pragma mark DELETE

-(void)deleteSSU:(SSUDetail *)detail{
    
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    
    [alertController askConfirmForRequest:(int)[filteredDetails indexOfObject:detail] delegate:self];
    
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    alertController.messageLabel.text = AMLocalizedString(@"SSUDeleteConfirmMessage", @"");
    
    
}

-(void)deleteSelected:(id)sender{
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    
    [alertController askConfirmForRequest:REQUEST_DELETE_SELECTED delegate:self];
    
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    alertController.messageLabel.text = AMLocalizedString(@"SSUDeleteConfirmMessage", @"");
    
}

-(void)requestConfirmed:(int)requestId{
    //Delete selected SSUs
    if(requestId == REQUEST_DELETE_SELECTED){
        NSMutableArray* selectedList = [self selectedList];
        if(self.isArchive)
            [[AppDataManager get] removeAllCompletedSSUIn:selectedList];
        else
            [[AppDataManager get] removeAllSSUInList:selectedList];
        
    }
    //Otherwise delete only clicked one (request id is the index of SSU)
    else{
        
        SSUDetail * detail = [filteredDetails objectAtIndex:requestId];
        
        if(self.isArchive)
            [[AppDataManager get] removeCompletedSSU:detail];
        else
            [[AppDataManager get] removeSSU:detail];
        
    }
    
    [self initSSUData];
    [self.table reloadData];
}


#pragma mark CREATE/CHANGE

-(void)modifySSU:(SSUDetail *)detail{
    
    NewSSUPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SSUCreationVC"];
    
    //Prevent from opening left menu
    [LeftMenuController shared].disabled=true;
    
    
    controller.detail = detail;
    
    if(self.isArchive) {
        [[AppDataManager get] modifyCompletedSSU:detail];
    }
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)newSSU:(id)sender{
    
    NewSSUPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SSUCreationVC"];
    
    //Prevent from opening left menu
    [LeftMenuController shared].disabled=true;
    
    
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark SYNCHRONIZE

-(void)syncSSU:(SSUDetail *)detail{
    
    failedCount = receivedCount = 0;
    
    if(detail.status == STATUS_SENT){
        [self showErrorAlertWithMessage:AMLocalizedString(@"SsuAlreadySynchronized", @"")];
    }
    else{
        [self showProgressWithMessage:[NSString stringWithFormat:AMLocalizedString(@"SendingSSU", @""),1,1] detailText:@"" callbackOnTap:nil];
        
        //Sync only first element
        [self doSync:detail];
    }
    
}

-(NSMutableArray*) selectedList{
    NSMutableArray* list = [NSMutableArray new];
    
    for(int i = 0 ; i < filteredDetails.count ; i++){
        NSBoolean* boolean =selectedBoolList[i];
        if(boolean.boolValue){
            [list addObject:filteredDetails[i]];
        }
    }
    
    return list;
}

-(void)syncSelected:(id)sender{
    
    NSMutableArray* selectedList = [self selectedList];
    if(selectedList.count > 0){
        
        //Check if already synchronized SSUs are selected
        BOOL alreadySyncExists = NO;
        
        for(int i = 0; i < selectedList.count && !alreadySyncExists ; i++){
            
            SSUDetail* ssu = selectedList[i];
            
            if(ssu.status == STATUS_SENT)
                alreadySyncExists = YES;
        }
        
        if(alreadySyncExists)
        {
            [self showErrorAlertWithMessage:AMLocalizedString(@"AlreadySyncInList", @"") alertId:REMOVED_FROM_LIST_ID];
        }
        else
        {
            [self doListSync:selectedList];
        }
        
    }
    
}

-(void)modalClosed:(TrasparentebaseController *)modalController{
    [super modalClosed:modalController];
    
    //In this case, all already synchronized ssu must be removed from synchronization list
    if(modalController.modalId == REMOVED_FROM_LIST_ID){
        
        NSMutableArray* selectedList = [self selectedList];
        //Store in a mutable array all unsent ssu
        NSMutableArray* toSync = [NSMutableArray new];
        
        for(int i = 0; i < selectedList.count; i++){
            
            SSUDetail* ssu = selectedList[i];
            
            if(ssu.status != STATUS_SENT)
                [toSync addObject:ssu];
        }
        
        //Send all not sent yet SSU
        [self doListSync:toSync];
        
    }
    else if (modalController.modalId == SYNC_COMPLETED_ALERT_ID){
        [self.table reloadData];
    }
    else if(modalController.modalId == SYNC_ERROR_ID){
        [self hideProgress];
    }
    
}

-(void) doListSync:(NSMutableArray*) SSUsToSync{
    
    if(SSUsToSync.count > 0){
        
        failedCount = receivedCount = 0;
        
        detailsToSynchronizeCount = (int)SSUsToSync.count;
        
        [self showProgressWithMessage:[NSString stringWithFormat:AMLocalizedString(@"SendingSSU", @""), 1, detailsToSynchronizeCount] detailText:@"" callbackOnTap:nil];
        
        detailsToSynchronize = [NSMutableArray new];
        
        for(int i = 1; i < SSUsToSync.count; i++)
            [detailsToSynchronize addObject:SSUsToSync[i]];
        
        
        
        [self doSync:SSUsToSync[0]];
    }
}

-(void)synchronizationAbortedForSSU:(SSUDetail *)SSUdetail{
    
}


-(void) stopSync{
    
    if(synchronizer)
        [synchronizer stopSync];
    
    detailsToSynchronize = nil;
    
    [self hideProgress];
}

-(void)syncronizationCompletedForSSU:(SSUDetail *)SSUdetail{
    receivedCount++;
    [self synchronizationFinishedForSSU:SSUdetail];
    
}

-(void)syncronizationFailedForSSU:(SSUDetail *)SSUdetail{
    failedCount++;
    detailsToSynchronize = nil;
    [self hideProgress];
    [self synchronizationFinishedForSSU:SSUdetail];
}

-(void)syncronizationFailedForSSU:(SSUDetail *)SSUdetail withMessage:(NSString *)message{
    failedCount++;
    [self showErrorAlertWithMessage:message alertId:SYNC_ERROR_ID];
}

-(void) synchronizationFinishedForSSU:(SSUDetail*) SSUdetail{
    if(detailsToSynchronize.count>0){
        
        //If exists, get next SSU to synchronize
        SSUDetail *nextSSU = [detailsToSynchronize objectAtIndex:0];
        [detailsToSynchronize removeObjectAtIndex:0];
        
        NSString* message = [NSString stringWithFormat:AMLocalizedString(@"SendingSSU", @""), detailsToSynchronizeCount - detailsToSynchronize.count, detailsToSynchronizeCount];
        [self changeProgressMessage:message andDetailText:nil];
        
        [self doSync:nextSSU];
    }
    else{
        
        detailsToSynchronize = nil;
        [self hideProgress];
        
        [SynchronizationHelper showSynchronizationAlertWithCompleted:receivedCount andFailed:failedCount inController:self];
        
    }
}
//Method which only instantiate synchronizer and send data
-(void) doSync:(SSUDetail *) detail{
    
    synchronizer = [[SSUSynchronizer alloc] initWithSSU:detail];
    synchronizer.delegate = self;
    [synchronizer synchronizeSSU];
}

#pragma mark BACKUP

-(void)backupSelected:(id)sender{
    NSMutableArray* selectedList = [self selectedList];
    if(selectedList.count>0){
        [self showBackupConfirmView];
        
    }
    else{
        [self showErrorAlertWithMessage:AMLocalizedString(@"NoSSUSelected", @"")];
    }
}

-(void) backupSelectedConfirmed{
    
    [self showProgressWithMessage:AMLocalizedString(@"WaitForBackup", @"")];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableArray* selectedList = [self selectedList];
        NSMutableArray* dataList = [NSMutableArray new];
        if(selectedList.count > 0){
            
            
            for(int i = 0; i < selectedList.count; i++){
                
                SSUDataConverter *dataConverter = [SSUDataConverter new];
                dataConverter.SSU = selectedList[i];
                id ssuData = [dataConverter getSSUDataDictionary];
                
                NSError* error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ssuData
                                                                   options:0 // Pass 0 if you don't care about the readability of the generated string
                                                                     error:&error];
                NSString *jsonString;
                if (error || ! jsonData) {
                    NSLog(@"Got an error while parsing ssu data to json: %@", error);
                } else {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    
                    NSString *fileName =dataConverter.SSU.serverID == nil   ? [NSString stringWithFormat:@"l%@.json",dataConverter.SSU.localID]
                    : [NSString stringWithFormat:@"s%@.json",dataConverter.SSU.serverID];
                    //[StorageManager storeToFile: fileName object:jsonString];
                    
                    
                    NSString* filePath = [StorageManager absoluteFilePath:fileName];
                    [jsonString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
                    
                    if(error){
                        NSLog(@"Got an error while writing ssu json data to file: %@", error);
                        return;
                    }
                    
                    //Add JSON file to the list
                    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
                    [dataList addObject:fileUrl];
                    
                    jsonData = nil;
                    jsonString = nil;
                    filePath = nil;
                    fileUrl = nil;
                    
                }
                
                for(NSString *imageFileName in [dataConverter.imagesQueue  allKeys]){
                    
                    UIImage* image = [dataConverter.imagesQueue objectForKey:imageFileName];
                    
                    NSString* imageFilePath = [StorageManager absoluteFilePath:imageFileName];
                    [UIImageJPEGRepresentation(image, 0.5) writeToFile:imageFilePath atomically:YES];
                    
                    
                    //Add image to the list
                    NSURL *imageFileUrl = [NSURL fileURLWithPath:imageFilePath];
                    [dataList addObject:imageFileUrl];
                    image = nil;
                    imageFilePath = nil;
                    imageFileUrl = nil;
                }
                
                dataConverter.imagesQueue = nil;
                dataConverter.SSU = nil;
                dataConverter = nil;
            }
            
            if(dataList.count > 0){
                NSArray *applicationActivities;
                if(DROPBOX_SHARED_ENABLED) {
                    //GSDropboxActivity* activity = [[GSDropboxActivity alloc] init];
                    //activity.delegate = self;
                    //applicationActivities = @[activity];
                    //doingBackup = NO;
                }
                else{
                    applicationActivities = nil;
                }
                
                UIActivityViewController* activityViewController =
                [[UIActivityViewController alloc] initWithActivityItems:dataList
                                                  applicationActivities:applicationActivities];
                
                if(IS_IPAD){
                    activityViewController.popoverPresentationController.sourceView = self.view;
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideProgress];
                    [self presentViewController:activityViewController animated:YES completion:nil];
                });
                
            }
            dataList = nil;
            selectedList = nil;
        }
    });
}

-(void)backupAborted:(id)sender{
    [self hideBackupConfirmView];
}

-(void) backupLogin:(id)sender{
    NSString* user = BACKUP_USER_EMAIL;
    NSString* pwd = self.pwdField.text;
    

    if([Validator stringNotBlank:pwd]){
        
        
        LIRequestComm *request = [[LIRequestComm alloc] init];
        
        
        [self showProgressWithMessage:AMLocalizedString(@"LoginProgressMessage", @"")];
        
        [request setSuccess:^(id data){
            [self hideBackupConfirmView];
            [self hideProgress];
            [self backupSelectedConfirmed];
        }];
        
        [request setFailure:^(id responseObject){
            [self hideProgress];
            [self dismissKeyboard];
            [self showErrorAlertWithMessage:AMLocalizedString(@"InvalidPassword", @"")];
            
        }];
        
        NSDictionary* dict =[NSDictionary dictionaryWithObjectsAndKeys:user,@"username",pwd,@"password", nil];
        [request post: LOGIN_URL params:dict HTTPheaders:nil jsonBody:YES];
        
    }
    else{
        [self showErrorAlertWithMessage:AMLocalizedString(@"EmptyPassword", @"")];
        [self dismissKeyboard];
    }
}


-(void) showBackupConfirmView{
    
    self.backupConfirmView.hidden = NO;
    [self.backupConfirmView setAlpha:0.0f];
    
    //fade in
    [UIView animateWithDuration:0.2f animations:^{
        
        [self.backupConfirmView  setAlpha:1.0f];
        
    } completion:nil];
    [self setAdaptToKeyboard:YES];
}
-(void) hideBackupConfirmView{
    
    [self dismissKeyboard];
    [self.backupConfirmView setAlpha:1.0f];
    
    //fade in
    [UIView animateWithDuration:0.2f animations:^{
        
        [self.backupConfirmView  setAlpha:0.0f];
        
    } completion:nil];
    [self setAdaptToKeyboard:NO];
}

/*
- (void)handleDropboxFileProgressNotification:(NSNotification *)notification
{
    NSURL *fileURL = notification.userInfo[GSDropboxUploaderFileURLKey];
    float progress = [notification.userInfo[GSDropboxUploaderProgressKey] floatValue];
    NSLog(@"Upload of %@ now at %.0f%%", fileURL.absoluteString, progress * 100);
    
}

- (void)handleDropboxUploadDidStartNotification:(NSNotification *)notification
{
    NSURL *fileURL = notification.userInfo[GSDropboxUploaderFileURLKey];
    NSLog(@"Started uploading %@", fileURL.absoluteString);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if(!doingBackup){
        doingBackup = YES;
    [self showProgressWithMessage:AMLocalizedString(@"WaitForBackup", @"")];
    }
}

- (void)handleDropboxUploadDidFinishNotification:(NSNotification *)notification
{
    NSURL *fileURL = notification.userInfo[GSDropboxUploaderFileURLKey];
    NSLog(@"Finished uploading %@", fileURL.absoluteString);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideProgress];
    
}

- (void)handleDropboxUploadDidFailNotification:(NSNotification *)notification
{
    NSURL *fileURL = notification.userInfo[GSDropboxUploaderFileURLKey];
    NSLog(@"Failed to upload %@", fileURL.absoluteString);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideProgress];
    [self showErrorAlertWithMessage:AMLocalizedString(@"BackupFailed", @"")];
}
*/
#pragma mark KEYBOARD EVENTS

-(void)setAdaptToKeyboard:(BOOL)adaptToKeyboard{
    if(adaptToKeyboard){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
    }
    else{
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.confirmBottomConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    CGFloat height = keyboardFrame.size.height;
    
    self.confirmBottomConstraint.constant = height - 60;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}
#pragma mark - TABLE

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return filteredDetails.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DraftCell* cell = [tableView dequeueReusableCellWithIdentifier:@"DraftCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.SSUdetail = filteredDetails[indexPath.row];
    cell.delegate = self;
    cell.menuMarginValue = menuMargin;
    cell.isSelected = selectedBoolList[indexPath.row];
    
    //Only in archive the circle image (indicating the synchronization state) must be shown
    cell.circleImage.hidden =  self.isArchive ? NO : NO;
    
    
    
    //All cell are closed when they appear
    [cell closeMenu:nil];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ShowSSUController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowSSUVC"];
    controller.detail = [filteredDetails objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark FILTER

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if(searchText.length>0){
        filteredDetails = [[NSMutableArray alloc] init];
        
        for(SSUDetail* detail in SSUDetails){
            if([self detail:detail passTextFilter:searchText])
                [filteredDetails addObject:detail];
        }
    }
    else{
        filteredDetails = [NSMutableArray arrayWithArray: SSUDetails];
    }
    
    [self.table reloadData];
    
    
}

-(BOOL) detail:(SSUDetail*) detail passTextFilter: (NSString*) searchText{
    if(detail.title && [detail.title rangeOfString:searchText options:NSCaseInsensitiveSearch].location!=NSNotFound)
        return YES;
    if(detail.carMakerName && [detail.carMakerName rangeOfString:searchText options:NSCaseInsensitiveSearch].location!=NSNotFound)
        return YES;
    if(detail.carModelName && [detail.carModelName rangeOfString:searchText options:NSCaseInsensitiveSearch].location!=NSNotFound)
        return YES;
    if(detail.carVersionName && [detail.carVersionName rangeOfString:searchText options:NSCaseInsensitiveSearch].location!=NSNotFound)
        return YES;
    
    return NO;
}


@end
