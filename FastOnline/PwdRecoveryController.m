//
//  PwdRecoveryController.m
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "PwdRecoveryController.h"
#import "ColorUtil.h"
#import "ImpostazioniManager.h"
#import "Validator.h"
#import "LIRequestComm.h"
#import "AlertController.h"
#import "Validator.h"
#import "LocalizationSystem.h"

@interface PwdRecoveryController ()

@end

@implementation PwdRecoveryController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setFastOnlineNavBar];
    //Init button style
    self.loginButton.layer.borderWidth= 2;
    self.loginButton.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.loginButton.layer.cornerRadius = 5;
    self.loginButton.backgroundColor = [[ImpostazioniManager get] getAppColor];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self dismissKeyboard];
}

-(IBAction)recovery:(id)sender{
    [self dismissKeyboard];
    NSString* email = self.emailInput.text;
    
    if([Validator isValidEmail:email]){
        
        LIRequestComm *request = [[LIRequestComm alloc] init];
        //Needed to use request inside block
        
        __weak BaseController* weakSelf = self;
        [self showProgressWithMessage:AMLocalizedString(@"RecoveryProgressMessage", @"")];
        
        [request setSuccess:^(id data){
            [self hideProgress];
            
            AlertController* alertController=[weakSelf.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
            self.transparentModalViewController = alertController;
            [weakSelf presentTransparentModalViewController:self.transparentModalViewController animated:true withAlpha:1.0];
            alertController.messageLabel.text = [NSString stringWithFormat: AMLocalizedString(@"RecoveryOkMessage", @""), email];
        }];
        
        [request setFailure:^(id responseObject){
            NSString* errorMessage = [responseObject objectForKey:@"message"];
            if(![Validator stringNotBlank:errorMessage]){
                errorMessage = AMLocalizedString(@"BaseRequestErrorTitle", @"");
            }
            
            [self showErrorAlertWithMessage:errorMessage];
            
            [self hideProgress];
        }];
        
        [request post: [NSString stringWithFormat:@"user/%@/passwordresets",email] params:nil];
        
    }
    else{
        [self showErrorAlertWithMessage:AMLocalizedString(@"ErrorEmptyInput", @"")];
    }
    
}

@end
