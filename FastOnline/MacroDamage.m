//
//  MacroDamage.m
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "MacroDamage.h"

@implementation MacroDamage

-(id)initWithData:(id)data{
    self = [super init];
    
    if(self){
        self->_damageId = [[data objectForKey:@"id"] stringValue];
        self->_name = [data objectForKey:@"label"];
        self->_subDamages = [NSMutableArray new];
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    self =  [super init];
    if (self) {
        self->_damageId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_subDamages = [decoder decodeObjectForKey:@"sub"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self->_damageId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_subDamages forKey:@"sub"];
    
}

-(void)addSubDamage:(DamageDetail *)detail{
    [self->_subDamages addObject:detail];
}
@end
