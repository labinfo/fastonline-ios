//
//  StatusController.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "StatusController.h"
#import "Status.h"
#import "ImpostazioniManager.h"
#import "OptionalCell.h"
#import "StatusCell.h"
#import "LocalizationSystem.h"

@interface StatusController ()

@end

@implementation StatusController{
    NSMutableArray* statusList, *optionals;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
}


-(void) loadData{
    
    //If status != nil, data has been loaded yet from disk
    if(statusList == nil){
        statusList = [NSMutableArray new];
        
        NSString* fileName = [[ImpostazioniManager get] getListDataFileName];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
        NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
        
        
        id allOptionals = [[NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil] objectForKey:@"stato_vettura_check"];
        
        optionals=[[NSMutableArray alloc] init];
        for (id value in allOptionals) {
            Optional * status = [[Optional alloc] initWithName:value];
            [optionals addObject : status];
        }
        
        id allStatus = [[NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil] objectForKey:@"stato_vettura"];
        
        statusList=[[NSMutableArray alloc] init];
        for (id value in allStatus) {
            Status * status = [[Status alloc] initWithName:value];
            [statusList addObject : status];
        }
    }
}

#pragma mark - DATA LOAD/STORE

-(id)getData{
    NSMutableArray* data = [NSMutableArray new];
    
    [data addObject:statusList];
    [data addObject:optionals];
    
    return data;
}

-(void)loadData:(id)data{
    if(data!=nil){
        statusList = [data objectAtIndex:0];
        optionals = [data objectAtIndex:1];
    }
}

#pragma mark - STATUS PICKER

-(void) pickStatusForItemAt: (int) index{
    
    StatusPickerController *picker = [self.storyboard instantiateViewControllerWithIdentifier:@"StatusPickerVC"];
    
    picker.pickerDelegate = self;
    picker.requestId = index;
    
    [self.pageController presentTransparentModalViewController:picker animated:YES withAlpha:1];
}

-(void)statusPicked:(int)statusCode forId:(int)requestId{
    
    //The id of request is equal to the position of the relative status in the array
    Status * status = statusList[requestId];
    
    status.statusCode = statusCode;
}

#pragma mark - ADD NEW ITEMS

-(void)addItem:(id)sender{
    AddOptionalAlertController *addController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddOptionalVC"];
    
    addController.optionalDelegate = self;
    
    [self.pageController presentTransparentModalViewController:addController animated:YES withAlpha:1];
    addController.titleLabel.text = AMLocalizedString(@"AddStatusTitle", @"");
}

//User created a new optional
-(void)optionalSelected:(NSString *)name{
    
    Status * status = [[Status alloc] initWithName:name];
    
    [statusList addObject : status];
    
    
    [self.table reloadData];
}

#pragma mark - TABLE VIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //Header + 2 optionals + status list + Add button + Legend / Continue button
    return 5 + statusList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Header
    if(indexPath.row==0){
        
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableCell.backgroundColor = [UIColor clearColor];
        
        return tableCell;
        
    }
    //Optionals
    else if(indexPath.row <= optionals.count){
        
        OptionalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OptionalCell"];
        cell.isViewMode = self.isViewMode;
        [cell setOptional:optionals[indexPath.row - 1]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    //Status
    else if(indexPath.row <= statusList.count + optionals.count)
    {
        
        StatusCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatusCell"];
        [cell setStatus:statusList[indexPath.row - optionals.count - 1]];cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else if(indexPath.row == statusList.count + optionals.count + 1){
        
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"AddButton"];
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.isViewMode) {
            tableCell.hidden = YES;
        }
        return tableCell;
    }
    else{
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"ContinueCell"];
        tableCell.backgroundColor = [UIColor clearColor];
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return tableCell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //Header
    if(indexPath.row==0){
        return 70;
    }
    //Optionals
    else if(indexPath.row <= optionals.count){
        return 44;
    }
    //Status
    else if(indexPath.row <= statusList.count + optionals.count)
    {
        return 44;
    }
    else if(indexPath.row == statusList.count + optionals.count + 1){
        if(self.isViewMode)
            return 0;
        return 80;
    }
    else{
        if(self.isViewMode)
            return 100;
        return 180;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!self.isViewMode)
    {
        if(indexPath.row >optionals.count && indexPath.row <= statusList.count + optionals.count)
            [self pickStatusForItemAt:indexPath.row - optionals.count - 1];
    }
}

@end
