//
//  DamageFormController.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BaseController.h"
#import "MacroDamage.h"
#import "ListSelectController.h"
#import "Damage.h"
#import "CameraControllerInvoker.h"

@protocol DamageNotifiedDelegate <NSObject>

-(void) damageNotified: (Damage*) damage;

@end

@interface DamageFormController : BaseController<UITableViewDelegate, UITableViewDataSource, ListDataSource, ListDelegate, CameraInvokerDelegate>

@property (nonatomic, strong) MacroDamage *macro;
@property (nonatomic, strong) Damage *damage;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) id<DamageNotifiedDelegate> damageDelegate;
@property (nonatomic) BOOL isViewMode;


-(IBAction)pickPosition:(id)sender;
-(IBAction)pickGravity:(id)sender;
-(IBAction)pickType:(id)sender;
-(IBAction)notesEditingEnd:(id)sender;
-(IBAction)addPicture:(id)sender;

-(IBAction)confirm:(id)sender;

@end
