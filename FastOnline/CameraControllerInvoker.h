//
//  CameraControllerInvoker.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCameraController.h"
#import "CropperController.h"

@protocol CameraInvokerDelegate<NSObject>

-(void) onPictureEdited:(UIImage*) image forId:(int)requestId;
-(void) onEditAbortedforId: (int) requestId;

@end

@interface CameraControllerInvoker : NSObject<CustomCameraDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropperDelegate>

@property (strong, nonatomic)  UIViewController *controller;
@property (nonatomic)  id<CameraInvokerDelegate> delegate;


-(void) takePictureForRequest: (int) requestId;
-(void) clear;

@end
