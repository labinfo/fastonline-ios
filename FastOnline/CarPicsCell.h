//
//  CarPicsCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 27/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FOCarImageContainer.h"


@interface CarPicsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet FOCarImageContainer  *carImage1, *carImage2, *carImage3, *carImage4,
                                                            *carImage5, *carImage6, *carImage7, *carImage8;

@end
