//
//  BaseController.h
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrasparentebaseController.h"
#import "AlertController.h"



@interface BaseController : UIViewController<ModalControlDelegate>

@property (strong,nonatomic) TrasparentebaseController * transparentModalViewController;

-(void) presentTransparentModalViewController: (TrasparentebaseController *) aViewController
                                     animated: (BOOL) isAnimated
                                    withAlpha: (CGFloat) anAlpha;


-(void) dismissKeyboard;
-(void) showMessage: (NSString*)message withTitle:(NSString*)title;

-(void) showProgressWithMessage: (NSString*) message detailText:(NSString*) detail callbackOnTap:(void (^)(void)) callback;
-(void) showProgressWithMessage: (NSString*) message;
-(void) changeProgressMessage:(NSString*) message andDetailText:(NSString*) detailText;
-(void) hideProgress;

-(void)setFastOnlineNavBar;
-(void) onBackClicked;

-(void)showErrorAlertWithMessage: (NSString*) message;
-(void)showErrorAlertWithMessage: (NSString*) message alertId:(int) alertId;
-(void)showErrorAlertWithMessage: (NSString*) message alertId:(int) alertId style:(AlertStyle) style;
-(void)showActionsAlertWithMessage:(NSString*) message request:(int)requestId delegate: (id<AlertActionsDelegate>) delegate;
@end
