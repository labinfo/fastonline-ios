//
//  CarData.h
//  FastOnline
//
//  Created by Nico Sordoni on 06/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CarMaker.h"
#import "CarModel.h"
#import "CarVersion.h"
#import "Maintenance.h"

@interface CarData : NSObject<NSCoding>

@property (nonatomic, strong)  CarMaker *maker;
@property (nonatomic, strong)  CarModel *model;
@property (nonatomic, strong)  CarVersion *version;

@property (nonatomic, strong)  NSString *plateNumber, *fuelType, *color, *park, *fuelLevel;
@property (nonatomic, strong)  NSDate *matriculationDate, *revisionDate, *arrivalDate;
@property (nonatomic) int userRate, kmCrossed, kw, doorsNumber, seatsNumber, cc;
@property (nonatomic) double fuelLevelValue;
@property (nonatomic, strong)  NSMutableArray *maintenances;

@property (nonatomic)  BOOL isUserAdmin;

- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

@end
