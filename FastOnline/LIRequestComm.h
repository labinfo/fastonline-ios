//
//  LIRequestComm.h
//  RSM-TV
//
//  Created by Laboratorio Informatico on 05/09/14.
//  Copyright (c) 2014 LabInfo. All rights reserved.
//

#import "LIRequest.h"

@interface LIRequestComm : LIRequest

@property (nonatomic,strong,readonly) NSString* errorMessage;
@end
