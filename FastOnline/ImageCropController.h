//
//  ImageCropController.h
//  FastOnline
//
//  Created by Nico Sordoni on 27/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageCropView.h"

@protocol ImageCropDelegate <NSObject>

-(void) viewCropped: (UIImage*) image forId: (int) imageId;

@end
@interface ImageCropController : UIViewController


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cropViewHeight;
@property (strong, nonatomic) IBOutlet ImageCropView *cropView;
@property (strong, nonatomic)  UIImage *image;
@property (nonatomic)  int rotationsCount;

@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *rotateButton;
@property (nonatomic)  int requestId;

@property (strong, nonatomic) id<ImageCropDelegate> cropDelegate;

-(IBAction)deleteCrop:(id)sender;
-(IBAction)confirmCrop:(id)sender;
-(IBAction)rotate:(id)sender;

@end
