//
//  OptionalCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "OptionalCell.h"

@implementation OptionalCell{
    Optional *optional;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
}

-(void)checkboxClicked:(id)sender{
    if(!self.isViewMode)
        optional.selected = !optional.selected;
}

-(void)setOptional:(Optional *)opt{
    
    //Remove observer from previous observed optionals
    if(optional!=nil && !self.isViewMode){
        [optional removeObserver:self forKeyPath:@"selected"];
    }
    
    optional = opt;
    
    self.optionalName.text = optional.name;
    [self setCheckImage];
    
    if(!self.isViewMode)
        [optional addObserver:self forKeyPath:@"selected" options:NSKeyValueObservingOptionNew context:nil];
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    [self setCheckImage];
    
}

-(void)setCheckImage{
    //Optional is the only observed value
    if(!optional.selected){
        self.checkView.image = [UIImage imageNamed:@"checkbox_empty.png"];
    }
    else{
        self.checkView.image = [UIImage imageNamed:@"checkbox-select.png"];
    }
}

-(void)dealloc{
    if(!self.isViewMode)
        [optional removeObserver:self forKeyPath:@"selected"];
}
@end
