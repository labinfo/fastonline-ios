//
//  HomeNavigationController.m
//  MyFirstFlash
//
//  Created by Nico Sordoni on 20/01/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

//Class which control the controllers appearing in the main screen
#import "HomeNavigationController.h"
#import "LeftMenuController.h"

@interface HomeNavigationController ()

@end

@implementation HomeNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIViewController* controller= self.viewControllers[0];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40); // custom frame
    [backButton setImage:[UIImage imageNamed:@"ico_menu@2x.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *logoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    logoButton.frame = CGRectMake(0, 0, 143, 40); // custom frame
    
    
    [logoButton setImage:[UIImage imageNamed:@"logo_manheim.png"] forState:UIControlStateNormal];
    [logoButton setImage:[UIImage imageNamed:@"logo_manheim.png"] forState:UIControlStateHighlighted];
    
    logoButton.showsTouchWhenHighlighted = NO;
    logoButton.highlighted = NO;
    
    UIBarButtonItem* logo = [[UIBarButtonItem alloc] initWithCustomView:logoButton];

    // set left barButtonItem to backButton
    
    controller.navigationItem.leftBarButtonItems =@[[[UIBarButtonItem alloc] initWithCustomView:backButton],logo];
    
    
}
- (void)backButtonPressed
{
    // write your code to prepare popview
    [self.viewDeckController toggleLeftViewAnimated:true];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
