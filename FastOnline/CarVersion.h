//
//  CarVersion.h
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarVersion : NSObject<NSCoding>

@property (nonatomic, strong, readonly)  NSString *versionId;
@property (nonatomic, strong, readonly)  NSString *name;


-(id) initWithData:(id)data;

- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

@end
