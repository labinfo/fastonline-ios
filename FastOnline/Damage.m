//
//  Damage.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "Damage.h"
#import "LocalizationSystem.h"

@implementation Damage

-(id)initWithMacro:(MacroDamage *)macro detail:(DamageDetail *)detail type:(DamageType *)type seriousness:(SeriousnessLevel)level{
    self = [super init];
    
    if(self){
        self->_macro = macro;
        self->_level = level;
        self->_detail = detail;
        self->_type = type;
        self->_notes = @"";
        self->_photos = [NSMutableArray new];
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    self =  [super init];
    if (self) {
        self->_macro = [decoder decodeObjectForKey:@"macro"];
        self->_level = [decoder decodeIntForKey:@"level"];
        self->_detail = [decoder decodeObjectForKey:@"detail"];
        self->_type = [decoder decodeObjectForKey:@"type"];
        self->_notes = [decoder decodeObjectForKey:@"notes"];
        self->_photos = [decoder decodeObjectForKey:@"photos"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self->_macro forKey:@"macro"];
    [encoder encodeInt:self->_level forKey:@"level"];
    [encoder encodeObject:self->_detail forKey:@"detail"];
    [encoder encodeObject:self->_type forKey:@"type"];
    [encoder encodeObject:self->_notes forKey:@"notes"];
    [encoder encodeObject:self->_photos forKey:@"photos"];
    
}

+(NSString *)stringForSeriousnessLevel:(SeriousnessLevel)level{
    
    NSString* stringName = [NSString stringWithFormat:@"SeriousnessLevel%d", level];
    return AMLocalizedString(stringName, @"");
}

-(NSString*) stringForSeriousnessLevel{
    return [Damage stringForSeriousnessLevel:self.level];
}

@end
