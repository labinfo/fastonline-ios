//
//  DamagesDataManager.m
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamagesDataManager.h"

@implementation DamagesDataManager{
    NSMutableDictionary *macroDamages, *damageDetails, *damageTypes;
}

-(instancetype)init{
    self = [super init];
    
    if(self)
        [self loadData];
    
    return self;
}

#pragma mark DATA LOAD

-(void)loadData{
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"danni" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    id allValues = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    
    id types = [allValues objectForKey:@"typeList"];
    [self initTypes:types];
    
    id macroList = [allValues objectForKey:@"macroList"];
    [self initMacroDamages:macroList];
    
    id detailList = [allValues objectForKey:@"map"];
    [self initDamagesDetail:detailList];
}

-(void) initMacroDamages: (id) list{
    macroDamages = [NSMutableDictionary new];
    
    for(id macroValue in list){
        MacroDamage *macro =[[MacroDamage alloc] initWithData:macroValue];
        [macroDamages setObject:macro forKey:macro.damageId];
    }
}

-(void) initDamagesDetail: (id) list{
    damageDetails = [NSMutableDictionary new];
    
    for(id detailValue in list){
        DamageDetail *detail =[[DamageDetail alloc] initWithData:detailValue];
        [damageDetails setObject:detail forKey:detail.detailId];
        
        //Add detail to its macro
        NSString *macroId = [[detailValue objectForKey:@"macro"] stringValue];
        MacroDamage *macro = [macroDamages objectForKey:macroId];
        [macro addSubDamage:detail];
        
        //Read types list and add them to current detail
        id typesList = [detailValue objectForKey:@"type"];
        for(NSNumber *typeId in typesList){
            
            DamageType *type = [damageTypes objectForKey:[typeId stringValue]];
            [detail addType:type];
        }
        
    }
}

-(void) initTypes: (id) list{
    damageTypes = [NSMutableDictionary new];
    
    for(id typeValue in list){
        DamageType *type =[[DamageType alloc] initWithData:typeValue];
        [damageTypes setObject:type forKey:type.typeId];
    }
    
    
}

#pragma mark GETTERS

-(MacroDamage *)getMacroForId:(NSString *)macroId{
    return [macroDamages objectForKey:macroId];
}

-(DamageDetail *)getDetailForId:(NSString *)detailId{
    return [damageDetails objectForKey:detailId];

}

-(DamageType *)getDamageTypeForId:(NSString *)typeId{
    return [damageTypes objectForKey:typeId];
}
@end
