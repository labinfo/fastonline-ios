//
//  SSUListSynchronizer.m
//  fastonline
//
//  Created by Nico Sordoni on 13/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "SSUListSynchronizer.h"
#import "LocalizationSystem.h"

@implementation SSUListSynchronizer{
    NSMutableArray* SSUs;
    SSUSynchronizer *synchronizer;
    int succeeded, failed;
}

-(id)initWithSSUs:(NSArray *)SSUList{
    self = [super init];
    if(self){
        SSUs = [NSMutableArray new];
        for(SSUDetail *detail in SSUList){
            [SSUs addObject:detail];
        }
        
        self.allSuccess = self.allFails = YES;
        succeeded = 0;
        failed=0;
    }
    
    return self;
    
}

-(void)synchronizeSSUs{
    if(SSUs.count>0){
        [self syncNext];
    }
    else{
        self.allFails = NO;
        [self.delegate synchronizationCompletedFrom: self sendFailed:failed sendSucceeded:succeeded];
    }
}

-(void)syncNext{
    
    if(SSUs.count>0){
        
        [self.delegate sendingSSUNumber:succeeded+failed+1 of:SSUs.count];
        
        //If exists, get next SSU to synchronize
        SSUDetail *nextSSU = [SSUs objectAtIndex:0];
        [SSUs removeObjectAtIndex:0];
        
        [self doSync:nextSSU];
    }
    else{
        [self.delegate synchronizationCompletedFrom: self sendFailed:failed sendSucceeded:succeeded];
    }
}


-(void) stopSync{
    
    if(synchronizer)
        [synchronizer stopSync];
    
    SSUs = nil;
    
}

-(void)syncronizationCompletedForSSU:(SSUDetail *)SSUdetail{
    
    self.allFails = NO;
    succeeded++;
    
    [self syncNext];
}

-(void)syncronizationFailedForSSU:(SSUDetail *)SSUdetail{
    
    self.allSuccess = NO;
    failed++;
    
    [self syncNext];
}

//Method which only instantiate synchronizer and send data
-(void) doSync:(SSUDetail *) detail{
    
    synchronizer = [[SSUSynchronizer alloc] initWithSSU:detail];
    synchronizer.delegate = self;
    [synchronizer synchronizeSSU];
}


@end
