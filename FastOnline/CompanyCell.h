//
//  CompanyCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Company.h"

@interface CompanyCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *addressField;
@property (strong, nonatomic) IBOutlet UITextField *townField;
@property (strong, nonatomic) IBOutlet UITextField *provinceField;
@property (strong, nonatomic) IBOutlet UITextField *capField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *phoneField;
@property (strong, nonatomic) IBOutlet UIImageView *pictureView;

-(void) setCompany:(Company*) company;

@end
