//
//  DamageFormController.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamageFormController.h"
#import "LocalizationSystem.h"
#import "DamageFormTopCell.h"
#import "DamageFormPicturesCell.h"
#import "Device.h"

#define REQUEST_ID_POSITION 1
#define REQUEST_ID_GRAVITY 2
#define REQUEST_ID_TYPE 3

#define IMAGES_IN_ROW 3

@interface DamageFormController ()

@end

@implementation DamageFormController{
    int gravityLevel;
    DamageDetail *selectedDetail;
    DamageType *selectedType;
    
    DamageFormTopCell *topCell;
    
    NSMutableArray* images;
    int imageCellsCount;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self dismissKeyboard];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setFastOnlineNavBar];
    
    gravityLevel = 0;
    imageCellsCount = 0;
    images =[NSMutableArray new];
    
    if(self.damage != nil){
        selectedType = self.damage.type;
        selectedDetail = self.damage.detail;
        gravityLevel = self.damage.level;
        
        if(self.damage.photos != nil){
            images = self.damage.photos;
            imageCellsCount =(int) (images.count + 2) / 3;
        }
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)notesEditingEnd:(id)sender{
    self.damage.notes = topCell.notesText.text;
}

#pragma mark - IMAGES

-(void)addPicture:(id)sender{
    
    CameraControllerInvoker* invoker = [CameraControllerInvoker new];
    invoker.controller = self;
    invoker.delegate = self;
    
    [invoker takePictureForRequest:1];
}

-(void)onPictureEdited:(UIImage *)image forId:(int)requestId{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [images addObject:image];
    
    if(images.count % IMAGES_IN_ROW == 1)
        imageCellsCount++;
    
    [self.table reloadData];
}

-(void)onEditAbortedforId:(int)requestId{
    
}

#pragma mark - CONFIRM DAMAGE

-(void)confirm:(id)sender{
    if([self validationOk]){
        
        if(self.damage == nil){
            self.damage = [Damage new];
        }
        
        self.damage.macro = self.macro;
        self.damage.detail = selectedDetail;
        self.damage.level = gravityLevel;
        self.damage.type = selectedType;
        self.damage.notes =  topCell.notesText.text;
        self.damage.photos = images;
        
        
        [self.damageDelegate damageNotified:self.damage];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(BOOL)validationOk{
    
    NSMutableString* errorMsg=[[NSMutableString alloc] init];
    
    
    if(selectedDetail==nil){
        [errorMsg appendString:AMLocalizedString(@"RequiredFieldPosition", @"")];
        [errorMsg appendString:@"\n"];
    }
    if(gravityLevel==0){
        [errorMsg appendString:AMLocalizedString(@"RequiredFieldSeriousness", @"")];
        [errorMsg appendString:@"\n"];
    }
    if(selectedType==nil){
        [errorMsg appendString:AMLocalizedString(@"RequiredFieldType", @"")];
        [errorMsg appendString:@"\n"];
    }
    
    if(errorMsg.length>0){
        
        errorMsg = [NSMutableString stringWithFormat:@"%@\n%@", AMLocalizedString(@"RequiredFields", @""), errorMsg];
        [self showErrorAlertWithMessage:errorMsg];
        return false;
    }
    
    return true;
}


#pragma mark - TABLEVIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2 + imageCellsCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        DamageFormTopCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"TopCell"];
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
        topCell = tableCell;
        
        if(self.damage != nil){
            [topCell setDamage:self.damage];
        }
        
        if(self.isViewMode){
            topCell.title.text = AMLocalizedString(@"DamageInsertViewModeTitle", @"");
            tableCell.notesText.enabled = NO;
        }
        
        tableCell.backgroundColor = [UIColor clearColor];
        return tableCell;
    }
    else if(indexPath.row <= imageCellsCount){
        
        DamageFormPicturesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PicsCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //Get index of first image that will appear in current row
        int firstImageIndex = (int)(indexPath.row - 1) * IMAGES_IN_ROW;
        
        cell.picture1.image = images[firstImageIndex];
        
        cell.picture2.image = images.count > firstImageIndex + 1 ? images[firstImageIndex + 1] : nil;
        
        cell.picture3.image = images.count > firstImageIndex + 2 ? images[firstImageIndex + 2] : nil;
        
        
        
        return cell;
    }
    else{
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"BottomCell"];
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableCell.backgroundColor = [UIColor clearColor];
        
        if(self.isViewMode)
            tableCell.hidden = YES;
        
        return tableCell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissKeyboard];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        return 380;
        
    }
    else if(indexPath.row <= imageCellsCount){
        return (SCREEN_WIDTH - 50) / 3.2;
    }
    else{
        //View mode has no buttons
        if(self.isViewMode)
            return 0;
        return 150;
    }
    
}


#pragma mark - DATA PICKERS

-(void)pickPosition:(id)sender{
    [self showListPickerForRequestId:REQUEST_ID_POSITION title:AMLocalizedString(@"PositionChooserTitle", @"")];
}

-(void)pickGravity:(id)sender{
    [self showListPickerForRequestId:REQUEST_ID_GRAVITY title:AMLocalizedString(@"GravityChooserTitle", @"")];
    
}
-(void)pickType:(id)sender{
    if(selectedDetail!=nil)
        [self showListPickerForRequestId:REQUEST_ID_TYPE title:AMLocalizedString(@"TypeChooserTitle", @"")];
}

-(void)showListPickerForRequestId: (int) requestId title: (NSString*)title{
    
    if(!self.isViewMode){
        ListSelectController* listController=[self.storyboard instantiateViewControllerWithIdentifier:@"ListSelect"];
        
        listController.listDelegate=self;
        listController.listDataSource = self;
        listController.requestId = requestId;
        
        [self presentTransparentModalViewController:listController animated:true withAlpha:1.0];
        listController.title = title;
    }
    
}

#pragma mark LIST DELEGATES

-(int)getListLengthForRequest:(int)requestId{
    switch (requestId) {
            
        case REQUEST_ID_POSITION:
            return (int) self.macro.subDamages.count;
            
        case REQUEST_ID_GRAVITY:
            return 3;
            
        case REQUEST_ID_TYPE:
            return (int)selectedDetail.types.count;
            
        default:
            return 0;
    }
}

-(NSString *)getTextAtPosition:(int)position forRequest:(int)requestId{
    switch (requestId) {
            
        case REQUEST_ID_POSITION:{
            DamageDetail* detail = self.macro.subDamages[position];
            return detail.name;
            
        }
        case REQUEST_ID_GRAVITY:{
            NSString* stringName = [NSString stringWithFormat:@"SeriousnessLevel%d", position + 1];
            return  AMLocalizedString(stringName, @"");
        }
        case REQUEST_ID_TYPE:{
            DamageType *type = selectedDetail.types[position];
            return type.name;
        }
        default:
            return @"";
    }
    
}

-(void)didSelectItemAtPosition:(int)position forRequest:(int)requestId{
    switch (requestId) {
            
        case REQUEST_ID_POSITION:{
            
            //If user select the current one damage, doesn't do nothing
            if(selectedDetail != self.macro.subDamages[position])
            {
                selectedDetail = self.macro.subDamages[position];
                gravityLevel = 0;
                selectedType = nil;
                
                topCell.detailText.text = selectedDetail.name;
                topCell.gravityLevelText.text = @"";
                topCell.damageTypeText.text = @"";
                
            }
            break;
        }
        case REQUEST_ID_GRAVITY:{
            gravityLevel = position + 1;
            
            NSString* stringName = [NSString stringWithFormat:@"SeriousnessLevel%d", position + 1];
            topCell.gravityLevelText.text = AMLocalizedString(stringName, @"");
            
            break;
        }
        case REQUEST_ID_TYPE:{
            selectedType = selectedDetail.types[position];
            
            topCell.damageTypeText.text = selectedType.name;
            break;
        }
    }
}


@end
