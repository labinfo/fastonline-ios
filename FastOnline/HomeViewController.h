//
//  ViewController.h
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@interface HomeViewController : BaseController

@property (nonatomic, strong) IBOutlet UILabel* copyrightLabel;
@property (nonatomic, strong) IBOutlet UIButton* loginButton;

-(IBAction)loginClicked:(id)sender;

@end

