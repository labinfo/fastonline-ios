//
//  LoginViewController.m
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "LoginViewController.h"
#import "ColorUtil.h"
#import "Device.h"
#import "ImpostazioniManager.h"
#import "Config.h"
#import "LocalizationSystem.h"
#import "Validator.h"
#import "LIRequestComm.h"
#import "AlertController.h"
#import "LeftMenuController.h"
#import "AppDataManager.h"
#import "CompaniesDownloader.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.title=@"";
    
    [self setFastOnlineNavBar];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.loginButton.layer.borderWidth= 2;
    self.loginButton.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.loginButton.layer.cornerRadius = 5;
    self.loginButton.backgroundColor = [[ImpostazioniManager get] getAppColor];
    
}
-(void) dismissKeyboard{
    [self.view endEditing:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self dismissKeyboard];
}
-(void) goAhead{
    
    LeftMenuController *left = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
    left.view.frame = CGRectMake(0, 0, 200, left.view.frame.size.height);
    
    //Center view will be set by LeftMenuController
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:nil
                                                                                    leftViewController:left
                                                                                   rightViewController:nil];
    deckController.leftLedge = SCREEN_WIDTH - LEFT_MENU_WIDTH;
    deckController.delegate = left;
    
    //[self presentViewController:deckController animated:TRUE completion:nil];
    [self.navigationController pushViewController:deckController animated:true];
}

-(IBAction) pwdRecovery:(id)sender{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"RecoveryVC"] animated:YES];

}



-(IBAction)login:(id)sender{
    
    
    
    NSString* user = self.userInput.text;
    NSString* pwd = self.pwdInput.text;
    
    /*
    user=@"nico@labinfo.net";
    pwd=@"nico90";
    
    user = @"andrea@labinfo.net";
    pwd=@"andrea";
    
    */
    
    [self dismissKeyboard];
    
    if([Validator stringNotBlank:user] && [Validator stringNotBlank:pwd]){
        
        
        LIRequestComm *request = [[LIRequestComm alloc] init];
        
        //Needed to use request inside block
        __weak BaseController* weakSelf = self;
        
        [self showProgressWithMessage:AMLocalizedString(@"LoginProgressMessage", @"")];
        
        [request setSuccess:^(id data){
            
            [[AppDataManager get] setUserData:data];
            CompaniesDownloader *downloader = [CompaniesDownloader new];
            downloader.controller = self;
            [downloader downloadCompaniesAndGoAhead];
        }];
        
        [request setFailure:^(id responseObject){
            
            
            NSString* errorMessage = [responseObject objectForKey:@"message"];
            if(![Validator stringNotBlank:errorMessage]){
                errorMessage = AMLocalizedString(@"BaseRequestErrorTitle", @"");
            }
            
            AlertController* alertController=[weakSelf.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
            
            [weakSelf presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
            alertController.messageLabel.text = errorMessage;
            
            [self hideProgress];
        }];
        
        NSDictionary* dict =[NSDictionary dictionaryWithObjectsAndKeys:user,@"username",pwd,@"password", nil];
        [request post: LOGIN_URL params:dict HTTPheaders:nil jsonBody:YES];
        
    }
    else{
        [self showErrorAlertWithMessage:AMLocalizedString(@"ErrorEmptyInput", @"")];
    }
}

-(void)modalClosed:(TrasparentebaseController *)modalController{
    [super modalClosed:modalController];
    self.transparentModalViewController = nil;
}

@end
