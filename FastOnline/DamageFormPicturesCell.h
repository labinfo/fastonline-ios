//
//  DamageFormPicturesCell.h
//  fastonline
//
//  Created by Nico Sordoni on 12/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DamageFormPicturesCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *picture1, *picture2, *picture3;


@end
