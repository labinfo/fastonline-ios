//
//  AddMaintenanceCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 23/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "AddMaintenanceCell.h"
#import "ColorUtil.h"

@implementation AddMaintenanceCell

- (void)awakeFromNib {
    self.btn.layer.borderWidth= 1;
    self.btn.layer.borderColor= [ColorUtil getRgbColorRed:140 green:140 blue:140 alpha:1].CGColor;
    self.btn.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

}

@end
