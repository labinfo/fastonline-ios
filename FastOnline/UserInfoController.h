//
//  UserInfoControllerViewController.h
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@end
