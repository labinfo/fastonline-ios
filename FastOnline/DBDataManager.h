//
//  DBDataManager.h
//  FastOnline
//
//  Created by Nico Sordoni on 08/10/15.
//  Copyright © 2015 Gruppo Filippetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSUDetail.h"

#define DB_FILE_NAME @"fastonline.db"

#define TABLE_NAME_SSU @"SSU"

#define FIELD_STATUS @"status"
#define FIELD_PLATE_NUMBER @"plateNumber"
#define FIELD_MAKER @"carMaker"
#define FIELD_MODEL @"carModel"
#define FIELD_VERSION @"carVersion"
#define FIELD_LOCAL_ID @"localId"
#define FIELD_SERVER_ID @"serverId"
#define FIELD_COMPANY_ID @"companyId"
#define FIELD_COMPLETION_LEVEL @"completion"



@interface DBDataManager : NSObject

+(DBDataManager*) shared;

-(BOOL)storeNewSSUId:(NSString*) localId;
-(BOOL)updateSSU:(SSUDetail*) ssu;
-(BOOL)deleteSSU:(SSUDetail*) ssu;

-(NSArray*) getSSUList;

@end
