//
//  Config.h
//  iAce
//
//  Created by Matteo Corradin on 19/09/13.
//  Copyright (c) 2013 Matteo Corradin. All rights reserved.
//



#define SERVER_UPDATE_PAGE @"/DatiProva.html"
#define URL_IMMAGINI @"/img/news/"
#define PUSH_NOTIFICATION_SET_TOKEN_PAGE @"/registra.php"

#define LEFT_MENU_WIDTH 260


#define REGISTRATION_URL @"account/ex-registration.php"
#define LOGIN_URL @"user/login"
#define FB_LOGIN_URL LOGIN_URL
#define COMPANIES_URL @"user/companies"///collaborator"
#define SYNC_URL @"vehiclesheets"

#define BACKUP_USER_EMAIL @"backup@gruppofilippetti.it"

#define APP_NAME @"FastOnline"
#define EMAIL_STAFF @"email@email.it"

#define DATA_RICERCA @"Data_ricerca"
#define LINGUA_DEFAULT @"it"



#define NOTIF_PROBLEMA_HARDWARE @"HW_PROBLEM"
#define NOTIF_PROBLEMA_GPS @"GPS_PROBLEM"

#define GOOGLE_ANALYTICS_ID @"UA-57879245-1"

#define PRECARICAMENTO @"NO"
#define DATA_PRECARICAMENTO @"11/11/2014 10:10:10"

#define DROPBOX_SHARED_ENABLED false

//New

#define SERVER_BASE_URL @"http://www.fastonline.eu/fastonline/api"