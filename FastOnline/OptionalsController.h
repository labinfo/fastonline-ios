//
//  OptionalsController.h
//  FastOnline
//
//  Created by Nico Sordoni on 28/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"
#import "AddOptionalAlertController.h"

@interface OptionalsController : BasePageController<UITableViewDataSource, UITableViewDelegate, AddOptionalDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (nonatomic) BOOL isViewMode;

-(IBAction)addOptional:(id)sender;

@end
