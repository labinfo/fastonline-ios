//
//  ImageCropController.m
//  FastOnline
//
//  Created by Nico Sordoni on 27/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "ImageCropController.h"
#import "Device.h"

@interface ImageCropController ()

@end

@implementation ImageCropController{
    double rotation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.cropViewHeight.constant = SCREEN_WIDTH / 4 * 3;
    rotation = 0;
    self.rotationsCount = 0;
    
    self.cropView.contentMode = UIViewContentModeScaleAspectFill;
    self.cropView.clipsToBounds = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //self.cropViewHeight.constant = SCREEN_WIDTH / 4 * 3;
    
    self.confirmButton.layer.cornerRadius = self.deleteButton.frame.size.width / 2;
    self.confirmButton.clipsToBounds = YES;
    self.deleteButton.layer.cornerRadius = self.deleteButton.frame.size.width / 2;
    self.deleteButton.clipsToBounds = YES;
    self.rotateButton.layer.cornerRadius = self.rotateButton.frame.size.width / 2;
    self.rotateButton.clipsToBounds = YES;
    
    
    //[self rotateUIImage:self.image clockwise:YES];
    self.cropView.image = self.image;
    //Image is shown rotated (cause camera is in land)
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)deleteCrop:(id)sender{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)confirmCrop:(id)sender{
    UIImage *cropped;
    CGRect CropRect = self.cropView.cropAreaInImage;
    CGImageRef imageRef = CGImageCreateWithImageInRect([self.cropView.imageView.image CGImage], CropRect) ;
    cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    [self.cropDelegate viewCropped: [self rotateUIImage:cropped] forId:self.requestId];
}

-(void)rotate:(id)sender{
    
    if(rotation == M_PI_2)
        rotation = 0;
    else
        rotation -= M_PI_2;
    
    self.rotationsCount = (self.rotationsCount + 1) % 4;
    
    self.cropView.transform = CGAffineTransformMakeRotation(rotation);
}

- (UIImage*)rotateUIImage:(UIImage*)sourceImage
{
    if(self.rotationsCount == 1)
        return [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationLeft];
    else if(self.rotationsCount == 2)
        return [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationDown];
    else if(self.rotationsCount == 3)
        return [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationRight];
    
    return sourceImage;
}

@end
