//
//  DateFieldPicker.h
//  FastOnline
//
//  Created by Nico Sordoni on 23/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "TrasparentebaseController.h"

@interface DateFieldPicker : TrasparentebaseController

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic) BOOL dateAndTime;


@property (nonatomic,strong) IBOutlet UIDatePicker *datePicker;


-(void)pickDateWithTextField:(UITextField *)textField containerController:(BaseController *)controller;
@end
