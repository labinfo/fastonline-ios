//
//  DateFieldPicker.m
//  FastOnline
//
//  Created by Nico Sordoni on 23/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DateFieldPicker.h"

@implementation DateFieldPicker{
    UITextField *field;
}

-(void)pickDateWithTextField:(UITextField *)textField containerController:(BaseController *)controller{
    field = textField;
    
    [controller presentTransparentModalViewController:self animated:true withAlpha:1.0];
}


-(void)viewWillAppear:(BOOL)animated{
    
    if(!self.dateAndTime)
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    
}


-(void)conferma:(id)sender{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if(self.dateAndTime)
        [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    else
        [formatter setDateFormat:@"dd/MM/yyyy"];
    
    field.text = [formatter stringFromDate:self.datePicker.date];
    
    self->_selectedDate = self.datePicker.date;
    [super conferma:sender];
}

@end
