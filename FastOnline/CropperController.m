//
//  CropperController.m
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CropperController.h"

@interface CropperController ()

@end

@implementation CropperController{
    double rotation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.cropViewHeight.constant = SCREEN_WIDTH / 4 * 3;
    self.rotationsCount = 0;
    
    //self.cropView.contentMode = UIViewContentModeScaleAspectFill;
    self.cropView.clipsToBounds = YES;
    self.cropView.image = self.image;
    
    if(self.image.size.width < self.image.size.height)
    {
        //self.cropView.rotationAngle -= M_PI_2;
    }
    
    rotation = 0;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //self.cropViewHeight.constant = SCREEN_WIDTH / 4 * 3;
    
    self.confirmButton.layer.cornerRadius = self.deleteButton.frame.size.width / 2;
    self.confirmButton.clipsToBounds = YES;
    self.deleteButton.layer.cornerRadius = self.deleteButton.frame.size.width / 2;
    self.deleteButton.clipsToBounds = YES;
    self.rotateButton.layer.cornerRadius = self.rotateButton.frame.size.width / 2;
    self.rotateButton.clipsToBounds = YES;
    
    
    //[self rotateUIImage:self.image clockwise:YES];
    self.cropView.image = self.image;
    //Image is shown rotated (cause camera is in land)
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)deleteCrop:(id)sender{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)confirmCrop:(id)sender{
    
    
    [self.cropDelegate viewCropped: [self rotateUIImage:self.cropView.croppedImage] forId:self.requestId];
}

-(void)rotate:(id)sender{
    
    if(rotation == M_PI_2)
        rotation = 0;
    else
        rotation -= M_PI_2;
    
    self.rotationsCount = (self.rotationsCount + 1) % 4;
    
    self.cropView.transform = CGAffineTransformMakeRotation(rotation);
    
    //[self.cropView setRotationAngle:M_PI_2 snap:NO];//.rotationAngle += M_PI_2;
    
}

- (UIImage*)rotateUIImage:(UIImage*)sourceImage
{
    if(self.rotationsCount == 1)
    return [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationLeft];
    else if(self.rotationsCount == 2)
    return [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationDown];
    else if(self.rotationsCount == 3)
    return [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationRight];
    
    return sourceImage;
}


@end
