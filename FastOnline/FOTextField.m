//
//  FOTextField.m
//  FastOnline
//
//  Created by Nico Sordoni on 20/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "FOTextField.h"
#import "ColorUtil.h"

@implementation FOTextField

- (void)drawRect:(CGRect)rect {
    
    CALayer* bottomBorder = [CALayer layer];
    
    UIColor* bordersColor = [ColorUtil getRgbColorRed:150 green:150 blue:150 alpha:1];
    bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height-1.0f, self.frame.size.width, 1.0);
    bottomBorder.backgroundColor = bordersColor.CGColor;
    [self.layer addSublayer:bottomBorder];
}


@end
