//
//  SSUSynchronizer.h
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSUDetail.h"
#import "BaseController.h"

@protocol SynchronizerDelegate <NSObject>

-(void) synchronizationAbortedForSSU: (SSUDetail*) SSUdetail;
-(void) syncronizationCompletedForSSU: (SSUDetail*) SSUdetail;
-(void) syncronizationFailedForSSU: (SSUDetail*) SSUdetail;
-(void) syncronizationFailedForSSU: (SSUDetail*) SSUdetail withMessage:(NSString*) message;

@end

@interface SSUSynchronizer : NSObject

@property (nonatomic)  id<SynchronizerDelegate> delegate;

-(id) initWithSSU: (SSUDetail*) SSU;
-(void) synchronizeSSU;
-(void) stopSync;

@end
