//
//  NSString+MD5.h
//  mff
//
//  Created by Nico Sordoni on 06/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(MD5)

-(NSString*) MD5;
@end
