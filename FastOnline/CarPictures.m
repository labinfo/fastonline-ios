//
//  CarPictures.m
//  FastOnline
//
//  Created by Nico Sordoni on 08/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarPictures.h"

@implementation CarPictures

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self =  [super init];
    if (self) {
    self->_images = [aDecoder decodeObjectForKey:@"images"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self->_images forKey:@"images"];
}
@end
