//
//  DamagesListController.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamagesListController.h"
#import "DamagesListCell.h"

@interface DamagesListController ()

@end

@implementation DamagesListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.isViewMode)
    {
        //If is in view mode, prevent user to create new SSU
        self.btnNewWidth.constant = 0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TABLEVIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.existingDamages.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Damage* damage = self.existingDamages[indexPath.row];
    
    DamagesListCell* cell = [tableView dequeueReusableCellWithIdentifier:@"DamageCell"];
    [cell setDamage:damage];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.listDelegate damagePickedFromList:self.existingDamages[indexPath.row]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self chiudi:nil];
}

-(void)newDamage:(id)sender{
    [self.listDelegate newDamageSelectedForMacroarea:self.macroArea];
    [self chiudi:sender];
}


@end
