//
//  CarDataCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 20/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import "CarData.h"

@interface CarDataCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *makerField;
@property (strong, nonatomic) IBOutlet UITextField *modelField;
@property (strong, nonatomic) IBOutlet UITextField *versionField;
@property (strong, nonatomic) IBOutlet UITextField *plateField;
@property (strong, nonatomic) IBOutlet UITextField *kmCrossedField;
@property (strong, nonatomic) IBOutlet UITextField *matriculationDateField;
@property (strong, nonatomic) IBOutlet UITextField *kwField;
@property (strong, nonatomic) IBOutlet UITextField *fuelField;
@property (strong, nonatomic) IBOutlet UITextField *doorsField;
@property (strong, nonatomic) IBOutlet UITextField *seatsField;
@property (strong, nonatomic) IBOutlet UITextField *colorField;
@property (strong, nonatomic) IBOutlet UITextField *ccField;
@property (strong, nonatomic) IBOutlet UITextField *revisionDateField;
@property (strong, nonatomic) IBOutlet UITextField *parkField;
@property (strong, nonatomic) IBOutlet UITextField *arrivalDateField;
@property (strong, nonatomic) IBOutlet UITextField *fuelLevelField;

@property (strong, nonatomic) IBOutlet UILabel *rateLabel;
@property (strong, nonatomic) IBOutlet RateView *rateView;

-(void) setCarData:(CarData*) data;

@end
