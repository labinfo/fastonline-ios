//
//  NewSSUPageController.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "NewSSUPageController.h"
#import "LocalizationSystem.h"
#import "ColorUtil.h"
#import "AppDataManager.h"
#import "DBDataManager.h"
#import "SynchronizationHelper.h"

#define SYNC_ALERT_ID 100

#define REQUEST_ID_BACK 1
#define REQUEST_ID_DELETE 2

@interface NewSSUPageController ()

@end

@implementation NewSSUPageController{
    PageMenuIcon* selectedIcon;
    SSUSynchronizer *synchronizer;
}

- (void)viewDidLoad {
    self.delegate = self;
    [super viewDidLoad];

    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    if(self.detail == nil){
        //Init self SSU
        NSString *newSSUID =[[AppDataManager get] newSSUIDforCompany:self.company];
        
        self.detail = [[SSUDetail alloc] initWithID:newSSUID];
        self.detail.companyID = self.company.companyId;
        
        [[DBDataManager shared] storeNewSSUId:newSSUID];

    }
    else{
        [self setMaxAllowedPosition:self.detail.completionLevel];
        
        for(int i = self.detail.completionLevel ; i>=0 ; i--){
            [self showVoiceAsSelected:i];
        }
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)leavingPageController{
    [super leavingPageController];
    self.delegate = nil;
    self.detail = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Add trash can button to navBar
    UIButton * trashCanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    trashCanButton.frame = CGRectMake(0, 0, 30, 30); // custom frame
    [trashCanButton setImage:[UIImage imageNamed:@"cestino.png"] forState:UIControlStateNormal];
    [trashCanButton addTarget:self action:@selector(deleteSSU:) forControlEvents:UIControlEventTouchUpInside];
    
    // set left barButtonItem to backButton
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:trashCanButton];
    
    
    // set left barButtonItem to backButton
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40); // custom frame
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *logoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    logoButton.frame = CGRectMake(0, 0, 143, 40); // custom frame
    
    
    [logoButton setImage:[UIImage imageNamed:@"logo_manheim.png"] forState:UIControlStateNormal];
    [logoButton setImage:[UIImage imageNamed:@"logo_manheim.png"] forState:UIControlStateHighlighted];
    
    logoButton.showsTouchWhenHighlighted = NO;
    logoButton.highlighted = NO;
    
    UIBarButtonItem* logo = [[UIBarButtonItem alloc] initWithCustomView:logoButton];
    
    self.navigationItem.leftBarButtonItems =@[[[UIBarButtonItem alloc] initWithCustomView:backButton],logo];
}


-(void)deleteSSU:(id) sender{
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    
    [alertController askConfirmForRequest:REQUEST_ID_DELETE delegate:self];
    
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    
    alertController.messageLabel.text = AMLocalizedString(@"ExitConfirmMessage", @"");
}

-(NSString*) keyForSSUSection: (int) sectionIndex{
    return [[AppDataManager get] keyForSection:sectionIndex ofSSUWithId:self.detail.localID];
}

-(void)onBack{
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    
    [alertController askConfirmForRequest:REQUEST_ID_BACK delegate:self];
    
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    alertController.messageLabel.text = AMLocalizedString(@"NewSSUBackPressed", @"");
}

-(void)leavingController:(BasePageController *)controller{
    id data = [controller getData];
    
    [[AppDataManager get] storeData:data forKey:[self keyForSSUSection:controller.position]];
    
    if(self.detail.completionLevel<controller.position){
        self.detail.completionLevel = controller.position;
        //Save new state (new position reached)
        [[AppDataManager get] addSSUDetail:self.detail];
    }
    if(data != nil){
        
        switch (controller.position) {
            case 0:
                [self.detail setCarData:data];
                [[AppDataManager get] addSSUDetail:self.detail];
                
                break;
                
                
            default:
                break;
        }
    }
}

-(void)forceDataStorageForController:(BasePageController *)controller{
    
    id data = [controller getData];
    
    [[AppDataManager get] storeData:data forKey:[self keyForSSUSection:controller.position]];
}

-(void)requestConfirmed:(int)requestId{
    if(requestId == REQUEST_ID_BACK){
        [self leavingController:[self getControllerForPosition:self.currentPosition]];
    }
    else{
        [[AppDataManager get] removeSSU: self.detail];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)end{
    self.detail.status = STATUS_COMPLETED;
    [[AppDataManager get] setSSUCompleted:self.detail];
    self.confirmView.hidden = NO;
    
}

-(NSArray *)getMenuVoices{
    return @[self.voice_1, self.voice_2, self.voice_3, self.voice_4, self.voice_5, self.voice_6];
}

-(void)showVoiceAsSelected:(int)position{
    if(selectedIcon)
        [selectedIcon deselect];
    
    PageMenuIcon *icon = [self getMenuVoices][position];
    icon.showBottomBorder = YES;
    [icon setSelectedWithColor:[ColorUtil getRgbColorRed:6 green:103 blue:206 alpha:1]];
    icon.image.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d-active.png", (position+1)]];
    
    selectedIcon = icon;
}

-(BasePageController *)createControllerForPosition:(int)position{
    BasePageController* controller;
    
    id data = [[AppDataManager get] loadDataForKey:[self keyForSSUSection:position]];
    switch (position) {
        case 0:{
            controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CarInfoVC"];
            break;
        }
        case 1:{
            controller =[self.storyboard instantiateViewControllerWithIdentifier:@"CarPicturesVC"];
            break;
        }
        case 2:{
            controller =[self.storyboard instantiateViewControllerWithIdentifier:@"OptionalsVC"];
            break;
        }
        case 3:{
            controller =[self.storyboard instantiateViewControllerWithIdentifier:@"StatusVC"];
            break;
        }
        case 4:{
            controller =[self.storyboard instantiateViewControllerWithIdentifier:@"DamagesMainVC"];
            break;
        }
        case 5:{
            controller =[self.storyboard instantiateViewControllerWithIdentifier:@"NotesVC"];
            break;
        }
        default:
            break;
            
            
    }
    
    if(data && controller){
        [controller loadData:data];
    }
    
    return controller;
    
}

#pragma mark - ACTIONS

-(void)sync:(id)sender{
    
    [self showProgressWithMessage:[NSString stringWithFormat:AMLocalizedString(@"SendingSSU", @""),1,1] detailText:@"" callbackOnTap:nil];
    
    //Sync only first element
    [self doSync:self.detail];
}
-(void) doSync:(SSUDetail *) detail{
    
    synchronizer = [[SSUSynchronizer alloc] initWithSSU:detail];
    synchronizer.delegate = self;
    [synchronizer synchronizeSSU];
}

-(void)syncronizationCompletedForSSU:(SSUDetail *)SSUdetail{
    
    
    //[self showErrorAlertWithMessage:AMLocalizedString(@"SyncOk", @"") alertId:SYNC_ALERT_ID];
    synchronizer.delegate = nil;
    synchronizer = nil;
    [self hideProgress];
    [SynchronizationHelper showSynchronizationAlertWithCompleted:1 andFailed:0 inController:self];
}

-(void)syncronizationFailedForSSU:(SSUDetail *)SSUdetail{
    //[self showErrorAlertWithMessage:AMLocalizedString(@"SyncFailed", @"") alertId:SYNC_ALERT_ID];
    synchronizer.delegate = nil;
    synchronizer = nil;
    [self hideProgress];
    [SynchronizationHelper showSynchronizationAlertWithCompleted:0 andFailed:1 inController:self];
}

-(void)modalClosed:(TrasparentebaseController *)modalController{
    //When sync alert is dismissed, go back
    [super modalClosed:modalController];
    if(modalController.modalId == SYNC_COMPLETED_ALERT_ID){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)synchronizationAbortedForSSU:(SSUDetail *)SSUdetail{
    
}


-(void)stopSync{
    [self hideProgress];
}

-(void)deleteAndRestart:(id)sender{
    
    [[AppDataManager get] removeSSU:self.detail];
    [self.navigationController popViewControllerAnimated:YES];
    
    NewSSUPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SSUCreationVC"];
    controller.company = self.company;
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)saveAndClose:(id)sender{
    
    [[AppDataManager get] setSSUCompleted:self.detail];
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
