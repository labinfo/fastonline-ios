//
//  DamageType.m
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamageType.h"

@implementation DamageType

-(id)initWithData:(id)data{
    self = [super init];
    
    if(self){
        self->_typeId = [[data objectForKey:@"id"] stringValue];
        self->_name = [data objectForKey:@"label"];
        self->_character = [data objectForKey:@"lettera"];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    self =  [super init];
    if (self) {
        self->_typeId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_character = [decoder decodeObjectForKey:@"char"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self->_typeId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_character forKey:@"char"];
    
}
@end
