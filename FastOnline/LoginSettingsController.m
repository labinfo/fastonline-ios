//
//  LoginSettingsController.m
//  FastOnline
//
//  Created by Nico Sordoni on 17/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "LoginSettingsController.h"
#import "ColorUtil.h"
#import "ImpostazioniManager.h"

@interface LoginSettingsController ()

@end

@implementation LoginSettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.updateButton.layer.borderWidth= 2;
    self.updateButton.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.updateButton.layer.cornerRadius = 5;
    self.updateButton.backgroundColor = [[ImpostazioniManager get] getAppColor];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
