//
//  LoginViewController.h
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@interface LoginViewController : BaseController<ModalControlDelegate>

@property (nonatomic, strong) IBOutlet UIView* userContainer;
@property (nonatomic, strong) IBOutlet UIView* pwdContainer;

@property (nonatomic, strong) IBOutlet UITextField* userInput;
@property (nonatomic, strong) IBOutlet UITextField* pwdInput;


@property (nonatomic, strong) IBOutlet UIButton* loginButton;

-(IBAction) pwdRecovery:(id)sender;
-(IBAction)login:(id)sender;
@end
