//
//  CompaniesDownloader.m
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CompaniesDownloader.h"
#import "LIRequestComm.h"
#import "LocalizationSystem.h"
#import "Config.h"
#import "AppDataManager.h"
#import "LeftMenuController.h"
#import "Device.h"

@implementation CompaniesDownloader

-(void) downloadCompaniesAndGoAhead{
    
    
    NSString* SID =[[AppDataManager get] getSID];
    LIRequestComm *request = [[LIRequestComm alloc] init];
    
    //Needed to use request inside block
    //__weak BaseController* weakSelf = self;
    
    [self.controller showProgressWithMessage:AMLocalizedString(@"DownloadingCompanies", @"")];
    
    [request setSuccess:^(id data){
        [[AppDataManager get] addCompanies:data];
        [self.controller hideProgress];
        [self goAhead];
    }];
    
    [request setFailure:^(id responseObject){
        
        /*Avoid crash if multiple press on button
         if(self.transparentModalViewController!=nil)
         return;
         
         
         NSString* errorMessage = [responseObject objectForKey:@"message"];
         if(![Validator stringNotBlank:errorMessage]){
         errorMessage = AMLocalizedString(@"BaseRequestErrorTitle", @"");
         }
         
         AlertController* alertController=[weakSelf.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
         
         [weakSelf presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
         alertController.messageLabel.text = errorMessage;
         */
        
        [self.controller hideProgress];
        [self goAhead];
    }];
    
    [request get: COMPANIES_URL params:nil HTTPheaders:nil jsonBody:YES];
    
    //While download is executed, start parse of vehicle data json
    [[AppDataManager get] loadVehicleData];
}

-(void) goAhead{
    
    LeftMenuController *left = [self.controller.storyboard instantiateViewControllerWithIdentifier:@"LeftMenu"];
    left.view.frame = CGRectMake(0, 0, 200, left.view.frame.size.height);
    
    //Center view will be set by LeftMenuController
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:nil
                                                                                    leftViewController:left
                                                                                   rightViewController:nil];
    deckController.leftLedge = SCREEN_WIDTH - LEFT_MENU_WIDTH;
    deckController.delegate = left;
    
    //[self presentViewController:deckController animated:TRUE completion:nil];
    [self.controller.navigationController pushViewController:deckController animated:true];
}
@end
