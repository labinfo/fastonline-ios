//
//  AppDataManager.m
//  FastOnline
//
//  Created by Nico Sordoni on 17/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "AppDataManager.h"
#import "SSKeychain.h"
#import "StorageManager.h"
#import "CarMaker.h"
#import "RandomUtils.h"
#import "DBDataManager.h"

#define USER_DATA_FILE @"UserData.data"
#define SSU_ID_MAX_FILE @"maxSSUID"
#define SSU_DETAILS_FILE @"SSUDetails"
#define COMPLETED_SSU_FILE @"SSUCompleted"
#define DATA_VERSION_FILE @"dataVersion"

#define APP_DATA_VERSION 2

static AppDataManager* shared = nil;

@implementation AppDataManager{
    NSMutableDictionary * dict;
    User* user;
    //SessionId
    NSString* SID;
    NSArray* carMakers;
    NSNumber *maxSSUID;
    NSMutableDictionary *SSUList;
    NSMutableDictionary *SSUCompleted;
    NSNumber* dataVersion;
    
    DBDataManager* dbManager;
    
}

+ (AppDataManager*)get {
    @synchronized(self) {
        if (shared == nil) {
            shared = [[AppDataManager alloc] init];
        }
    }
    return shared;
}

-(instancetype)init{
    self=[super init];
    
    if(self){
        
        user = [StorageManager loadFromFile:USER_DATA_FILE];
        SID = [SSKeychain passwordForService:@"SID" account:@"default"];
        
        dataVersion =[StorageManager loadFromFile:DATA_VERSION_FILE];
        
        dbManager = [DBDataManager shared];
        
        //TODO read from DB
        
        //A new version of app's data, force to delete the old information
        if(!dataVersion || [dataVersion intValue] < APP_DATA_VERSION){
            
            dataVersion = [NSNumber numberWithInt: APP_DATA_VERSION];
            [StorageManager storeToFile:DATA_VERSION_FILE object:dataVersion];
            
            [self cleanAllData];
        }
        else{
            [self initDataFromDB];
        }
    }
    
    return self;
}

-(void) initDataFromDB{
    
    SSUList = [NSMutableDictionary new];
    SSUCompleted = [NSMutableDictionary new];
    
    NSArray* ssuList = [dbManager getSSUList];
    for(SSUDetail * ssuDetail in ssuList){
        if(ssuDetail.status == STATUS_DRAFT){
            [SSUList setObject:ssuDetail forKey:ssuDetail.localID];
        }
        else
        {
            [SSUCompleted setObject:ssuDetail forKey:ssuDetail.localID];
        }
    }
    
    
}

-(void) cleanAllData{
    
    SSUList = [NSMutableDictionary new];
    [StorageManager storeToFile:SSU_DETAILS_FILE object:SSUList];
    
    SSUCompleted = [NSMutableDictionary new];
    [StorageManager storeToFile:COMPLETED_SSU_FILE object:SSUCompleted];
}

-(void)storeData:(id<NSCoding>)data forKey:(NSString *)key{
    [StorageManager storeToFile:key object:data];
    
}
-(id<NSCoding>)loadDataForKey:(NSString *)key{
    return [StorageManager loadFromFile:key];
}

#pragma mark - SSU

-(NSNumber *)newSSUID{
    NSNumber* newSSUID;
    @synchronized(self) {
        maxSSUID = [NSNumber numberWithInt:([maxSSUID intValue] + 1)];
        newSSUID = [maxSSUID copy];
        [StorageManager storeToFile:SSU_ID_MAX_FILE object:maxSSUID];
    }
    return newSSUID;
    
}

-(NSString *)newSSUIDforCompany:(Company *)company{
    CFAbsoluteTime time = CFAbsoluteTimeGetCurrent();
    
    //NSString * newId = [NSString stringWithFormat:@"%@_%@_%f_%@", company.companyId, SID, time, [RandomUtils randomValue:30]];
    NSString * newId = [NSString stringWithFormat:@"%f_%@_%@_%@",  time, [RandomUtils randomValue:30], SID, company.companyId];
    
    return newId;
}

-(NSString *)keyForSection:(int)sectionIndex ofSSUWithId:(NSString *)localID{
    return [NSString stringWithFormat:@"SSU_%@_section_%d",localID, sectionIndex];
}

#pragma mark SSU DRAFTS

-(void)addSSUDetail:(SSUDetail *)detail{
    [SSUList setObject:detail forKey:detail.localID];
    [dbManager updateSSU:detail];
}

-(void)removeSSU:(SSUDetail *)detail{
    [SSUList removeObjectForKey:detail.localID];
    [self removeSSUFiles:detail];
    [dbManager deleteSSU:detail];
}

-(void)removeAllSSUInList:(NSArray *)SSUDetailList{
    for(SSUDetail* detail in SSUDetailList){
        [SSUList removeObjectForKey:detail.localID];
        [self removeSSUFiles:detail];
        [dbManager deleteSSU:detail];
    }
    
}

-(void) removeSSUFiles:(SSUDetail*) detail{
    for(int i=0;i<6;i++){
        [StorageManager deleteFile:[self keyForSection:i ofSSUWithId:detail.localID]];
    }
}

-(void)modifyCompletedSSU:(SSUDetail *)SSU{
    //SSU.isCompleted = NO;
    SSU.status = STATUS_DRAFT;
    
    //Remove SSU from completed (without deleting SSU Files)
    [SSUCompleted removeObjectForKey:SSU.localID];
    
    //Add SSU to drafts and save persistently new state
    [SSUList setObject:SSU forKey:SSU.localID];
    [dbManager updateSSU:SSU];
    
}

-(NSArray *)getSSUDetailsList{
    return [SSUList allValues];
}

#pragma mark COMPLETED SSU

-(void)setSSUCompleted:(SSUDetail *)detail{
    
    //detail.isCompleted = YES;
    
    if(detail.status == STATUS_DRAFT){
        detail.status = STATUS_COMPLETED;
    }
    
    //Remove completed SSU from drafts (without deleting SSU Files)
    [SSUList removeObjectForKey:detail.localID];
    [SSUCompleted setObject:detail forKey:detail.localID];
    
    [dbManager updateSSU:detail];
}


-(void)removeCompletedSSU:(SSUDetail *)detail{
    [SSUCompleted removeObjectForKey:detail.localID];
    [self removeSSUFiles:detail];
    
    [dbManager deleteSSU:detail];
}

-(void)removeAllCompletedSSUIn:(NSArray *)SSUDetailList{
    for(SSUDetail* detail in SSUDetailList){
        [SSUCompleted removeObjectForKey:detail.localID];
        [self removeSSUFiles:detail];
        [dbManager deleteSSU:detail];
    }
    
}

-(NSArray *)getCompletedSSUList{
    return [SSUCompleted allValues];
}

#pragma mark USER

-(void)setUserData:(id)data{
    user = [[User alloc] initWithData:data];
    [StorageManager storeToFile:USER_DATA_FILE object:user];
}

-(User *)getUser{
    return user;
}
-(void) addCompanies: (id)data{
    [user addCompanies:data];
    //Update stored info
    [StorageManager storeToFile:USER_DATA_FILE object:user];
    
}

#pragma mark SENSITIVE DATA (SID)
-(NSString*) getSID{
    if(SID == nil){
        SID = [SSKeychain passwordForService:@"SID" account:@"default"];
    }
    return SID;
    
}

-(void)setSID: (NSString*) userSID{
    [SSKeychain setPassword:userSID forService:@"SID" account:@"default"];
    SID = userSID;
}

-(void)logout{
    [StorageManager storeToFile:@"UserData.data" object:nil];
    [SSKeychain deletePasswordForService:@"SID" account:@"default"];
    
    user =nil;
    SID = nil;
    
    //Remove all stored data
    [self removeAllSSUInList:[SSUList allValues]];
    [self removeAllCompletedSSUIn:[SSUCompleted allValues]];
    
    //Reset SSU local IDs count
    maxSSUID = [NSNumber numberWithInt:0];
    [StorageManager storeToFile:SSU_ID_MAX_FILE object:maxSSUID];
    
}

#pragma mark UTIL METHODS

- (id)getObjectForKey:(NSString*)key{
    return [dict objectForKey:key];
}

- (void)setObject:(id)valore forKey:(NSString*)key{
    if (valore == nil) {
        [dict removeObjectForKey:key];
    }
    else {
        [dict setObject:valore forKey:key];
    }
    //[dict writeToFile:path atomically:YES];
}

#pragma mark VEHICLES

-(void)loadVehicleData{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"vehiclesdata" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    id allValues = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    
    NSMutableArray* makers=[[NSMutableArray alloc] init];
    for (id value in allValues) {
        [makers addObject:[[CarMaker alloc] initWithData:value]];
    }
    
    carMakers = [[NSArray alloc] initWithArray:makers];
}

-(NSArray *)getCarMakers{
    return carMakers;
}

-(void) freeVehiclesData{
    carMakers = nil;
}

#pragma mark DAMAGES

-(DamagesDataManager *)getDamagesDataManager{
    return [DamagesDataManager new];
}

-(int) getUnsentItemsCount{
    return (int)[[SSUList allKeys]count];
}


@end
