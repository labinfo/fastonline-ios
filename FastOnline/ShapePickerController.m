//
//  ShapePickerController.m
//  FastOnline
//
//  Created by Nico Sordoni on 26/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "ShapePickerController.h"

@interface ShapePickerController ()

@end

@implementation ShapePickerController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.transform = CGAffineTransformMakeRotation(M_PI_2);
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)shapePicked:(id)sender{
    int index = ((UIButton*) sender).tag;
    
    if(index==0)
        [self.shapeDelegate front3QPicked];
    else if (index==1)
        [self.shapeDelegate rear3QPicked];
    else
        [self.shapeDelegate noShapePicked];
    
    [self chiudi:sender];
}

@end
