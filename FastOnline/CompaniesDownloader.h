//
//  CompaniesDownloader.h
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseController.h"

@interface CompaniesDownloader : NSObject

@property (nonatomic, strong) BaseController* controller;

-(void)downloadCompaniesAndGoAhead;

@end
