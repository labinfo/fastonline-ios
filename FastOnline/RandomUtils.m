//
//  RandomUtils.m
//  FastOnline
//
//  Created by Nico Sordoni on 09/10/15.
//  Copyright © 2015 Gruppo Filippetti. All rights reserved.
//

#import "RandomUtils.h"

static NSString* DATA = @"qTYUIhj8klDQWE6Rxc9VBNZXCw1OiopL7PasdfSA4zKJH3GFvb5nmgert2yu0M";

@implementation RandomUtils

+ (NSString*)randomValue:(int) maxValue{
    
    NSMutableString* string = [NSMutableString new];
    
    for(int i = 0 ; i < maxValue; i++){
        
        NSString* newRandomChar = [NSString stringWithFormat:@"%c",[DATA characterAtIndex:[self randomIntWithMax:DATA.length-1]]];
        [string appendString:newRandomChar];
        
    }
    
    return [NSString stringWithString:string];
}

+(int)randomIntWithMax:(int) max{
    return arc4random_uniform(max);
}

@end
