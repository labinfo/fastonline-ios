//
//  User.m
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "User.h"
#import "NilToDefaultConverter.h"

@implementation User

-(id)initWithData:(id)originalData{
    self = [super init];
    
    if(self){
        
        id data = [[NilToDefaultConverter alloc] initWithId:originalData andDefaultValue:@""];
        
        self->_id = [data objectForKey:@"id"];
        self->_name = [data objectForKey:@"name"];
        self->_surname = [data objectForKey:@"surname"];
        self->_admin = [[NSString stringWithFormat:@"%@",[data objectForKey:@"admin"]] isEqualToString:@"1"];
        self->_email = [data objectForKey:@"email"];
        self->_phone = [data objectForKey:@"phone"];
        self->_picture = [data objectForKey:@"picture"];
        self->_address = [[Address alloc] initWithStreet:[data objectForKey:@"address"] town:[data objectForKey:@"city"]
                                                   state:[data objectForKey:@"state"] zip:[data objectForKey:@"zip"]];
        
        //Check if data contains companes
        id companiesData = [originalData objectForKey:@"company"];
        if((companiesData && companiesData != (id)[NSNull null])){
            NSMutableArray *companies =[[NSMutableArray alloc] init];
            [companies addObject:[[Company alloc] initWithData:[originalData objectForKey:@"company"]]];
            self->_companies = companies;
        }
        
        //NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        
        self->_lastLogin = [dateFormatter dateFromString:[data objectForKey:@"lastLogin"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self->_id forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_surname forKey:@"surname"];
    [encoder encodeBool:self->_admin forKey:@"admin"];
    [encoder encodeObject:self->_email forKey:@"email"];
    [encoder encodeObject:self->_phone forKey:@"phone"];
    [encoder encodeObject:self->_picture forKey:@"picture"];
    [encoder encodeObject:self->_address forKey:@"address"];
    [encoder encodeObject:self->_companies forKey:@"company"];
    [encoder encodeObject:self->_lastLogin forKey:@"lastLogin"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_id = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_surname = [decoder decodeObjectForKey:@"surname"];
        self->_admin = [decoder decodeBoolForKey:@"admin"];
        self->_email = [decoder decodeObjectForKey:@"email"];
        self->_phone = [decoder decodeObjectForKey:@"phone"];
        self->_picture = [decoder decodeObjectForKey:@"picture"];
        self->_address = [decoder decodeObjectForKey:@"address"];
        self->_companies = [decoder decodeObjectForKey:@"company"];
        self->_lastLogin = [decoder decodeObjectForKey:@"lastLogin"];
    }
    return self;
}

-(void) addCompanies:(id) data{
    NSMutableArray *companies =[[NSMutableArray alloc] init];
    for(id value in data) {
        [companies addObject:[[Company alloc] initWithData:value]];
    }
    //[companies addObject:[[Company alloc] initWithData:[data objectForKey:@"company"]]];
    self->_companies = companies;
}
@end
