//
//  DamageType.h
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DamageType : NSObject<NSCoding>

@property (nonatomic, readonly)  NSString *typeId;
@property (nonatomic, strong, readonly)  NSString *name;
@property (nonatomic, strong, readonly)  NSString *character;

-(id) initWithData:(id)data;

@end
