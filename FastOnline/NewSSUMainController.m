//
//  NewSSUMainController.m
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "NewSSUMainController.h"
#import "ColorUtil.h"
#import "ImpostazioniManager.h"
#import "ListSelectController.h"
#import "AppDataManager.h"
#import "LocalizationSystem.h"
#import "LeftMenuController.h"
#import "NewSSUPageController.h"
#import "CameraControllerInvoker.h"

@interface NewSSUMainController ()

@end

@implementation NewSSUMainController{
    NSArray* companies;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.startButton.layer.borderWidth= 2;
    self.startButton.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.startButton.layer.cornerRadius = 5;
    self.startButton.backgroundColor = [[ImpostazioniManager get] getAppColor];
    
    User* user = [[AppDataManager get] getUser];
    companies = user.companies;
    
    self.title = @"";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    //Enable left menu (may have been disabled from push)
    [LeftMenuController shared].disabled=false;
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [LeftMenuController shared].disabled=true;

}



-(void)startNewSSU:(id)sender{
    
    
    if(companies.count>0){
        
        ListSelectController* listController=[self.storyboard instantiateViewControllerWithIdentifier:@"ListSelect"];
        
        listController.listDelegate=self;
        listController.listDataSource = self;
        
        [self presentTransparentModalViewController:listController animated:true withAlpha:1.0];
        listController.title = AMLocalizedString(@"CompanyChooserTitle", @"");
        
    }else{
        [self showErrorAlertWithMessage:AMLocalizedString(@"NoCompanyError", @"")];
    }
    
}


-(int) getListLengthForRequest:(int)requestId{
    return (int)companies.count;
}

-(NSString*) getTextAtPosition: (int) position forRequest:(int)requestId{
    Company* company = [companies objectAtIndex:position];
    
    return company.name;
}

-(void)didSelectItemAtPosition:(int)position forRequest:(int) requestId{
    
    NewSSUPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SSUCreationVC"];
    controller.company = [companies objectAtIndex:position];
    
    [self.navigationController pushViewController:controller animated:YES];

    
    
}

@end
