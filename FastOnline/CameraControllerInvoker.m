//
//  CameraControllerInvoker.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CameraControllerInvoker.h"
#import "CustomCameraController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "FileSystemInfo.h"
#import "Config.h"
#import <MobileCoreServices/UTCoreTypes.h>

@implementation CameraControllerInvoker{
    UIImagePickerController *globalPicker;
    CustomCameraController* cameraController;
    UIView* overlayView;

    BOOL picTaken;
}



-(void)viewCropped:(UIImage *)image forId:(int)imageId{
    [self.delegate onPictureEdited:image forId:imageId];
}

-(void)clear{
    cameraController = nil;
    globalPicker = nil;
    overlayView = nil;
}
-(void)takePictureForRequest:(int)requestId{
    
    picTaken = NO;
    
    globalPicker = [[UIImagePickerController alloc] init];
    globalPicker.delegate = self;
    globalPicker.sourceType = UIImagePickerControllerSourceTypeCamera;

    
    if(cameraController==nil){
        
        // create the overlay view
        overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, self.controller.view.frame.size.height-100, self.controller.view.frame.size.width, 100)];
        overlayView.opaque=NO;
        overlayView.backgroundColor=[UIColor clearColor];
        
        cameraController=[self.controller.storyboard instantiateViewControllerWithIdentifier:@"CameraOverlayVC"];
        cameraController.cameraDelegate = self;
        overlayView = cameraController.view;
        
        
        
        // parent view for our overlay
        UIView *cameraView=[[UIView alloc] initWithFrame:self.controller.view.bounds];
        [cameraView addSubview:overlayView];
        
    }
    
    globalPicker.showsCameraControls = NO;
    
    [globalPicker setCameraOverlayView:overlayView];
   
    
    globalPicker.cameraDevice=UIImagePickerControllerCameraDeviceRear;
    globalPicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
    
    //Tag is used to identify the controller for which has been made the request
    globalPicker.view.tag = requestId;
    
    [self.controller presentViewController:globalPicker animated:YES completion:NULL];
    
    
    if(requestId==0)
        cameraController.frontShape.hidden = false;
    else
        cameraController.frontShape.hidden = true;
    
    if (requestId==1)
        cameraController.rearShape.hidden = false;
    else
        cameraController.rearShape.hidden = true;
    
    
}

-(void) shootPicture {
    
    [globalPicker takePicture];
    if(!picTaken){
    SystemSoundID soundID = 1108;
    AudioServicesPlaySystemSound(soundID);
        picTaken = YES;
    }
    
}

-(void)closeCamera{
    [self cancelPicture];
}

- (void)cancelPicture {
    //cameraController = nil;
    [self.controller dismissViewControllerAnimated:YES completion:nil];
    //self.delegate = nil;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIFICA_BLOCCA" object:nil];
    
    [self cancelPicture];
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    UIImage *resizedImage = [self imageWithImage:chosenImage convertToSize:CGSizeMake(chosenImage.size.width/2, chosenImage.size.height/2)];
    
    CropperController *cropController = [self.controller.storyboard instantiateViewControllerWithIdentifier:@"CropperVC"];
    
    
    cropController.requestId = (int) picker.view.tag;
    cropController.cropDelegate = self;
    
    cropController.image = resizedImage;
    
    [self.controller presentViewController:cropController animated:YES completion:nil];
    
    
    
}
- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"NOTIFICA_BLOCCA" object:nil];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (UIImage*)rotateUIImage:(UIImage*)sourceImage clockwise:(BOOL)clockwise
{
    return  [UIImage imageWithCGImage:sourceImage.CGImage scale:1.0 orientation:UIImageOrientationRight];
}

@end
