//
//  StatusPickerController.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "StatusPickerController.h"

@interface StatusPickerController ()

@end

@implementation StatusPickerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch* touch = touches.allObjects[0];
    CGPoint touchLocation = [touch locationInView:self.view];
    if(!CGRectContainsPoint(self.containerView.frame, touchLocation))
        [self chiudi:nil];
}

-(void)dismissKeyboard{
    [self.view endEditing:YES];
    
}
-(void)statusBad:(id)sender{
    [self.pickerDelegate statusPicked:STATUS_BAD forId:self.requestId];
    [self chiudi:sender];
}

-(void)statusGood:(id)sender{
    [self.pickerDelegate statusPicked:STATUS_GOOD forId:self.requestId];
    [self chiudi:sender];
}

-(void)statusMedium:(id)sender{
    [self.pickerDelegate statusPicked:STATUS_MEDIUM forId:self.requestId];
    [self chiudi:sender];
}
@end
