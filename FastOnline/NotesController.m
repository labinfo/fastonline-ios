//
//  NotesController.m
//  FastOnline
//
//  Created by Nico Sordoni on 05/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "NotesController.h"
#import "NewSSUPageController.h"

@interface NotesController ()

@end

@implementation NotesController{
    NSString *notes;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    if(notes){
        self.notesField.text = notes;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.pageController leavingController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(id)getData{
    return self.notesField.text;
}

-(void)loadData:(id)data{
    notes = data;
}


-(void)goAhead:(id)sender{
    
    
    //Force data storage for notes
    NewSSUPageController* pageController = (NewSSUPageController*) self.pageController;
    [pageController forceDataStorageForController:self];
    
    [super goAhead:sender];
}

@end
