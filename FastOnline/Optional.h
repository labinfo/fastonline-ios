//
//  Optional.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Optional : NSObject<NSCoding>

@property (nonatomic)  BOOL selected;
@property (nonatomic, strong, readonly)  NSString *name;


-(id) initWithName:(NSString*)name;
@end
