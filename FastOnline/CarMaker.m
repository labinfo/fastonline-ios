//
//  CarMaker.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarMaker.h"
#import "CarModel.h"

@implementation CarMaker
-(id)initWithData:(id)data{
    self = [super init];
    
    if(self){
        
        //id data = [[NilToDefaultConverter alloc] data andDefaultValue:@""];
        
        self->_makerId = [data objectForKey:@"id"];
        self->_name = [data objectForKey:@"name"];
        
        id allModels=[data objectForKey:@"models"];
        
        NSMutableArray* models=[[NSMutableArray alloc] init];
        for (id modelData in allModels) {
            [models addObject:[[CarModel alloc] initWithData:modelData]];
        }
        
        self->_models = [[NSArray alloc] initWithArray:models];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self->_makerId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_models forKey:@"models"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_makerId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_models = [decoder decodeObjectForKey:@"models"];
    }
    return self;
}

@end
