//
//  CarPictures.h
//  FastOnline
//
//  Created by Nico Sordoni on 08/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarPictures : NSObject<NSCoding>

@property (strong, nonatomic) NSArray* images;

-(id)initWithCoder:(NSCoder *)aDecoder;
-(void)encodeWithCoder:(NSCoder *)aCoder;
@end
