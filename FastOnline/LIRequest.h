//
//  LIRequest.h
//
//  Created by Mirko Ravaioli on 05/09/14.
//  Copyright (c) 2014 Mirko Ravaioli. All rights reserved.
//

#import "LIRequestBase.h"

@class LIRequest;

@protocol LIRequestDelegate
@optional
- (void)success:(LIRequest*)request response:(id)response;
- (void)failure:(LIRequest*)request responseObject: (id)responseObject;
- (void)validateResponseError:(LIRequest*)request error:(NSInteger)error;
@end

@interface LIRequest : LIRequestBase

@property (assign) NSInteger tag;

- (void)setSuccess:(void (^)(id response))success;
- (void)setFailure:(void (^)(id response))failure;
- (void)setValidateResponseError:(void(^)(NSInteger error))failure;

-(void)setDelegate:(id<LIRequestDelegate>)delegate;

- (void)addObserverSuccess:(NSString*)name;
- (void)addObserverSuccess:(NSString*)name anObject:(id)object;
- (void)addObserverFailure:(NSString*)name;
- (void)addObserverFailure:(NSString*)name anObject:(id)object;
- (void)addObserverValidateResponseError:(NSString*)name;
- (void)addObserverValidateResponseError:(NSString*)name anObject:(id)object;
@end
