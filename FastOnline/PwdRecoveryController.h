//
//  PwdRecoveryController.h
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@interface PwdRecoveryController : BaseController

@property (nonatomic, strong) IBOutlet UIView* emailContainer;
@property (nonatomic, strong) IBOutlet UIButton* loginButton;
@property (nonatomic, strong) IBOutlet UITextField* emailInput;

-(IBAction)recovery:(id)sender;

@end
