//
//  RandomUtils.h
//  FastOnline
//
//  Created by Nico Sordoni on 09/10/15.
//  Copyright © 2015 Gruppo Filippetti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomUtils : NSObject
+ (NSString*)randomValue:(int) maxValue;
+(int)randomIntWithMax:(int) max;

@end
