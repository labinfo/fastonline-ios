//
//  DamagesListCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Damage.h"

@interface DamagesListCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *seriousnessLabel;
@property (nonatomic, strong) IBOutlet UILabel *positionLabel;
@property (nonatomic, strong) IBOutlet UILabel *typeLabel;

-(void) setDamage:(Damage*) damage;

@end
