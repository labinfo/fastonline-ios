//
//  OptionalCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Optional.h"

@interface OptionalCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *optionalName;
@property (strong, nonatomic) IBOutlet UIImageView *checkView;
@property (nonatomic) BOOL isViewMode;

-(IBAction)checkboxClicked:(id)sender;

-(void)setOptional:(Optional*) optional;

@end
