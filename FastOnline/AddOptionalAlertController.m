//
//  AddOptionalAlertController.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "AddOptionalAlertController.h"

@interface AddOptionalAlertController ()

@end

@implementation AddOptionalAlertController

- (void)viewDidLoad {
    [super viewDidLoad];

    [super setAdaptToKeyboard:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveButtonClicked:(id)sender{
    
    [self.optionalDelegate optionalSelected: self.nameField.text];
    
    [self chiudi:sender];
}

@end
