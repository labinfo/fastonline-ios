//
//  GSDropboxActivity.h
//
//  Created by Simon Whitaker on 19/11/2012.
//  Copyright (c) 2012 Goo Software Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActivityDismissDelegate

-(void) activityDidDismiss;

@end

@interface GSDropboxActivity : UIActivity


@property (nonatomic, weak) id<ActivityDismissDelegate> delegate;

+ (NSString*)activityTypeString;

@end
