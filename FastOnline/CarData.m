//
//  CarData.m
//  FastOnline
//
//  Created by Nico Sordoni on 06/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarData.h"

@implementation CarData

-(instancetype)init{
    self = [super init];
    if(self){
        self->_maintenances = [NSMutableArray new];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_maker = [decoder decodeObjectForKey:@"maker"];
        self->_model = [decoder decodeObjectForKey:@"model"];
        self->_version = [decoder decodeObjectForKey:@"version"];
        
        self->_plateNumber = [decoder decodeObjectForKey:@"plateNumber"];
        self->_fuelLevel = [decoder decodeObjectForKey:@"fuel"];
        self->_color = [decoder decodeObjectForKey:@"color"];
        self->_park = [decoder decodeObjectForKey:@"park"];
        self->_fuelLevel = [decoder decodeObjectForKey:@"fuelLevel"];
        
        self->_fuelLevelValue = [decoder decodeDoubleForKey:@"fuelLevelValue"];
        
        self->_matriculationDate = [decoder decodeObjectForKey:@"matrDate"];
        self->_revisionDate = [decoder decodeObjectForKey:@"revDate"];
        self->_arrivalDate = [decoder decodeObjectForKey:@"arrDate"];
        
        self->_userRate = [decoder decodeIntForKey:@"rate"];
        self->_kmCrossed = [decoder decodeIntForKey:@"km"];
        self->_kw = [decoder decodeIntForKey:@"kw"];
        self->_doorsNumber = [decoder decodeIntForKey:@"doors"];
        self->_seatsNumber = [decoder decodeIntForKey:@"seats"];
        self->_cc = [decoder decodeIntForKey:@"cc"];
        
        self->_maintenances = [decoder decodeObjectForKey:@"maintenances"];
    }
    return self;
    
}
-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self->_maker forKey:@"maker"];
    [encoder encodeObject:self->_model forKey:@"model"];
    [encoder encodeObject:self->_version forKey:@"version"];
    
    [encoder encodeObject:self->_plateNumber forKey:@"plateNumber"];
    [encoder encodeObject:self->_fuelLevel forKey:@"fuel"];
    [encoder encodeObject:self->_color forKey:@"color"];
    [encoder encodeObject:self->_park forKey:@"park"];
    [encoder encodeObject:self->_fuelLevel forKey:@"fuelLevel"];
    
    [encoder encodeDouble:self->_fuelLevelValue forKey:@"fuelLevelValue"];
    
    [encoder encodeObject:self->_matriculationDate forKey:@"matrDate"];
    [encoder encodeObject:self->_revisionDate forKey:@"revDate"];
    [encoder encodeObject:self->_arrivalDate forKey:@"arrDate"];
    
    [encoder encodeInt:self->_userRate forKey:@"rate"];
    [encoder encodeInt:self->_kmCrossed forKey:@"km"];
    [encoder encodeInt:self->_kw forKey:@"kw"];
    [encoder encodeInt:self->_doorsNumber forKey:@"doors"];
    [encoder encodeInt:self->_seatsNumber forKey:@"seats"];
    [encoder encodeInt:self->_cc forKey:@"cc"];
    
    [encoder encodeObject:self->_maintenances forKey:@"maintenances"];
    
}
@end
