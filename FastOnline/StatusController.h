//
//  StatusController.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"
#import "AddOptionalAlertController.h"
#import "StatusPickerController.h"


@interface StatusController : BasePageController<UITableViewDataSource, UITableViewDelegate, AddOptionalDelegate, StatusPickerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *table;
@property (nonatomic) BOOL isViewMode;


-(IBAction)addItem:(id)sender;

@end
