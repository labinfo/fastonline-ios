//
//  Optional.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "Optional.h"

@implementation Optional

-(id)initWithName:(NSString *)name{
    self =[super init];
    
    if(self){
        self->_name = name;
        self->_selected = NO;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self =[super init];
    
    if(self){
        
        self->_name = [aDecoder decodeObjectForKey:@"name"];
        self->_selected = [aDecoder decodeBoolForKey:@"selected"];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self->_name forKey:@"name"];
    [aCoder encodeBool:self->_selected forKey:@"selected"];
}
@end
