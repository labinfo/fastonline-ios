//
//  ImpostazioniManager.h
//  theananke
//
//  Created by Mirko Ravaioli on 11/09/13.
//  Copyright (c) 2013 Mirko Ravaioli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "User.h"

@interface ImpostazioniManager : NSObject

+ (ImpostazioniManager*)get;


-(void)setLingua:(NSString*)lingua;
-(NSString*)getLingua;
-(NSString*) getDeviceLangCode;

-(NSString*) getListDataFileName;

-(void)setPosizioneManualeLatitudine:(NSNumber*)latitudine longitudine:(NSNumber*)longitudine;
-(NSDictionary*)getPosizioneManuale;
-(void)setPosizioneManualeAttiva:(BOOL)stato;
-(BOOL)getPosizioneManualeAttiva;

-(UIColor*)getAppColor;


@end
