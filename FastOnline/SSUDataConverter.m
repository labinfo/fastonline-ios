//
//  SSUDataConverter.m
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "SSUDataConverter.h"
#import "AppDataManager.h"
#import "StorageManager.h"
#import "Damage.h"
#import "Optional.h"
#import "Status.h"
#import "CarPictures.h"

#define FRONT_PHOTO @"urlFrontPhoto"
#define REAR_PHOTO @"urlRearPhoto"
#define CRUSCOTTO @"cruscotto"
#define TARGA @"targa"
#define STRUMENTI @"strumenti"
#define SEDILE_ANTERIORE @"sedileAnteriore"
#define SEDILE_POSTERIORE @"sedilePosteriore"
#define BAGAGLIAIO  @"bagagliaio"

@implementation SSUDataConverter

-(instancetype)init{
    self = [super init];
    
    if(self){
        self.imagesQueue = [NSMutableDictionary new];
    }
    
    return self;
}


#pragma mark - SERVER DATA DICTIONARY
-(NSMutableDictionary *)getSSUDataDictionary{
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    [dict setObject:[self getVehicleInfo] forKey:@"vehicleInfo"];
    [dict setObject:[self getDamages] forKey:@"damages"];
    [dict setObject:[self getEquipments] forKey:@"equipments"];
    [dict setObject:[self getStatus] forKey:@"status"];
    [dict setObject:[self getPictures] forKey:@"photos"];
    
    [dict setObject:self.SSU.companyID forKey:@"companyId"];
    
    NSString* notes = [self getNotes];
    if(notes != nil)
        [dict setObject:notes forKey:@"notes"];
    
    return dict;
}

#pragma mark VEHICLE DATA

-(NSMutableDictionary *)getVehicleInfo{
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    CarData* data =  [StorageManager loadFromFile: [[AppDataManager get] keyForSection:0 ofSSUWithId:self.SSU.localID]];
    
    NSDateFormatter *UTCFormatter = [[NSDateFormatter alloc] init];
    [UTCFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    
    [dict setObject:[NSDictionary dictionaryWithObjectsAndKeys: data.maker.makerId, @"id", data.maker.name, @"name", nil]
             forKey:@"brand"];
    [dict setObject:[NSDictionary dictionaryWithObjectsAndKeys: data.model.modelId, @"id", data.model.name, @"name", nil]
             forKey:@"model"];
    [dict setObject:[NSDictionary dictionaryWithObjectsAndKeys: data.version.versionId, @"id", data.version.name, @"name", nil]
             forKey:@"version"];
    
    if(data.color.length>0){
        [dict setObject: data.color forKey:@"color"];
    }
    
    [self addValidateInt:data.doorsNumber toDict:dict forKey:@"doors"];
    [self addValidateInt:data.cc toDict:dict forKey:@"engineDisplacement"];
    [self addValidateInt:data.userRate toDict:dict forKey:@"grade"];
    [self addValidateInt:data.kmCrossed toDict:dict forKey:@"km"];
    [self addValidateInt:data.kw toDict:dict forKey:@"kw"];
    [self addValidateInt:data.seatsNumber toDict:dict forKey:@"places"];
    
    if(data.fuelLevelValue>=0){
        [dict setObject:[NSNumber numberWithDouble: data.fuelLevelValue] forKey: @"fuelQuantity"];
    }
    
    if(data.fuelType.length > 0){
        [dict setObject:data.fuelType forKey: @"fuelType"];
    }
    
    if(data.matriculationDate){
        [dict setObject:[dateFormatter stringFromDate:data.matriculationDate] forKey: @"matriculation"];
    }
    if(data.park.length > 0){
        [dict setObject:data.park forKey: @"park"];
    }
    if(data.plateNumber.length > 0){
        [dict setObject:data.plateNumber forKey: @"plate"];
    }
    
    if(data.revisionDate){
        [dict setObject:[dateFormatter stringFromDate:data.revisionDate] forKey: @"revision"];
    }
    if(data.arrivalDate){
        [dict setObject:[UTCFormatter stringFromDate:data.arrivalDate] forKey:@"arrival"];
    }
    
    return dict;
}


-(NSArray*) getMaintenances{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableArray* maintenances = [NSMutableArray new];
    CarData* data =  [StorageManager loadFromFile: [[AppDataManager get] keyForSection:0 ofSSUWithId:self.SSU.localID]];
    
    for(Maintenance *maint in data.maintenances){
        NSMutableDictionary * dict = [NSMutableDictionary new];
        
        if(maint.desc.length > 0){
            [dict setObject:maint.desc forKey:@"description"];
        }
        if(maint.date){
            [dict setObject:maint.date forKey:@"date"];
        }
        
        [self addValidateInt:maint.kmCrossed toDict:dict forKey:@"km"];
        
        [maintenances addObject:dict];
    }
    return maintenances;
}

-(void) addValidateInt: (int) value toDict:(NSMutableDictionary*) dict forKey:(NSString*) key{
    if(value >= 0){
        [dict setObject:[NSNumber numberWithInt: value] forKey:key];
    }
}

#pragma mark DAMAGES

-(NSArray *)getDamages{
    
    NSMutableArray* damages = [NSMutableArray new];
    
    NSMutableDictionary* damagesMap =  [StorageManager loadFromFile: [[AppDataManager get] keyForSection:4 ofSSUWithId:self.SSU.localID]];
    
    NSArray* allDamagesLists = [damagesMap allValues];
    
    for(NSMutableArray* damagesList in allDamagesLists){
        for(Damage *damage in damagesList){
            
            NSMutableDictionary* dict = [NSMutableDictionary new];
            
            [dict setObject:damage.detail.name forKey:@"zone"];
            [dict setObject:[NSNumber numberWithInt:damage.level] forKey:@"level"];
            
        
            if(damage.notes.length > 0){
                [dict setObject:damage.notes forKey:@"note"];
            }
            
            [dict setObject:damage.type.character forKey:@"type"];
            
            NSMutableArray * photos = [NSMutableArray new];
            if(damage.photos.count > 0){
                
                for(UIImage * image in damage.photos){
                    
                    NSString* fileName = [self getFileNameForNewPicture];
                    [self.imagesQueue setObject:image forKey:fileName];
                    [photos addObject:fileName];
                }
                
                [dict setObject:[NSArray arrayWithArray: photos] forKey:@"urlPhotos"];
            }
            
            [damages addObject:dict];
        }
    }
    
    return damages;
    
}

#pragma mark EQUIPMENTS

-(NSArray *)getEquipments{
    NSMutableArray *returnedData = [NSMutableArray new];
    
    NSMutableArray* array = [StorageManager loadFromFile: [[AppDataManager get] keyForSection:2 ofSSUWithId:self.SSU.localID]];
    
    
    NSMutableArray* selectedOptionals = [array objectAtIndex:1];
    
    for(Optional *optional in selectedOptionals){
        [returnedData addObject:optional.name];
    }
    
    return returnedData;
}

#pragma mark NOTES

-(NSString *)getNotes{
    
    
    NSString* notes = [StorageManager loadFromFile: [[AppDataManager get] keyForSection:5 ofSSUWithId:self.SSU.localID]];
    
    
    return notes;
}

#pragma mark STATUS

-(NSDictionary *)getStatus{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    NSMutableArray* data = [StorageManager loadFromFile: [[AppDataManager get] keyForSection:3 ofSSUWithId:self.SSU.localID]];
    
    NSMutableArray* statusList = [data objectAtIndex:0];
    NSMutableArray* optionals = [data objectAtIndex:1];
    
    Optional *crashed = optionals[0];
    [dict setObject: crashed.selected ? @YES : @NO forKey:@"autoIncidentata"];
    
    Optional *previousCrashes = optionals[1];
    [dict setObject: previousCrashes.selected ? @YES : @NO forKey:@"precedentiIncidenti"];
    
    
    NSMutableArray* components = [NSMutableArray new];
    for(Status *status in statusList){
        if(status.statusCode != NO_STATUS){
            [components addObject:[NSDictionary dictionaryWithObjectsAndKeys:status.name,@"component",
                                   [status stringForStatus] , @"state", nil]];
        }
    }
    [dict setObject:components forKey:@"components"];
    
    return dict;
}

#pragma mark PICTURES

-(NSDictionary *)getPictures{
    
    NSArray* picturesKeys = @[FRONT_PHOTO, REAR_PHOTO, CRUSCOTTO, TARGA, SEDILE_ANTERIORE, STRUMENTI, SEDILE_POSTERIORE, BAGAGLIAIO];
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    CarPictures* pics =  [StorageManager loadFromFile: [[AppDataManager get] keyForSection:1 ofSSUWithId:self.SSU.localID]];
    
    for(int i = 0; i< pics.images.count ;i++){
        if(pics.images[i] != [NSNull null]){
            
            NSString* fileName = [self getFileNameForNewPicture];
            [self.imagesQueue setObject:pics.images[i] forKey:fileName];
            [dict setObject:fileName forKey:picturesKeys[i]];
            
        }
    }
    
    
    return dict;
}

#pragma mark UTILS

-(NSString*) getFileNameForNewPicture{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd_HHmm"];
    NSDate *today = [NSDate date];
    
    NSString *fileName = [NSString stringWithFormat:@"%@_%d_%d.jpg",[formatter stringFromDate:today],[self.SSU.localID intValue],(int)self.imagesQueue.count+1];
    
    
    return fileName;
}

@end
