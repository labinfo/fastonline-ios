//
//  DamagesListCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamagesListCell.h"

@implementation DamagesListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)setDamage:(Damage *)damage{
    self.positionLabel.text = damage.detail.name;
    self.typeLabel.text = damage.type.name;
    self.seriousnessLabel.text = [damage stringForSeriousnessLevel];
    
}
@end
