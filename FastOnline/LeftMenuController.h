//
//  LeftMenuController.h
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"
#import "SSUListSynchronizer.h"

@interface LeftMenuController : UIViewController<IIViewDeckControllerDelegate,UITableViewDelegate,UITableViewDataSource, SSUListSynchronizerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *table;

+(LeftMenuController*)shared;
@property (nonatomic) BOOL disabled;
@end
