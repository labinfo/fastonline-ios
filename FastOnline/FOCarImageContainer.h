//
//  FOCarImageContainer.h
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarPicturesController.h"


@interface FOCarImageContainer : UIView

@property (strong, nonatomic)  CarPicturesController *pictureController;
@property (strong, nonatomic)  UIImage *image;
@property (nonatomic)  BOOL isViewMode;

@end
