//
//  NotesController.h
//  FastOnline
//
//  Created by Nico Sordoni on 05/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"

@interface NotesController : BasePageController

@property (strong, nonatomic) IBOutlet UITextField *notesField;

@end
