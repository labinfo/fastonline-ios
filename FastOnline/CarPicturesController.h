//
//  CarPicturesController.h
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePageController.h"
#import "CarPictures.h"
#import "CameraControllerInvoker.h"

@interface CarPicturesController : BasePageController<CameraInvokerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic)  CarPictures *data;

@property (nonatomic)  BOOL isViewMode;

-(IBAction)selectArrivalDate:(id)sender;

-(IBAction)pickFront3Q:(id)sender;

-(void) takePictureAtIndex: (int) index;
@end
