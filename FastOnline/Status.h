//
//  Status.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NO_STATUS 0
#define STATUS_GOOD 1
#define STATUS_MEDIUM 2
#define STATUS_BAD 3


@interface Status : NSObject<NSCoding>

@property (nonatomic)  int statusCode;
@property (nonatomic, strong, readonly)  NSString *name;

-(id) initWithName:(NSString*)name;
-(NSString*) stringForStatus;
@end
