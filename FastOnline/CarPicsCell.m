//
//  CarPicsCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 27/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarPicsCell.h"

@implementation CarPicsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
