//
//  FileSystemInfo.m
//  theananke
//
//  Created by Mirko Ravaioli on 11/09/13.
//  Copyright (c) 2013 Mirko Ravaioli. All rights reserved.
//

#import "FileSystemInfo.h"

@implementation FileSystemInfo

+ (void) deleteFile:(NSString*)file {
    NSString *path = [FileSystemInfo getWritablePathWithoutCopy:file];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    }
}

+ (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

+ (NSString *) getWritablePathWithoutCopy :(NSString *)file{
    NSString *writablePath = [[FileSystemInfo applicationDocumentsDirectory] stringByAppendingPathComponent:file];
    return writablePath;
}

+(id)loadDataFromPath:(NSString*)path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    NSData *data = [[NSMutableData alloc]
                    initWithContentsOfFile:path];
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc]
                                     initForReadingWithData:data];
    id object = [unarchiver decodeObjectForKey:@"data"];
    [unarchiver finishDecoding];
    return object;
}

+ (void) writeData:(id)object toPath:(NSString*)path {
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc]
                                 initForWritingWithMutableData:data];
    [archiver encodeObject:object forKey:@"data"];
    [archiver finishEncoding];
    [data writeToFile:path atomically:YES];
}

@end
