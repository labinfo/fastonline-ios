//
//  DamagesDataManager.h
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DamageDetail.h"
#import "DamageType.h"
#import "MacroDamage.h"

@interface DamagesDataManager : NSObject

-(MacroDamage*) getMacroForId: (NSString*) macroId;
-(DamageDetail*) getDetailForId: (NSString*) detailId;
-(DamageType*) getDamageTypeForId: (NSString*) typeId;

@end
