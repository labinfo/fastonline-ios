//
//  BasePageController.m
//  mff
//
//  Created by Nico Sordoni on 09/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import "BasePageController.h"

@interface BasePageController ()

@end

@implementation BasePageController{
    BOOL goingAhead;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    goingAhead = NO;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pageControllerDidExit{
    
}

-(BOOL) canGoForward{
    return false;
}

-(id)getData{
    return nil;
}

-(void)loadData:(id)data{
    
}

-(void)goAhead:(id)sender{
    @synchronized(self){
        if(!goingAhead) {
            goingAhead = YES;
            if(self.pageController.menuVoices.count - 1 == self.position){
                [self.pageController end];
            }
            else{
                [self.pageController goToPage:self.position + 1];
            }
            goingAhead = NO;
        }
    }
}
-(BOOL)validationOk{
    return true;
}

@end
