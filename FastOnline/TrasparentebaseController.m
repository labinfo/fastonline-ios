//
//  TrasparentebaseController.m
//  KiloRaze
//
//  Created by Lab Info on 28/01/15.
//  Copyright (c) 2015 Lab Info. All rights reserved.
//

#import "TrasparentebaseController.h"
#import "UIKeyboardListener.h"

@interface TrasparentebaseController ()

@end

@implementation TrasparentebaseController{
    NSLayoutConstraint* bottomMargin;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) dismissTransparentModalViewControllerAnimated:(BOOL) animated valore:(NSString*)valore andChiave:(NSString*)key{
    
    if (animated) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.view.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             //[self.caller showNavigationBar];
                             //[self.caller metodoChiamatoDaTrasparentController:valore andChiave:key];
                             [self.view removeFromSuperview];
                         }];
    }
}

- (IBAction)chiudi:(id)sender {
    [self dismissTransparentModalViewControllerAnimated:true valore:@"" andChiave:@""];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.delegate modalClosed: self];
    self.delegate = nil;
    
}

- (IBAction)conferma:(id)sender {
    [self chiudi:nil];
    
}

-(void)setAdaptToKeyboard:(BOOL)adaptToKeyboard{
    if(adaptToKeyboard){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    }
    else{
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

-(void)adaptLayoutWhenKeyboardAppear:(NSLayoutConstraint *)margin{
    
}

- (void)keyboardWillHide:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.bottomLayoutConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    CGFloat height = keyboardFrame.size.height;
    
    self.bottomLayoutConstraint.constant = height - 100;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}
@end
