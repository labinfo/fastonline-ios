//
//  NewSSUPageController.h
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"
#import "Company.h"
#import "AlertController.h"
#import "SSU.h"
#import "SSUSynchronizer.h"

@interface NewSSUPageController : PageContainerController<PageContainerDelegate, AlertConfirmDelegate, SynchronizerDelegate>

@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_1;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_2;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_3;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_4;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_5;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_6;

@property (strong, nonatomic) IBOutlet UIView *confirmView;

@property (strong, nonatomic) Company *company;
@property (strong, nonatomic) SSUDetail *detail;

-(void) deleteSSU: (id)sender;
-(IBAction) deleteAndRestart: (id)sender;
-(IBAction) saveAndClose: (id)sender;
-(IBAction) sync: (id)sender;

-(void)forceDataStorageForController: ( BasePageController*) controller;

@end
