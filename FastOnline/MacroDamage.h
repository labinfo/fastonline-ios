//
//  MacroDamage.h
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DamageDetail.h"

@interface MacroDamage : NSObject<NSCoding>

@property (nonatomic, readonly)  NSString *damageId;
@property (nonatomic, strong, readonly)  NSString *name;
@property (nonatomic, strong, readonly)  NSMutableArray *subDamages;

-(id) initWithData:(id)data;
-(void) addSubDamage: (DamageDetail*) detail;

@end
