//
//  Damage.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MacroDamage.h"

#define SERIOUSNESS_LEVEL_LOW 1
#define SERIOUSNESS_LEVEL_MEDIUM 2
#define SERIOUSNESS_LEVEL_HIGH 3

typedef int SeriousnessLevel;

@interface Damage : NSObject<NSCoding>

@property (nonatomic, strong) MacroDamage *macro;
@property (nonatomic, strong) DamageDetail *detail;
@property (nonatomic, strong) DamageType *type;
@property (nonatomic) SeriousnessLevel level;
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSMutableArray *photos;


-(id) initWithMacro : (MacroDamage*) macro detail: (DamageDetail*) detail type: (DamageType*) type seriousness: (SeriousnessLevel) level;

-(NSString*) stringForSeriousnessLevel;

+(NSString*) stringForSeriousnessLevel:(SeriousnessLevel) level;
@end
