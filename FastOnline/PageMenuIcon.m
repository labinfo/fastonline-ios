//
//  PageMenuIcon.m
//  mff
//
//  Created by Nico Sordoni on 10/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import "PageMenuIcon.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Overlay.h"

@implementation PageMenuIcon{
    CALayer* bottomBorder;
}


-(void) setSelectedWithColor: (UIColor*) color{
    if(self.showBottomBorder){
        bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height-3.0f, self.frame.size.width, 3.0f);
        bottomBorder.backgroundColor = color.CGColor;
        [self.layer addSublayer:bottomBorder];
    }
    
    self.image.image = [self.image.image imageWithColor:color];
    
}

-(void) deselect{
    if(bottomBorder){
        bottomBorder.backgroundColor = [UIColor clearColor].CGColor;
    }
}
@end
