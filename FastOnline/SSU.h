//
//  SSU.h
//  FastOnline
//
//  Created by Nico Sordoni on 06/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CarData.h"
#import "SSUDetail.h"

@interface SSU : NSObject

@property (nonatomic)  NSString* localID;
@property (nonatomic, strong)  CarData *carData;

-(SSUDetail*) getDetail;
@end
