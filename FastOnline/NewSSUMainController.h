//
//  NewSSUMainController.h
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "ListSelectController.h"

@interface NewSSUMainController : BaseController<ListDelegate, ListDataSource>

@property (nonatomic, strong) IBOutlet UIButton *startButton;

-(IBAction)startNewSSU:(id)sender;

@end
