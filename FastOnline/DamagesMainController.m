//
//  DamagesMainController.m
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamagesMainController.h"
#import "DamagesDataManager.h"
#import "AppDataManager.h"
#import "LocalizationSystem.h"

@interface DamagesMainController ()

@end

@implementation DamagesMainController{
    DamagesMainCell *cell;
    DamagesDataManager *damagesDataManager;
    DamageConfirmController * damageConfirmController;
    
    NSMutableDictionary* damagesMap;
    BOOL dataLoadedFromDisk;
    
}

-(void)dealloc{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    damagesDataManager = [[AppDataManager get] getDamagesDataManager];
    
    //Otherwise has been loaded from disk
    if(damagesMap == nil){
        damagesMap = [NSMutableDictionary new];
        dataLoadedFromDisk = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)pageControllerDidExit{
    
    damageConfirmController.macro = nil;
    damageConfirmController.confirmDelegate = nil;
    damageConfirmController.pageController = nil;
    damageConfirmController = nil;
    
    damagesDataManager= nil;
    damagesMap = nil;
    
    cell.delegate = nil;
    cell = nil;
}

#pragma mark - DATA LOAD/STORE

-(id)getData{
    return damagesMap;
}

-(void)loadData:(id)data{
    damagesMap = data;
    dataLoadedFromDisk = true;
}

#pragma mark - DAMAGES

//When WebView has been restored, if damage data has been loaded from disk, load previous existing damages
-(void)loadCompleted{
    if(dataLoadedFromDisk){
        NSArray* macroIds = damagesMap.allKeys;
        
        for(NSString* macroId in macroIds){
            MacroDamage* macro = [damagesDataManager getMacroForId:macroId];
            [self updateMacroArea:macro];
        }
    }
}

//When user tap on a macro area, must confirm the selection before starting the insertion, so a modal view is shown
-(void)damagePickedWithId:(NSString*)damageId{
    
    if(!self.isViewMode){
        MacroDamage *macro = [damagesDataManager getMacroForId:damageId];
        if(damageConfirmController == nil){
            
            damageConfirmController = [self.storyboard instantiateViewControllerWithIdentifier:@"DamageConfirmVC"];
            
            damageConfirmController.confirmDelegate = self;
            damageConfirmController.pageController = self.pageController;
        }
        
        damageConfirmController.macro = macro;
        [self.pageController presentTransparentModalViewController:damageConfirmController animated:YES withAlpha:1];
        
        damageConfirmController.macroNameLabel.text = macro.name;
    }
    else{
        [self damageInsertionConfirmed:[damagesDataManager getMacroForId:damageId]];
    }
    
}


//Called when a user tap an area of the car and select "Insert" in the appearing box
-(void)damageInsertionConfirmed:(MacroDamage *)macro{
    
    
    
    //If macro area contains yet at least a damage, the damageList must be shown, allowing the user to choose if insert a
    //new damage or modify an existing one
    NSMutableArray* macroAreaDamages = [damagesMap objectForKey: macro.damageId];
    
    if (macroAreaDamages == nil || macroAreaDamages.count == 0){
        if(!self.isViewMode ){
            [self launchDamageCreationControllerForMacro: macro withExistingDamage:nil];
        }
    }else{
        
        DamagesListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DamagesListVC"];
        controller.macroArea = macro;
        controller.existingDamages = macroAreaDamages;
        controller.listDelegate = self;
        controller.isViewMode = self.isViewMode;
        
        [self.pageController presentTransparentModalViewController:controller animated:YES withAlpha:1];
    }
    
}

//User required to modify an existing damage
-(void)damagePickedFromList:(Damage *)damage{
    [self launchDamageCreationControllerForMacro:damage.macro withExistingDamage:damage];
}

//From list of damages, user selected to create a new one
-(void)newDamageSelectedForMacroarea: (MacroDamage*) macro{
    [self launchDamageCreationControllerForMacro:macro withExistingDamage:nil];
    
}

//Launch the form to modify an existing damage or creating a new one (if parameter "damage" is null)
-(void) launchDamageCreationControllerForMacro: (MacroDamage*)macro withExistingDamage: (Damage*) damage{
    
    DamageFormController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DamageFormVC"];
    controller.macro = macro;
    controller.damage = damage;
    controller.damageDelegate = self;
    controller.isViewMode = self.isViewMode;
    
    [self.pageController.navigationController pushViewController:controller animated:YES];
    /*If previous created damage for selected area exist, a special modal controller must be launched, otherside
     user will be straight redirected to damage creation*/
}

//Called after user terminate the insertion of a damage
-(void)damageNotified:(Damage *)damage{
    
    
    NSMutableArray* macroAreaDamages = [self getListForMacroArea:damage.macro];
    
    //ADD new damage only if not yet contained
    if(![macroAreaDamages containsObject:damage])
        [macroAreaDamages addObject:damage];
    
    [self updateMacroArea:damage.macro];
    
}

-(void) updateMacroArea:(MacroDamage *)macro{
    
    NSMutableArray* macroAreaDamages = [self getListForMacroArea:macro];
    
    //Is needed to be known which is the major damage seriousness in the area
    int maxSeriousnessLevel = 0;
    
    for(Damage * areaDamage in macroAreaDamages)
        if(areaDamage.level > maxSeriousnessLevel)
            maxSeriousnessLevel = areaDamage.level;
    
    [cell.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:
                                                          @"javascript:bridge.updateBlock(\"%@\",\"%d\");",macro.damageId,
                                                          maxSeriousnessLevel]];
}

#pragma mark DAMAGES MAP MANAGEMENT

-(NSMutableArray*) getListForMacroArea: (MacroDamage *)macro{
    
    //Damages map keep a list of all damages for each macro area
    NSMutableArray* macroAreaDamages;
    macroAreaDamages = [damagesMap objectForKey: macro.damageId];
    
    //If area is not yet created, create it and add to the map
    if(macroAreaDamages == nil){
        macroAreaDamages = [NSMutableArray new];
        [damagesMap setObject:macroAreaDamages forKey:macro.damageId];
    }
    
    return macroAreaDamages;
}


#pragma mark - TABLE VIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    tableView.allowsSelection = NO;
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
        tableCell.backgroundColor = [UIColor clearColor];
        if(self.isViewMode)
            tableCell.hidden = YES;
        return tableCell;
    }
    else if(indexPath.row == 1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.delegate = self;
        
        if(self.isViewMode)
            cell.cellTitle.text = AMLocalizedString(@"DamagesViewModeTitle", @"");
        
        [cell.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:
                                                              @"javascript:bridge.updateBlock(\"1\",\"2\");"]];
        return cell;
    }
    else{
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"ContinueCell"];
        if(self.isViewMode)
            tableCell.hidden = YES;
        tableCell.backgroundColor = [UIColor clearColor];
        
        return tableCell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        if(self.isViewMode)
            return 0;
        return 280;
    }
    else if(indexPath.row == 1){
        return 600;
    }
    else{
        if(self.isViewMode)
            return 0;
        return 65;
    }
}

@end
