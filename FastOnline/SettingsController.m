//
//  SettingsController.m
//  FastOnline
//
//  Created by Nico Sordoni on 17/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "SettingsController.h"
#import "UIImage+Overlay.h"
#import "ColorUtil.h"
#import "LeftMenuController.h"

@interface SettingsController ()

@end

@implementation SettingsController{
    NSMutableArray* controllers;
    NSArray* icons;
    PageMenuIcon* selectedIcon;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    controllers = [[NSMutableArray alloc] init];
    
    icons = @[self.loginIcon, self.userIcon, self.appIcon];
    [self setSelectedItemAtPosition:0];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageVC"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = false;
    
    UIViewController* controller = [self getControllerForPosition:0];
    [self.pageViewController setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    // Change the size of page view controller
    self.pageViewController.view.frame = self.menu_cointainer.bounds;
    
    [self addChildViewController:self.pageViewController];
    [self.menu_cointainer addSubview:self.pageViewController.view];
    
    
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //Enable left menu (may have been disabled from push)
    [LeftMenuController shared].disabled=false;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [LeftMenuController shared].disabled=true;
    
}


-(void) setSelectedItemAtPosition: (int) position{
    if(selectedIcon!=nil){
        selectedIcon.backgroundColor = [UIColor whiteColor];
        selectedIcon.image.image = [selectedIcon.image.image imageWithColor:[ColorUtil getRgbColorRed:155 green:155 blue:155 alpha:1]];
    }
    
    PageMenuIcon* icon =  (PageMenuIcon*) icons[position];
    icon.backgroundColor = [UIColor clearColor];
    icon.image.image = [icon.image.image imageWithColor:[ColorUtil getRgbColorRed:4 green:154 blue:253 alpha:1]];
    
    selectedIcon = icon;
}



-(UIViewController*) getControllerForPosition: (int) position{
    
    
    if(controllers.count>position)
        return [controllers objectAtIndex:position];
    
    
    UIViewController* controller = nil;
    
    switch (position) {
        case 0:
            controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsPwdVc"];
            break;
        case 1:
            
            controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsUserVC"];
            break;
        case 2:
            
            controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsAppVC"];
            break;
        default:
            break;
    }
    
    controller.view.tag = position;
    [controllers addObject:controller];
    
    
    return controller;
}


-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if(completed){
        UIViewController* controller = [pageViewController.viewControllers objectAtIndex:0];
        int position = controller.view.tag;
        
        [self setSelectedItemAtPosition:position];
    }
}


-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    int position = viewController.view.tag;
    
    if(position==0 || position==1){
        return [self getControllerForPosition:position+1];
    }
    
    return nil;
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    int position = viewController.view.tag;
    
    if(position==1 || position==2){
        return [self getControllerForPosition:position-1];
    }
    
    return nil;
}
@end
