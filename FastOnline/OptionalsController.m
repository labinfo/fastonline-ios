//
//  OptionalsController.m
//  FastOnline
//
//  Created by Nico Sordoni on 28/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "OptionalsController.h"
#import "ImpostazioniManager.h"
#import "Optional.h"
#import "OptionalCell.h"

#define KEY_PATH_SELECTED @"selected"

@interface OptionalsController ()

@end

@implementation OptionalsController{
    NSMutableArray* optionals;
    NSMutableArray* selectedOptionals;
    NSMutableArray* observedOptionals;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
}

-(void) loadData{
    
    //If selected optionals is not nil, data has been loaded from disk
    if(selectedOptionals == nil)
    {
        observedOptionals = [NSMutableArray new];
        selectedOptionals = [NSMutableArray new];
        
        NSString* fileName = [[ImpostazioniManager get] getListDataFileName];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
        NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
        id allValues = [[NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil] objectForKey:@"equipaggiamento"];
        
        optionals=[[NSMutableArray alloc] init];
        for (id value in allValues) {
            Optional * optional = [[Optional alloc] initWithName:value];
            if(!self.isViewMode){
                [optional addObserver:self forKeyPath:KEY_PATH_SELECTED options:NSKeyValueObservingOptionNew context:nil];
                [observedOptionals addObject:optional];
            }
            [optionals addObject : optional];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)pageControllerDidExit{
    for(Optional* optional in observedOptionals){
        @try {
            [optional removeObserver:self forKeyPath:KEY_PATH_SELECTED];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    Optional *optional = object;
    
    if(optional.selected)
        [selectedOptionals addObject:optional];
    else
        [selectedOptionals removeObject:optional];
}

-(void)addOptional:(id)sender{
    AddOptionalAlertController *addController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddOptionalVC"];
    
    addController.optionalDelegate = self;
    
    [self.pageController presentTransparentModalViewController:addController animated:YES withAlpha:1];
}

//User created a new optional
-(void)optionalSelected:(NSString *)name{
    
    Optional * optional = [[Optional alloc] initWithName:name];
    
    //New created optionals are selected for default
    optional.selected = YES;
    
    if(!self.isViewMode){
        [optional addObserver:self forKeyPath:KEY_PATH_SELECTED options:NSKeyValueObservingOptionNew context:nil];
        [observedOptionals addObject:optional];
    }
    
    [optionals addObject : optional];
    
    [selectedOptionals addObject:optional];
    
    [self.table reloadData];
}

#pragma mark - PERSISTENT DATA MANAGEMENT

-(id)getData{
    NSMutableArray* data = [NSMutableArray new];
    
    [data addObject:optionals];
    [data addObject:selectedOptionals];
    
    return data;
}

-(void)loadData:(id)data{
    NSMutableArray* array = data;
    
    optionals = [array objectAtIndex:0];
    selectedOptionals = [array objectAtIndex:1];
}

#pragma mark - TABLE VIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    tableView.allowsSelection = NO;
    
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(self.isViewMode)
        return 1 + optionals.count;
    
    //Header + optionalsList + Add button + Continue button
    return 3 + optionals.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self isCellHeader:indexPath]){
        
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
        tableCell.backgroundColor = [UIColor clearColor];
        
        return tableCell;
        
    }
    else if([self isCellOptional: indexPath]){
        
        
        OptionalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OptionalCell"];
        cell.isViewMode = self.isViewMode;
        [cell setOptional:optionals[indexPath.row - 1]];
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    else if([self isCellAddButton:indexPath])
    {
        
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"AddButton"];
        return tableCell;
    }
    else{
        UITableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"ContinueCell"];
        tableCell.backgroundColor = [UIColor clearColor];
        return tableCell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self isCellHeader:indexPath]){
        return 70;
    }
    else if([self isCellOptional: indexPath]){
        return 44;
    }
    else if([self isCellAddButton:indexPath])
    {
        return 80;
    }
    return 90;
}

#pragma mark CELLS RECOGNITION

-(BOOL) isCellHeader:(NSIndexPath *)indexPath{
    return indexPath.row == 0;
}

-(BOOL) isCellOptional:(NSIndexPath *)indexPath{
    return indexPath.row <= optionals.count;
}

-(BOOL) isCellAddButton:(NSIndexPath *)indexPath{
    return indexPath.row == optionals.count + 1;
}


-(BOOL) isCellContinueButton:(NSIndexPath *)indexPath{
    return indexPath.row > optionals.count + 1;
}

@end
