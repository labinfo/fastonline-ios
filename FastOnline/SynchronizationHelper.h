//
//  SynchronizationHelper.h
//  FastOnline
//
//  Created by Nico Sordoni on 10/10/15.
//  Copyright © 2015 Gruppo Filippetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseController.h"
#import "SSUListSynchronizer.h"

#define SYNC_COMPLETED_ALERT_ID 2048

@interface SynchronizationHelper : NSObject

+(void) showSynchronizationAlertWithCompleted:(int)receivedSSU andFailed:(int)failedSSU inController:(BaseController*) controller;

@end
