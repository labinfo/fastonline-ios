//
//  UIImage+Overlay.h
//  Hotel
//
//  Created by Matteo Corradin on 14/04/14.
//  Copyright (c) 2014 Matteo Corradin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Overlay)

- (UIImage *)imageWithColor:(UIColor *)color1;

@end
