//
//  StatusCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Status.h"

@interface StatusCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *statusName;
@property (strong, nonatomic) IBOutlet UIImageView *statusImage;

-(void)setStatus:(Status*) status;
@end
