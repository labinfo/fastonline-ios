//
//  HomeNavigationController.h
//  MyFirstFlash
//
//  Created by Nico Sordoni on 20/01/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IIViewDeckController.h"

@interface HomeNavigationController : UINavigationController<UINavigationBarDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) IBOutlet IIViewDeckController *viewDeckController;

@end
