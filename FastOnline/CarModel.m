//
//  CarModel.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarModel.h"
#import "CarVersion.h"

@implementation CarModel

-(id)initWithData:(id)data{
    self = [super init];
    
    if(self){
        
        //id data = [[NilToDefaultConverter alloc] data andDefaultValue:@""];
        
        self->_modelId = [data objectForKey:@"id"];
        self->_name = [data objectForKey:@"name"];
        
        id allVersions=[data objectForKey:@"versions"];
        
        NSMutableArray* versions=[[NSMutableArray alloc] init];
        for (id versionData in allVersions) {
            [versions addObject:[[CarVersion alloc] initWithData:versionData]];
        }
        
        self->_versions = [[NSArray alloc] initWithArray:versions];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self->_modelId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_versions forKey:@"versions"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_modelId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_versions = [decoder decodeObjectForKey:@"versions"];
    }
    return self;
}


@end
