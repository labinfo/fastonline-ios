//
//  SynchronizationHelper.m
//  FastOnline
//
//  Created by Nico Sordoni on 10/10/15.
//  Copyright © 2015 Gruppo Filippetti. All rights reserved.
//

#import "SynchronizationHelper.h"
#import "LocalizationSystem.h"

@implementation SynchronizationHelper

+(void)showSynchronizationAlertWithCompleted:(int)receivedSSU andFailed:(int)failedSSU inController:(BaseController *)baseController{
    [baseController showErrorAlertWithMessage: [NSString stringWithFormat:AMLocalizedString(@"SSUSent", @""), receivedSSU + failedSSU, receivedSSU, failedSSU]
                                      alertId:SYNC_COMPLETED_ALERT_ID style: failedSSU == 0 ? ALERT_STYLE_SUCCESS : ALERT_STYLE_NORMAL];
    
    
}
@end
