//
//  SSU.m
//  FastOnline
//
//  Created by Nico Sordoni on 06/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "SSU.h"
#import "LocalizationSystem.h"

@implementation SSU

-(SSUDetail *)getDetail{
    SSUDetail* detail = [SSUDetail new];
    
    detail.localID = self.localID;
    detail.title = self.carData.plateNumber ? self.carData.plateNumber : AMLocalizedString(@"Unknown", @"");
    
    detail.carMakerName = self.carData.maker ? self.carData.maker.name : AMLocalizedString(@"Unknown", @"");
    detail.carModelName = self.carData.model ? self.carData.model.name : AMLocalizedString(@"Unknown", @"");
    detail.carVersionName = self.carData.version ? self.carData.version.name : AMLocalizedString(@"Unknown", @"");
    
    return detail;
}
@end
