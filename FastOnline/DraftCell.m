//
//  DraftCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 07/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DraftCell.h"
#import "ColorUtil.h"
#import "UIImage+Overlay.h"

@implementation DraftCell{
    BOOL menuOpened;
}

- (void)awakeFromNib {
    menuOpened = false;
}

-(void)dealloc{
    [self.isSelected removeObserver:self forKeyPath:@"boolValue"];
}

-(void)setIsSelected:(NSBoolean *)isSelected{
    
    [self.isSelected removeObserver:self forKeyPath:@"boolValue"];
    
    self->_isSelected = isSelected;
    [self.isSelected addObserver:self forKeyPath:@"boolValue" options:NSKeyValueObservingOptionNew context:nil];
    
    [self updateCheckBoxImage];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    [self updateCheckBoxImage];
    
    [self.delegate detail:self.SSUdetail selected:self.isSelected.boolValue];
}

-(void)updateCheckBoxImage
{
    
    if(self.isSelected.boolValue)
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox-select"];
    else
        self.checkBoxImage.image = [UIImage imageNamed:@"checkbox_empty"];
}

-(void)setSSUdetail:(SSUDetail *)detail{
    self->_SSUdetail = detail;
    
    self.titleLabel.text = detail.title;
    self.makerLabel.text = detail.carMakerName;
    self.modelLabel.text = detail.carModelName;
    self.versionLabel.text = detail.carVersionName;
    
    if(detail.status == STATUS_COMPLETED){
        self.circleImage.image = [self.circleImage.image imageWithColor:[ColorUtil getRgbColorRed:211 green:211 blue:211 alpha:1]];
    }
    else if(detail.status == STATUS_SENT){
        self.circleImage.image = [self.circleImage.image imageWithColor:[ColorUtil getRgbColorRed:27 green:207 blue:0 alpha:1]];
    }
    else{
        self.circleImage.hidden = YES;
    }
}

-(void)toggleSelection:(id)sender{
    self.isSelected.boolValue = !self.isSelected.boolValue;
}



-(void)openMenu:(id)sender{
    
    if(menuOpened){
        self.menuMargin.constant = -150;
    }
    else{
        self.menuMargin.constant = self.menuMarginValue;
        [self.delegate CellOpened:self];
    }
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self layoutIfNeeded];
                     }];
    
    menuOpened = !menuOpened;
}

-(void)closeMenu:(id)sender{
    
    self.menuMargin.constant = -150;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self layoutIfNeeded];
                     }];
    
    menuOpened = NO;
}

-(void)doModify:(id)sender{
    [self.delegate modifySSU:self.SSUdetail];
}
-(void)doDelete:(id)sender{
    [self.delegate deleteSSU:self.SSUdetail];
}

-(void)doSync:(id)sender{
    [self.delegate syncSSU:self.SSUdetail];
}
@end
