//
//  UserInfoCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UserInfoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *surnameField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;

-(void) setUser:(User*) user;

@end
