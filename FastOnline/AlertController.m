//
//  AlertController.m
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "AlertController.h"
#import "ColorUtil.h"
#import "UIImage+Overlay.h"

@interface AlertController ()

@end

@implementation AlertController{
    id<AlertActionsDelegate> actionsDelegate;
    id<AlertConfirmDelegate> confirmDelegate;
    int requestId;
}

-(void)dealloc{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if(self.style != ALERT_STYLE_SUCCESS)
    {
        self.style = ALERT_STYLE_NORMAL;
    }
    else
    {
        self.titleLabel.hidden = YES;
        self.iconImage.image = [[UIImage imageNamed:@"checkmark"] imageWithColor:[ColorUtil getRgbColorRed:26 green:201 blue:0 alpha:1]];
    }
    
    [self styleButton:self.okButton];
}

-(void)viewWillAppear:(BOOL)animated{
    if(actionsDelegate != nil){
        
        self.okView.hidden = YES;
        self.twoButtonsView.hidden = YES;
        self.threeButtonsView.hidden = NO;
        
        
        [self styleButton:self.deleteButton];
        [self styleButton:self.closeButton];
        [self styleButton:self.modifyButton];
    }
    else if(confirmDelegate != nil){
        
        self.okView.hidden = YES;
        self.twoButtonsView.hidden = NO;
        self.threeButtonsView.hidden = YES;
        
        [self styleButton:self.noButton];
        [self styleButton:self.yesButton];
    }
}

-(void)styleButton: (UIButton*) button{
    if(self.style == ALERT_STYLE_SUCCESS){
        
        button.layer.borderWidth= 2;
        button.layer.borderColor= [ColorUtil getRgbColorRed:26 green:201 blue:0 alpha:1].CGColor;
        button.layer.cornerRadius = 5;
        
        button.backgroundColor =  [ColorUtil getRgbColorRed:177 green:238 blue:168 alpha:1];
    }
    else
    {
        button.layer.borderWidth= 2;
        button.layer.borderColor= [ColorUtil getRgbColorRed:253 green:21 blue:0 alpha:1].CGColor;
        button.layer.cornerRadius = 5;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidDisappear:(BOOL)animated{
    confirmDelegate = nil;
    actionsDelegate = nil;
}

#pragma mark 2 BUTTONS

-(void)askConfirmForRequest:(int)reqId delegate:(id<AlertConfirmDelegate>)delegate{
    requestId = reqId;
    confirmDelegate = delegate;
}

-(void)yesPressed:(id)sender{
    [confirmDelegate requestConfirmed:requestId];
    [self chiudi:nil];
}

#pragma mark 3 BUTTONS

-(void)showActionsForRequest:(int)reqId delegate:(id<AlertActionsDelegate>)delegate{
    
    requestId = reqId;
    actionsDelegate = delegate;
    
}

-(IBAction)modifyPressed:(id)sender{
    [self chiudi:nil];
    [actionsDelegate modifyForRequest:requestId];
}
-(IBAction)deletePressed:(id)sender{
    [self chiudi:nil];
    [actionsDelegate deleteForRequest:requestId];
    
}

@end
