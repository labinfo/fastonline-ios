//
//  BasePageController.h
//  mff
//
//  Created by Nico Sordoni on 09/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContainerController.h"
#import "BaseController.h"

@class PageContainerController;

@interface BasePageController : BaseController

@property (nonatomic) int position;
@property(nonatomic,strong) PageContainerController* pageController;

-(IBAction)goAhead:(id)sender;

-(id) getData;
-(void) loadData:(id) data;

-(void) pageControllerDidExit;

-(BOOL) validationOk;


@end
