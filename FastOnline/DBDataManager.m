//
//  DBDataManager.m
//  FastOnline
//
//  Created by Nico Sordoni on 08/10/15.
//  Copyright © 2015 Gruppo Filippetti. All rights reserved.
//

#import "DBDataManager.h"
#import "LocalizationSystem.h"
#import <sqlite3.h>

static DBDataManager *sharedInstance = nil;
static sqlite3 *database = nil;

@implementation DBDataManager{
    
}


+ (DBDataManager *)shared {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[DBDataManager alloc] init];
            [sharedInstance createDB];
        }
    }
    return sharedInstance;
}

#pragma mark - INITIALIZATION

-(BOOL) createDB{
    NSString* dbPath = [self getDBPath];
    
    BOOL isSuccess = YES;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: dbPath ] == NO) {
        const char *dbpath = [dbPath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
            isSuccess = [self createSSUTable];
            return  isSuccess;
        }
        else {
            NSLog(@"Failed to open/create database");
            return NO;
        }
    }
    
    const char *dbpath = [dbPath UTF8String];
    if (sqlite3_open(dbpath, &database) != SQLITE_OK) {
        NSLog(@"%s",sqlite3_errmsg(database));
    }
    return isSuccess;
}

-(BOOL) createSSUTable{
    NSString* createQuery = [NSString stringWithFormat:
            @"CREATE TABLE IF NOT EXISTS `%@` (`%@`	TEXT, `%@`	TEXT, `%@`	TEXT, `%@`	TEXT, `%@`	TEXT, `%@`	INTEGER,`%@`	INTEGER,`%@`	INTEGER,`%@`	INTEGER, PRIMARY KEY(%@));"
            , TABLE_NAME_SSU, FIELD_LOCAL_ID, FIELD_PLATE_NUMBER, FIELD_MAKER, FIELD_MODEL, FIELD_VERSION, FIELD_STATUS, FIELD_COMPANY_ID, FIELD_SERVER_ID,
                             FIELD_COMPLETION_LEVEL, FIELD_LOCAL_ID];
    char *errMsg;
    const char *sql_stmt = [createQuery UTF8String];
    if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
        NSLog(@"Failed to create SSU table! Error:%s",errMsg);
        return NO;
    }
    return YES;
}

#pragma mark - DATA MANAGEMENT

-(BOOL)storeNewSSUId:(NSString *)localId{
    NSString *insertSQL = [NSString stringWithFormat:
                           @"INSERT INTO %@ (%@) VALUES (?)",
                           TABLE_NAME_SSU,
                           FIELD_LOCAL_ID];
    
    sqlite3_stmt *statement = nil;
    
    if (sqlite3_prepare_v2(database, [insertSQL UTF8String], -1, &statement, NULL) == SQLITE_OK) {
        
        sqlite3_bind_text(statement, 1, localId.UTF8String, -1, SQLITE_TRANSIENT);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else
        {
            NSLog(@"Error while executing new SSU ID insertion: %s",sqlite3_errmsg(database));
        }
    }
    else
    {
        NSLog(@"Error while preparing new SSU ID insertion: %s",sqlite3_errmsg(database));
    }
    
    return NO;
}

-(BOOL) updateSSU:(SSUDetail *)ssu{
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET %@=?,%@=?,%@=?,%@=?,%@=?,%@=?,%@=?,%@=? WHERE %@=?",
                           TABLE_NAME_SSU,
                           FIELD_MAKER,
                           FIELD_MODEL,
                           FIELD_VERSION,
                           FIELD_PLATE_NUMBER,
                           FIELD_STATUS,
                           FIELD_COMPANY_ID,
                           FIELD_SERVER_ID,
                           FIELD_COMPLETION_LEVEL,
                           FIELD_LOCAL_ID];
    
    sqlite3_stmt *statement = nil;

    if(sqlite3_prepare_v2(database, [updateSQL UTF8String], -1, &statement, NULL ) == SQLITE_OK) {
        sqlite3_bind_text(statement, 1, ssu.carMakerName.UTF8String, -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, ssu.carModelName.UTF8String, -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 3, ssu.carVersionName.UTF8String, -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 4, ssu.title.UTF8String, -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 5, ssu.status);
        sqlite3_bind_int(statement, 6, [ssu.companyID intValue]);
        
        int serverIdValue = ssu.serverID == nil ? -1 : [ssu.serverID intValue];
        sqlite3_bind_int(statement, 7, serverIdValue);
        
        sqlite3_bind_int(statement, 8, ssu.completionLevel);
        sqlite3_bind_text(statement, 9, ssu.localID.UTF8String, -1, SQLITE_TRANSIENT);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            NSLog(@"Error execute update chat: %s",sqlite3_errmsg(database));
        }
    }
    else {
        NSLog(@"Error prepare update chat: %s",sqlite3_errmsg(database));
    }
    
    
    return NO;
}

-(NSArray *)getSSUList{
    
    NSMutableArray* list = [NSMutableArray new];
    sqlite3_stmt *statement;
    
    NSString *querySQL = [NSString stringWithFormat: @"SELECT %@,%@,%@,%@,%@,%@,%@,%@,%@ FROM %@",
                          FIELD_LOCAL_ID,
                          FIELD_MAKER,
                          FIELD_MODEL,
                          FIELD_VERSION,
                          FIELD_PLATE_NUMBER,
                          FIELD_STATUS,
                          FIELD_COMPANY_ID,
                          FIELD_SERVER_ID,
                          FIELD_COMPLETION_LEVEL,
                          TABLE_NAME_SSU];
    
    if (sqlite3_prepare_v2(database, [querySQL UTF8String], -1, &statement, NULL) == SQLITE_OK)
    {
        
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            char *localIdChars = (char *) sqlite3_column_text(statement, 0);
            char *makerChars = (char *) sqlite3_column_text(statement, 1);
            char *modelChars = (char *) sqlite3_column_text(statement, 2);
            char *versionChars = (char *) sqlite3_column_text(statement, 3);
            char *plateNumChars = (char *) sqlite3_column_text(statement, 4);
            int status = sqlite3_column_int(statement, 5);
            int companyId = sqlite3_column_int(statement, 6);
            int serverId = sqlite3_column_int(statement, 7);
            int completionLevel = sqlite3_column_int(statement, 8);
            
            SSUDetail* ssuDetail = [SSUDetail new];
            ssuDetail.localID =[[NSString alloc] initWithUTF8String:localIdChars];
            ssuDetail.carMakerName = makerChars != nil ? [[NSString alloc] initWithUTF8String:makerChars] : AMLocalizedString(@"Unknown", @"");
            ssuDetail.carModelName = modelChars!= nil ? [[NSString alloc] initWithUTF8String:modelChars]: AMLocalizedString(@"Unknown", @"");
            ssuDetail.carVersionName = versionChars != nil ? [[NSString alloc] initWithUTF8String:versionChars]: AMLocalizedString(@"Unknown", @"");
            ssuDetail.title = plateNumChars!= nil ?[[NSString alloc] initWithUTF8String:plateNumChars]: AMLocalizedString(@"Unknown", @"");;
            ssuDetail.status = status;
            ssuDetail.companyID = [NSNumber numberWithInt: companyId];
            
            ssuDetail.serverID = serverId == -1 ? nil :[NSNumber numberWithInt: serverId];
            
            ssuDetail.completionLevel = completionLevel;
            
            [list addObject:ssuDetail];
        }
        sqlite3_finalize(statement);
    }
    else {
        NSLog(@"Error select chat: %s",sqlite3_errmsg(database));
    }
    
    return [NSArray arrayWithArray:list];
}

-(BOOL)deleteSSU:(SSUDetail *)ssu{
    BOOL success = false;
    sqlite3_stmt *statement = nil;
    NSString *deleteMessage = [NSString stringWithFormat:@"DELETE from %@ where %@ = ?",TABLE_NAME_SSU, FIELD_LOCAL_ID];
    sqlite3_prepare_v2(database, [deleteMessage UTF8String], -1, &statement, NULL);
    sqlite3_bind_text(statement, 1, ssu.localID.UTF8String, -1, SQLITE_TRANSIENT);
    if (sqlite3_step(statement) == SQLITE_DONE) {
        success = true;
    }        else {
        NSLog(@"Error execute delete message: %s",sqlite3_errmsg(database));
    }
    sqlite3_finalize(statement);
    return success;
}

#pragma mark - UTILS

-(NSString *) getDBPath{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    return [[NSString alloc] initWithString:
            [docsDir stringByAppendingPathComponent: DB_FILE_NAME]];
}

@end
