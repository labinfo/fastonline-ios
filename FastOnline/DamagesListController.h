//
//  DamagesListController.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "TrasparentebaseController.h"
#import "Damage.h"

@protocol DamagesListDelegate <NSObject>

-(void) damagePickedFromList:(Damage*) damage;
-(void) newDamageSelectedForMacroarea: (MacroDamage*) macro;

@end

@interface DamagesListController : TrasparentebaseController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) BOOL isViewMode;
@property (nonatomic, strong) MacroDamage *macroArea;
@property (nonatomic, strong) id<DamagesListDelegate> listDelegate;
@property (nonatomic, strong) NSArray *existingDamages;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *btnNewWidth;

-(IBAction)newDamage:(id)sender;

@end
