//
//  UserInfoControllerViewController.m
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "UserInfoController.h"
#import "AppDataManager.h"
#import "UserInfoCell.h"
#import "CompanyCell.h"

@interface UserInfoController ()

@end

@implementation UserInfoController{
    User* user;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.table.allowsSelection = NO;
    user = [[AppDataManager get] getUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==1){
        return 1;
    }
    else{
        return user.companies.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        static NSString *cellID = @"CompanyCell";
        CompanyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        [cell setCompany:[user.companies objectAtIndex:indexPath.row]];
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }
    else{
        
        static NSString *cellID = @"UserCell";
        UserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        [cell setUser: user];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0)
        return 310;
    else
        return 180;
}

@end
