//
//  Company.m
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "Company.h"
#import "NilToDefaultConverter.h"

@implementation Company
-(id) initWithData:(id)dataObject{
    self=[super init];
    if(self){
        
        id data;
        
        //Is necessary to manage the 2 structure that data may have
        if([dataObject objectForKey:@"res"])
            data = [dataObject objectForKey:@"res"];
        else
            data = dataObject;
        
        data = [[NilToDefaultConverter alloc] initWithId:data andDefaultValue:@""];
        
        self->_companyId = [data objectForKey:@"id"];
        self->_name = [data objectForKey:@"name"];
        self->_email = [data objectForKey:@"email"];
        self->_picture = [data objectForKey:@"picture"];
        self->_phone = [data objectForKey:@"phone"];
        self->_vatNumber = [data objectForKey:@"vatNumber"];
        self->_address = [[Address alloc] initWithStreet:[data objectForKey:@"address"] town:[data objectForKey:@"city"]
                                                  state:[data objectForKey:@"state"] zip:[data objectForKey:@"zip"]];
        
        self->_isUserAdmin =[[dataObject valueForKey:@"admin"] isEqual:[NSNumber numberWithBool:YES]];
        self->_isUserManager =[[dataObject valueForKey:@"manager"] isEqual:[NSNumber numberWithBool:YES]];
        self->_isUserCollaborator =[[dataObject valueForKey:@"collaborator"] isEqual:[NSNumber numberWithBool:YES]];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self->_companyId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_email forKey:@"email"];
    [encoder encodeObject:self->_phone forKey:@"phone"];
    [encoder encodeObject:self->_picture forKey:@"picture"];
    [encoder encodeObject:self->_address forKey:@"address"];
    [encoder encodeObject:self->_vatNumber forKey:@"vat"];
    
    [encoder encodeBool:self->_isUserAdmin forKey:@"admin"];
    [encoder encodeBool:self->_isUserCollaborator forKey:@"collaborator"];
    [encoder encodeBool:self->_isUserManager forKey:@"manager"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        
        self->_companyId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_email = [decoder decodeObjectForKey:@"email"];
        self->_phone = [decoder decodeObjectForKey:@"phone"];
        self->_picture = [decoder decodeObjectForKey:@"picture"];
        self->_address = [decoder decodeObjectForKey:@"address"];
        self->_vatNumber = [decoder decodeObjectForKey:@"vat"];
        
        self->_isUserAdmin = [decoder decodeBoolForKey:@"admin"];
        self->_isUserCollaborator = [decoder decodeBoolForKey:@"collaborator"];
        self->_isUserManager = [decoder decodeBoolForKey:@"manager"];
    }
    return self;
}
@end
