//
//  ListSelectCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListSelectCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *itemTextLabel;

@end
