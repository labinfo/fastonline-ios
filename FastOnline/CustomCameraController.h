//S
//  CustomCameraController.h
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "ShapePickerController.h"

@protocol CustomCameraDelegate <NSObject>

-(void)closeCamera;
-(void)shootPicture;

@end
@interface CustomCameraController : BaseController<ShapePickerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *exitText;

@property (strong, nonatomic) IBOutlet UIImageView *frontShape;
@property (strong, nonatomic) IBOutlet UIImageView *rearShape;
@property (strong, nonatomic) IBOutlet UIImageView *logo;

@property (strong, nonatomic) IBOutlet UIButton *prova;

@property (strong, nonatomic) IBOutlet UIView *cameraButtonView;
@property (strong, nonatomic) IBOutlet UIButton *cameraButton;
@property (strong, nonatomic) IBOutlet UIImageView *cameraButtonImage;

@property (strong, nonatomic) IBOutlet UIImageView *shapesButtonImage;
@property (strong, nonatomic) IBOutlet UIButton *shapesButton;
@property (strong, nonatomic) IBOutlet UIButton *shapesButton2;

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (strong, nonatomic)  id<CustomCameraDelegate> cameraDelegate;



-(IBAction)takePicture:(id)sender;
-(IBAction)closeCamera:(id)sender;
-(IBAction)chooseOverlay:(id)sender;

@end
