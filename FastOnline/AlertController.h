//
//  AlertController.h
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "TrasparentebaseController.h"

#define ALERT_STYLE_NORMAL 1
#define ALERT_STYLE_SUCCESS 2

typedef int AlertStyle;


@protocol AlertActionsDelegate <NSObject>

-(void) modifyForRequest: (int) requestId;
-(void) deleteForRequest: (int) requestId;

@end

@protocol AlertConfirmDelegate

-(void) requestConfirmed: (int)requestId;

@end

@interface AlertController : TrasparentebaseController

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) IBOutlet UILabel* messageLabel;
@property (nonatomic, strong) IBOutlet UIButton* okButton;

@property (nonatomic, strong) IBOutlet UIView* okView;
@property (nonatomic, strong) IBOutlet UIView* threeButtonsView;
@property (nonatomic, strong) IBOutlet UIView* twoButtonsView;

@property (nonatomic, strong) IBOutlet UIButton* deleteButton;
@property (nonatomic, strong) IBOutlet UIButton* modifyButton;
@property (nonatomic, strong) IBOutlet UIButton* closeButton;

@property (nonatomic, strong) IBOutlet UIButton* yesButton;
@property (nonatomic, strong) IBOutlet UIButton* noButton;

@property (nonatomic) AlertStyle style;


-(IBAction)yesPressed:(id)sender;

-(IBAction)modifyPressed:(id)sender;
-(IBAction)deletePressed:(id)sender;

-(void) showActionsForRequest: (int)requestId delegate: (id<AlertActionsDelegate>) delegate;

-(void) askConfirmForRequest: (int)requestId delegate: (id<AlertConfirmDelegate>) delegate;
@end
