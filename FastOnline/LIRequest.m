//
//  LIRequest.m
//
//  Created by Mirko Ravaioli on 05/09/14.
//  Copyright (c) 2014 Mirko Ravaioli. All rights reserved.
//

#import "LIRequest.h"

@implementation LIRequest {
    void (^_success)(id response);
    void (^_failure)(id response);
    void (^_validateResponseError)(NSInteger error);
}

-(void)setDelegate:(id<LIRequestDelegate>)delegate {
    __weak LIRequest *this = self;
    [self setSuccess:^(id response) {
        [delegate success:this response:response];
    }];
    [self setFailure:^(id responseObject) {
        [delegate failure:this responseObject:responseObject];
    }];
    [self setValidateResponseError:^(NSInteger error) {
        [delegate validateResponseError:this error:error];
    }];
}

- (void)addObserverSuccess:(NSString*)name {
    [self addObserverSuccess:name anObject:nil];
}
- (void)addObserverSuccess:(NSString*)name anObject:(id)object{
    [self setSuccess:^(id response) {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:response forKey:@"response"];
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:object userInfo:userInfo];
    }];
}
- (void)addObserverFailure:(NSString*)name {
    [self addObserverFailure:name anObject:nil];
}
- (void)addObserverFailure:(NSString*)name anObject:(id)object{
    [self setFailure:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:object];
    }];
}
- (void)addObserverValidateResponseError:(NSString*)name {
    [self addObserverValidateResponseError:name anObject:nil];
}
- (void)addObserverValidateResponseError:(NSString*)name anObject:(id)object{
    [self setValidateResponseError:^(NSInteger error) {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:(int)error] forKey:@"error"];
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:object userInfo:userInfo];
    }];
}

- (void)setSuccess:(void (^)(id response))success {
    _success = success;
}
- (void)setFailure:(void (^)(id response))failure {
    _failure = failure;
}
- (void)setValidateResponseError:(void(^)(NSInteger error))failure {
    _validateResponseError = failure;
}

- (void)callbackFailure:(id)response {
    _failure(response);
}
- (void)callbackSuccess:(id)response {
    _success(response);
}
- (void)callbackValidateResponseError:(NSInteger)error withObject: (id)responseObj {
    if (_validateResponseError) {
        _validateResponseError(error);
    }
    else {
        [self callbackFailure: responseObj];
    }
}
@end
