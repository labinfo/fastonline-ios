//
//  CarPicturesController.m
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarPicturesController.h"
#import "CustomCameraController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "StorageManager.h"

#import "FileSystemInfo.h"
#import "Config.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "MBProgressHUD.h"
#import "Device.h"
#import "ImageCropView.h"
#import "ImageCropController.h"
#import "LocalizationSystem.h"
#import "CarPicsCell.h"
#import "CropperController.h"
#import "AppDataManager.h"
#import "NewSSUPageController.h"

@interface CarPicturesController ()

@end

@implementation CarPicturesController{
    UIImagePickerController *globalPicker;
    CustomCameraController* controller;
    UIButton *buttonClick, *buttonCancel;
    UIView* overlayView;
    CameraControllerInvoker* invoker;
    //Required pictures
    BOOL frontPicked, rearPicked, platePicked;
    
    NSArray* carImageContainers;
    
}

-(void)dealloc{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"";
    
    invoker = [CameraControllerInvoker new];
    invoker.controller = self;
    invoker.delegate = self;
    
    frontPicked = rearPicked = platePicked = NO;
    
    if(self.data == nil)
        self.data = [CarPictures new];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.title = @"";
}

-(void)viewDidDisappear:(BOOL)animated{
    
     
}

-(void)pageControllerDidExit{
    
    for(FOCarImageContainer* container in carImageContainers)
    {
        container.pictureController = nil;
    }
    carImageContainers = nil;
    
    [invoker clear];
    invoker.controller = nil;
    invoker.delegate = nil;
    invoker =nil;
    
    self.data.images = nil;
    self.data = nil;
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)validationOk{
    
    //View mode should never lock user, so any check is not necessary
    if(self.isViewMode)
        return true;
    
    NSMutableString* errorMsg=[[NSMutableString alloc] init];
    
    
    if(!frontPicked){
        [errorMsg appendString:AMLocalizedString(@"MissingFrontPicture", @"")];
        [errorMsg appendString:@"\n"];
    }
    if(!rearPicked){
        [errorMsg appendString:AMLocalizedString(@"MissingRearPicture", @"")];
        [errorMsg appendString:@"\n"];
    }
    if(!platePicked){
        [errorMsg appendString:AMLocalizedString(@"MissingPlatePicture", @"")];
        [errorMsg appendString:@"\n"];
    }
    if(errorMsg.length>0){
        [self.pageController showErrorAlertWithMessage:errorMsg];
        return false;
    }
    
    return true;
}

#pragma mark - DATA

-(id)getData{
    
    NSMutableArray* imgs = [NSMutableArray new];
    for(FOCarImageContainer* container in carImageContainers)
    {
        if(container.image)
            [imgs addObject:container.image];
        else
            [imgs addObject:[NSNull null]];
    }
    
    self.data.images = imgs;
    
    return self.data;
}

-(void)loadData:(id)data{
    self.data = data;
}


#pragma mark - TABLE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    self.table.allowsSelection = NO;
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row ==0){
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
        cell.backgroundColor = [UIColor clearColor];
        
        if(self.isViewMode)
            cell.hidden = YES;
        
        return cell;
    }
    else if (indexPath.row == 1){
        CarPicsCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"PicsCell"];
        
        tableCell.backgroundColor = [UIColor clearColor];
        
        carImageContainers = @[tableCell.carImage1, tableCell.carImage2, tableCell.carImage3, tableCell.carImage4, tableCell.carImage5, tableCell.carImage6, tableCell.carImage7, tableCell.carImage8];
        
        
        int i=0;
        for(FOCarImageContainer* container in carImageContainers)
        {
            container.isViewMode = self.isViewMode;
            container.pictureController = self;
            if(self.data.images.count>0){
                if( self.data.images[i] && self.data.images[i] != (id)[NSNull null]){
                    
                    if(i == 0)
                        frontPicked = YES;
                    else if (i == 1)
                        rearPicked = YES;
                    else if (i == 3)
                        platePicked = YES;
                    
                    container.image = self.data.images[i];
                }
            }
            i++;
        }
        
        
        return tableCell;
    }else{
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ConfirmCell"];
        cell.backgroundColor = [UIColor clearColor];
        
        if(self.isViewMode)
            cell.hidden = YES;
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row ==0){
        return self.isViewMode ? 0 : 180;
    }
    else if (indexPath.row == 1){
        return 720;
    }else{
        return self.isViewMode ? 0 : 65;
    }
}

-(IBAction)selectArrivalDate:(id)sender{
    
    
    //[self.pageController presentViewController:camera animated:YES completion:nil];
    
}



#pragma mark - UIIMAGEPICKER

#pragma mark ACTIONS

-(void)pickFront3Q:(id)sender{
}


-(void)onPictureEdited:(UIImage *)image forId:(int)requestId{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if(requestId == 0)
        frontPicked = YES;
    else if (requestId == 1)
        rearPicked = YES;
    else if (requestId == 3)
        platePicked = YES;
    
    FOCarImageContainer *container = carImageContainers[requestId];
    container.image = image;
 
    //Force data storage for each picture taken
    NewSSUPageController* pageController = (NewSSUPageController*) self.pageController;
    [pageController forceDataStorageForController:self];
}

-(void)onEditAbortedforId:(int)requestId{
    
}

#pragma mark CAMERA INVOCATIONS


-(void)takePictureAtIndex:(int)index{
    
    
    [invoker takePictureForRequest:index];
}



@end
