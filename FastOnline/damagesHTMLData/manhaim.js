var bridge = {
	onLoad: function() {
        
		//app.onLoad();

    },
	blockSelected: function(id) {

		//app.blockSelected(id);
	},
	updateBlock: function(id, color) {
        
		$('.overlay-area[data-id=' + id + ']').remove();
		var bg = $('<div class="overlay-area layer" />');
		$('.overlay').append(bg);
		bg.css({backgroundImage: 'url(\'' + color + '_' + id  + '.png\')'});
	}
};

$(function(){
	var aree = [
		{
			id: '1',
			css: {
				left: '25px',
				top: '139px',
				width: '74px',
				height: '146px'
			}
		},
		{
			id: '1',
			css: {
				left: '136px',
				top: '158px',
				width: '101px',
				height: '109px'
			}
		},
		{
			id: '1',
			css: {
				left: '249px',
				top: '156px',
				width: '90px',
				height: '111px'
			}
		},
		{
			id: '1',
			css: {
				left: '240px',
				top: '156px',
				width: '28px',
				height: '111px'
			}
		},
		{
			id: '1',
			css: {
				left: '267px',
				top: '159px',
				width: '30px',
				height: '105px'
			}
		},
		{
			id: '9',
			css: {
				left: '397px',
				top: '161px',
				width: '62px',
				height: '102px'
			}
		},
		{
			id: '9',
			css: {
				left: '460px',
				top: '164px',
				width: '43px',
				height: '97px'
			}
		},
		{
			id: '9',
			css: {
				left: '528px',
				top: '132px',
				width: '85px',
				height: '155px'
			}
		},
		{
			id: '4',
			css: {
				left: '297px',
				top: '166px',
				width: '100px',
				height: '91px'
			}
		},
		{
			id: '2',
			css: {
				left: '135px',
				top: '16px',
				width: '104px',
				height: '80px'
			}
		},
		{
			id: '2',
			css: {
				left: '239px',
				top: '37px',
				width: '98px',
				height: '58px'
			}
		},
		{
			id: '2',
			css: {
				left: '243px',
				top: '95px',
				width: '97px',
				height: '31px'
			}
		},
		{
			id: '2',
			css: {
				left: '152px',
				top: '142px',
				width: '188px',
				height: '14px'
			}
		},
		{
			id: '2',
			css: {
				left: '298px',
				top: '156px',
				width: '45px',
				height: '10px'
			}
		},
		{
			id: '6',
			css: {
				left: '241px',
				top: '16px',
				width: '152px',
				height: '21px'
			}
		},
		{
			id: '5',
			css: {
				left: '241px',
				top: '390px',
				width: '150px',
				height: '20px'
			}
		},
		{
			id: '13',
			css: {
				left: '249px',
				top: '439px',
				width: '148px',
				height: '63px'
			}
		},
		{
			id: '10',
			css: {
				left: '244px',
				top: '502px',
				width: '155px',
				height: '24px'
			}
		},
		{
			id: '10',
			css: {
				left: '213px',
				top: '439px',
				width: '34px',
				height: '151px'
			}
		},
		{
			id: '3',
			css: {
				left: '134px',
				top: '334px',
				width: '106px',
				height: '77px'
			}
		},
		{
			id: '3',
			css: {
				left: '238px',
				top: '331px',
				width: '97px',
				height: '59px'
			}
		},
		{
			id: '3',
			css: {
				left: '241px',
				top: '299px',
				width: '102px',
				height: '33px'
			}
		},
		{
			id: '3',
			css: {
				left: '149px',
				top: '268px',
				width: '190px',
				height: '15px'
			}
		},
		{
			id: '3',
			css: {
				left: '294px',
				top: '256px',
				width: '51px',
				height: '12px'
			}
		},
		{
			id: '7',
			css: {
				left: '335px',
				top: '332px',
				width: '172px',
				height: '60px'
			}
		},
		{
			id: '7',
			css: {
				left: '390px',
				top: '390px',
				width: '92px',
				height: '20px'
			}
		},
		{
			id: '7',
			css: {
				left: '342px',
				top: '296px',
				width: '118px',
				height: '36px'
			}
		},
		{
			id: '7',
			css: {
				left: '460px',
				top: '322px',
				width: '46px',
				height: '11px'
			}
		},
		{
			id: '7',
			css: {
				left: '339px',
				top: '261px',
				width: '164px',
				height: '26px'
			}
		},
		{
			id: '12',
			css: {
				left: '247px',
				top: '525px',
				width: '146px',
				height: '63px'
			}
		},
		{
			id: '8',
			css: {
				left: '338px',
				top: '37px',
				width: '168px',
				height: '62px'
			}
		},
		{
			id: '8',
			css: {
				left: '342px',
				top: '98px',
				width: '121px',
				height: '31px'
			}
		},
		{
			id: '8',
			css: {
				left: '339px',
				top: '141px',
				width: '160px',
				height: '23px'
			}
		},
		{
			id: '8',
			css: {
				left: '392px',
				top: '15px',
				width: '113px',
				height: '22px'
			}
		}
	];
	for (var i = 0; i < aree.length; i++) {
		var a = $('<a class="area" data-id="' + aree[i].id + '" href="bridge://blockSelected/' + aree[i].id + '" />');
		$('.event-area').append(a);
		a.css(aree[i].css);
  //a.css({border: '1px solid #000'});
	}
  
  /*$('body').on('click', function() {
               alert(1);
  });*/


    /*$('body').on('click', '.area', function() {
		//bridge.blockSelected($(this).attr('data-id'));
	});*/

  /*$('.area').click(function(){
                   alert(1);
                   })*/
  
  location.href = "bridge://onLoad";
	bridge.onLoad();
});