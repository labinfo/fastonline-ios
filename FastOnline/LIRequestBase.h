//
//  LIRequestBase.h
//
//  Created by Laboratorio Informatico on 04/09/14.
//  Copyright (c) 2014 LabInfo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LIRequestBase : NSObject

- (id)initWithContentType:(NSString*)ct;
- (void)post:(NSString*)url;
- (void)post:(NSString*)url params:(NSDictionary*)params;
- (void)post:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody:(BOOL) json;

- (void)get:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody:(BOOL) json;
- (void)put:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody:(BOOL) json;

- (NSString*)parseUrl:(NSString*)url;
- (NSDictionary*)parseParams:(NSDictionary*)params;
- (NSInteger)validateResponse:(id)response;


- (void)callbackFailure:(id)response;
- (void)callbackSuccess:(id)response;
- (void)callbackValidateResponseError:(NSInteger)error withObject: (id)responseObj;
-(void)abortAllOperation;
-(void)caricaDaLocale;

@end
