//
//  ShapePickerController.h
//  FastOnline
//
//  Created by Nico Sordoni on 26/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "TrasparentebaseController.h"

@protocol ShapePickerDelegate <NSObject>

-(void)front3QPicked;
-(void)rear3QPicked;
-(void)noShapePicked;

@end
@interface ShapePickerController : TrasparentebaseController


@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) id<ShapePickerDelegate> shapeDelegate;

-(IBAction)shapePicked:(id)sender;

@end
