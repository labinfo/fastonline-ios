//
//  DraftsController.h
//  FastOnline
//
//  Created by Nico Sordoni on 07/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BaseController.h"
#import "DraftCell.h"
#import "SSUSynchronizer.h"
//#import "GSDropboxActivity.h"
//#import "GSDropboxUploader.h"


@interface DraftsController : BaseController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, DetailCellDelegate, SynchronizerDelegate,
AlertConfirmDelegate>//, ActivityDismissDelegate>


@property (strong, nonatomic) IBOutlet UITableView *table;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *actionsView;
@property (strong, nonatomic) IBOutlet UIView *searchBarView;

@property (strong, nonatomic) IBOutlet UIView *topViewDrafts;
@property (strong, nonatomic) IBOutlet UIView *topViewArchive;

@property (strong, nonatomic) IBOutlet UIView *backupConfirmView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *confirmTopConstraint,*confirmBottomConstraint;

@property (strong, nonatomic) IBOutlet UITextView *messageView;
@property (strong, nonatomic) IBOutlet UITextField *pwdField;
-(IBAction)backupAborted:(id)sender;
-(IBAction)backupLogin:(id)sender;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *supportViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *actionsViewHeight;

@property (nonatomic) BOOL isArchive;

-(IBAction)newSSU:(id)sender;
-(IBAction)startSearch:(id)sender;
-(IBAction)selectAll:(id)sender;
-(IBAction)openActions:(id)sender;
-(IBAction)deleteSelected:(id)sender;
-(IBAction)syncSelected:(id)sender;
-(IBAction)backupSelected:(id)sender;

@end
