//
//  TrasparentebaseController.h
//  KiloRaze
//
//  Created by Lab Info on 28/01/15.
//  Copyright (c) 2015 Lab Info. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ModalControlDelegate;


@interface TrasparentebaseController : UIViewController

@property (nonatomic) int modalId;
@property (strong,nonatomic) id<ModalControlDelegate> delegate;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *bottomLayoutConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *topLayoutConstraint;
@property (nonatomic) BOOL adaptToKeyboard;



-(void) dismissTransparentModalViewControllerAnimated:(BOOL) animated valore:(NSString*)valore andChiave:(NSString*)key;

- (IBAction)chiudi:(id)sender;

- (IBAction)conferma:(id)sender;

- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;
@end

@protocol ModalControlDelegate<NSObject>

-(void)modalClosed: (TrasparentebaseController*) modalController;

@end