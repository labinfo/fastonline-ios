//
//  Maintenance.h
//  FastOnline
//
//  Created by Nico Sordoni on 06/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Maintenance : NSObject<NSCoding>

@property (nonatomic, strong)  NSString *desc;
@property (nonatomic, strong)  NSDate *date;
@property (nonatomic)  int kmCrossed;

@end
