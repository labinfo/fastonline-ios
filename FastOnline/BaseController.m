//
//  BaseController.m
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BaseController.h"
#import "MBProgressHUD.h"
#import "ColorUtil.h"

@interface BaseController ()

@end

@implementation BaseController{
    MBProgressHUD* currentHud;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.transparentModalViewController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) presentTransparentModalViewController: (TrasparentebaseController *) aViewController
                                     animated: (BOOL) isAnimated
                                    withAlpha: (CGFloat) anAlpha{
    
    if(self.transparentModalViewController!=nil)
        return;
    
    self.transparentModalViewController = aViewController;
    if(!self.transparentModalViewController.delegate)
        self.transparentModalViewController.delegate = self;
    
    //[aViewController setCaller:self];
    UIView *view = aViewController.view;
    
    view.opaque = NO;
    view.alpha = anAlpha;
    [view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *each = obj;
        each.opaque = NO;
        each.alpha = anAlpha;
    }];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    
    if (isAnimated) {
        view.frame = [[UIScreen mainScreen] bounds];
        //[self.navigationController setNavigationBarHidden:true animated:true];
        view.alpha = 0.0;
        [self.view addSubview:view];
        [UIView animateWithDuration:0.3
                         animations:^{
                             view.alpha = 1.0;
                         } completion:^(BOOL finished) {
                             //nop
                         }];
        
    }else{
        //[self.navigationController setNavigationBarHidden:true animated:true];
        view.frame = [[UIScreen mainScreen] bounds];
        [self.view addSubview:view];
    }
}

-(void)dismissKeyboard{
    [self.view endEditing:YES];
}

-(void)showMessage:(NSString *)message withTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void) showProgressWithMessage: (NSString*) message{
    [self showProgressWithMessage:message detailText:@"" callbackOnTap:nil];
}

-(void) showProgressWithMessage: (NSString*) message detailText:(NSString*) detail callbackOnTap:(void (^)(void)) callback {
    if (currentHud != nil) {
        return;
    }
    //self.viewDeckController.panningMode = IIViewDeckNoPanning;
    currentHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    currentHud.color = [ColorUtil getRgbColorRed:128 green:128 blue:128 alpha:0.9];
    // Gradiente
    currentHud.dimBackground = YES;
    

    /*if (message == nil || message.length == 0) {
        currentHud.labelText = @"Loading...";
    }
    if (message != nil && message.length == 0) {
        message = nil;
    }
    */
    currentHud.labelText = message;
    
    if(detail.length > 0)
        currentHud.detailsLabelText = detail;
    
    if(callback != nil)
        [currentHud setCallbackOnTap:callback];
}

-(void)changeProgressMessage:(NSString*) message andDetailText:(NSString*) detailText{
    if(currentHud == nil){
        return;
    }
    
    if(message.length > 0)
        currentHud.labelText = message;
    
    if(detailText.length > 0)
        currentHud.detailsLabelText = detailText;
    
    
}

-(void) hideProgress{
    [currentHud hide:true];
    currentHud = nil;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self dismissKeyboard];
}

-(void)showErrorAlertWithMessage:(NSString *)message{
    [self showErrorAlertWithMessage:message alertId:0];
}

-(void)setFastOnlineNavBar{
    //Add trash can button to navBar
    
    
    
    // set left barButtonItem to backButton
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 35, 35); // custom frame
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(onBackClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *logoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    logoButton.frame = CGRectMake(0, 0, 140, 40); // custom frame
    
    
    [logoButton setImage:[UIImage imageNamed:@"logo_manheim.png"] forState:UIControlStateNormal];
    [logoButton setImage:[UIImage imageNamed:@"logo_manheim.png"] forState:UIControlStateHighlighted];
    
    logoButton.showsTouchWhenHighlighted = NO;
    logoButton.highlighted = NO;
    
    UIBarButtonItem* logo = [[UIBarButtonItem alloc] initWithCustomView:logoButton];
    
    
    self.navigationItem.leftBarButtonItems =@[[[UIBarButtonItem alloc] initWithCustomView:backButton],logo];
}

-(void)onBackClicked{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showActionsAlertWithMessage:(NSString*) message request:(int)requestId delegate: (id<AlertActionsDelegate>) delegate{
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    alertController.style = ALERT_STYLE_NORMAL;
    
    [alertController showActionsForRequest:requestId delegate:delegate];
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    alertController.messageLabel.text = message;
    
}
-(void)showErrorAlertWithMessage: (NSString*) message alertId:(int) alertId{
    
    [self showErrorAlertWithMessage:message alertId:alertId style:ALERT_STYLE_NORMAL];
}

-(void)showErrorAlertWithMessage: (NSString*) message alertId:(int) alertId style:(AlertStyle)style{
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    
    alertController.style = style;
    
    alertController.modalId = alertId;
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    
    
    alertController.messageLabel.text = message;
}


-(void)modalClosed:(TrasparentebaseController *)modalController{

    self.transparentModalViewController = nil;
    [self.navigationController.navigationBar setUserInteractionEnabled:YES];
}

@end
