//
//  MaintenanceCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "MaintenanceCell.h"
#import "OnlyNumberInputValidator.h"

@implementation MaintenanceCell{
    OnlyNumberInputValidator * validator;
}

- (void)awakeFromNib {
    validator = [[OnlyNumberInputValidator alloc] init];
    
    [self restrictToNumbers:self.kmField];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(IBAction)selectDate:(id)sender{
    
    self.datePicker = [self.mainController.storyboard instantiateViewControllerWithIdentifier:@"DatePickerVC"];
    
    [self.datePicker pickDateWithTextField:self.dateField containerController:self.mainController];
    
}

-(void)setMaintentance:(Maintenance*)maintenance{
    if(maintenance.kmCrossed > 0)
    self.kmField.text = [NSString stringWithFormat:@"%d", maintenance.kmCrossed];
    
    self.datePicker.selectedDate = maintenance.date;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    self.dateField.text = [formatter stringFromDate:maintenance.date];
    
    self.descField.text = maintenance.desc;
    
}

-(void) restrictToNumbers:(UITextField*) field{
    field.keyboardType = UIKeyboardTypeDecimalPad;
    field.delegate = validator;
    
}

@end
