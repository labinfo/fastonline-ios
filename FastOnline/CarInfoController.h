//
//  CarInfoController.h
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"
#import "ListSelectController.h"
#import "CarData.h"

@interface CarInfoController : BasePageController<UITableViewDataSource, UITableViewDelegate, ListDataSource, ListDelegate, AlertActionsDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) CarData *carData;

-(IBAction)selectMaker:(id)sender;
-(IBAction)selectModel:(id)sender;
-(IBAction)selectVersion:(id)sender;

-(IBAction)selectFuel:(id)sender;
-(IBAction)selectFuelLevel:(id)sender;

-(IBAction)selectArrivalDate:(id)sender;
-(IBAction)selectMatriculationDate:(id)sender;
-(IBAction)selectRevisionDate:(id)sender;

-(IBAction)addMaintenance:(id)sender;

@end
