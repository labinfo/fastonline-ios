//
//  EmptyToNilConverter.h
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NilToDefaultConverter : NSObject

-(id)initWithId:(id)wrappedObject andDefaultValue:(id)defaultValue;

- (id)objectForKey:(id)aKey;

@end
