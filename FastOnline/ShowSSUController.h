//
//  ShowSSUController.h
//  FastOnline
//
//  Created by Nico Sordoni on 09/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "PageContainerController.h"
#import "SSUDetail.h"
#import "SSUSynchronizer.h"

@interface ShowSSUController : PageContainerController<PageContainerDelegate, SynchronizerDelegate, AlertConfirmDelegate>

@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_1;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_2;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_3;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_4;
@property (strong, nonatomic) IBOutlet PageMenuIcon *voice_5;

@property (strong, nonatomic) SSUDetail *detail;

-(IBAction)iconClicked:(id)sender;

@end
