//
//  CameraOverlay.h
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CameraOverlay : UIView

@property (strong, nonatomic) IBOutlet UIImageView *logo;
@end
