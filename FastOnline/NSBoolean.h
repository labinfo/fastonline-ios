//
//  NSBoolean.h
//  FastOnline
//
//  Created by Nico Sordoni on 08/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBoolean : NSObject

@property (nonatomic) int boolValue;
@end
