//
//  FOButton.m
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "FOButton.h"
#import "ColorUtil.h"
#import "ImpostazioniManager.h"

@implementation FOButton


- (void)drawRect:(CGRect)rect {
    
    //Init button style
    
    self.backgroundColor = [[ImpostazioniManager get] getAppColor];
    self.layer.borderWidth= 2;
    self.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.layer.cornerRadius = 5;
    self.clipsToBounds = YES;
}


@end
