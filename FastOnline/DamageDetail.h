//
//  DamageDetail.h
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DamageType.h"

@interface DamageDetail : NSObject<NSCoding>

@property (nonatomic, readonly)  NSString *detailId;
@property (nonatomic, strong, readonly)  NSString *name;
@property (nonatomic, strong, readonly)  NSMutableArray *types;
@property (nonatomic, strong, readonly)  NSMutableArray *typeres;

-(id) initWithData:(id)data;
-(void) addType: (DamageType*) type;

@end
