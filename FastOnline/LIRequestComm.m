//
//  LIRequestRSM.m
//  RSM-TV
//
//  Created by Laboratorio Informatico on 05/09/14.
//  Copyright (c) 2014 LabInfo. All rights reserved.
//

#import "LIRequestComm.h"
#import "UrlGenerator.h"
#import "AppDataManager.h"

@implementation LIRequestComm

- (NSString*)parseUrl:(NSString*)url {
    return [UrlGenerator getUrlForJsonWithString:url];
    
}

- (NSInteger)validateResponse:(id)response{
    if([[NSString stringWithFormat:@"%@",[response objectForKey:@"success"]] isEqualToString:@"0"])
    {
        self->_errorMessage = [response objectForKey:@"message"];
        return 1;
    }
    return 0;
}

-(void)post:(NSString *)url params:(NSDictionary *)params HTTPheaders:(NSDictionary *)headers jsonBody:(BOOL)json{
    
    NSString* SID = [[AppDataManager get] getSID];
    
    if(SID!=nil){
        NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:headers];
        [dict setValue:SID forKey:@"sessionid"];
        headers = dict;
    }
    
    [super post:url params:params HTTPheaders:headers jsonBody:json];
    
}

-(void)put:(NSString *)url params:(NSDictionary *)params HTTPheaders:(NSDictionary *)headers jsonBody:(BOOL)json{
    NSString* SID = [[AppDataManager get] getSID];
    
    if(SID!=nil){
        NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:headers];
        [dict setValue:SID forKey:@"sessionid"];
        headers = dict;
    }
    
    [super put:url params:params HTTPheaders:headers jsonBody:json];
    
}

-(void)get:(NSString *)url params:(NSDictionary *)params HTTPheaders:(NSDictionary *)headers jsonBody:(BOOL)json{
    
    NSString* SID = [[AppDataManager get] getSID];
    
    if(SID!=nil){
        NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:headers];
        [dict setValue:SID forKey:@"sessionid"];
        headers = dict;
    }
    
    [super get:url params:params HTTPheaders:headers jsonBody:json];
    
}



/*
- (void)callbackFailure {
 [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_UPDATE_FAILURE object:nil];
}
*/
@end
