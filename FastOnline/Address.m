//
//  Address.m
//  MyFirstFlash
//
//  Created by Nico Sordoni on 14/01/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import "Address.h"
#import "NilToDefaultConverter.h"

@implementation Address

-(id)initWithStreet:(NSString*)street town:(NSString*) town state:(NSString*)state zip: (NSString*) zip{
    self=[super init];
    if(self){
        
        if(!street || street == (id)[NSNull null])
            self.street=@"";
        else
            self.street=street;
        
        if(!town || town == (id)[NSNull null])
            self.town=@"";
        else
            self.town=town;
        
        if(!state || state == (id)[NSNull null])
            self.state=@"";
        else
            self.state=state;
        
        if(!zip || zip == (id)[NSNull null])
            self.zip=@"";
        else
            self.zip=zip;
    }
    return self;
}

-(id) initWithData:(id)data{
    self=[super init];
    if(self){
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self->_street forKey:@"street"];
    [encoder encodeObject:self->_town forKey:@"town"];
    [encoder encodeObject:self->_state forKey:@"state"];
    [encoder encodeObject:self->_zip forKey:@"zip"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_street = [decoder decodeObjectForKey:@"street"];
        self->_town = [decoder decodeObjectForKey:@"town"];
        self->_state = [decoder decodeObjectForKey:@"state"];
        self->_zip = [decoder decodeObjectForKey:@"zip"];
    }
    return self;
}
@end
