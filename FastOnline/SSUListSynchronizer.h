//
//  SSUListSynchronizer.h
//  fastonline
//
//  Created by Nico Sordoni on 13/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSUSynchronizer.h"

@protocol SSUListSynchronizerDelegate;

@interface SSUListSynchronizer : NSObject<SynchronizerDelegate>

@property (nonatomic, strong)  id<SSUListSynchronizerDelegate> delegate;
@property (nonatomic, strong)  BaseController *controller;
@property (nonatomic)  BOOL allSuccess, allFails;

-(id) initWithSSUs: (NSArray*) SSUList;
-(void) synchronizeSSUs;
-(void) stopSync;
@end

@protocol SSUListSynchronizerDelegate <NSObject>

@required

-(void)synchronizationCompletedFrom: (SSUListSynchronizer*) synchronizer sendFailed:(int)error sendSucceeded:(int) received;
-(void)sendingSSUNumber:(int)number of:(int) count;

@end