//
//  CustomCameraController.m
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CustomCameraController.h"
#import "ShapePickerController.h"

#import "Device.h"

@interface CustomCameraController ()

@end
@implementation CustomCameraController{
    ShapePickerController* shapePicker;
    UIDeviceOrientation orientation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.exitText.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseOverlay:)];
    singleTap.numberOfTapsRequired = 1;
    self.shapesButtonImage.userInteractionEnabled = YES;
    [self.shapesButtonImage addGestureRecognizer:singleTap];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    
    
    self.cameraButton.layer.cornerRadius = self.cameraButton.frame.size.width / 2;
    self.cameraButton.clipsToBounds = YES;
    
    self.shapesButton.clipsToBounds = YES;
    
    if(IS_IPAD){
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        
        // catch rotation!! lo rimouovo qui e non nel disappera perchè ne ho bisogno nelal macchina fotografica
        
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if(IS_IPAD){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(IS_IPAD);
    [self deviceOrientationDidChange:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)closeCamera:(id)sender{
    [self.cameraDelegate closeCamera];
}

-(void)takePicture:(id)sender{
    [self.cameraDelegate shootPicture];
}

-(void)chooseOverlay:(id)sender{
    
    shapePicker=[self.storyboard instantiateViewControllerWithIdentifier:@"ShapePickerVC"];
    
    shapePicker.shapeDelegate = self;
    
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
            shapePicker.view.transform =CGAffineTransformMakeRotation(-M_PI_2);
            break;
            
        case UIDeviceOrientationLandscapeRight:
            shapePicker.view.transform =CGAffineTransformMakeRotation(M_PI_2);
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            shapePicker.view.transform =CGAffineTransformMakeRotation(M_PI);
            break;
            
        default:
            break;
    }
    
    [self presentTransparentModalViewController:shapePicker animated:true withAlpha:1.0];
}

#pragma mark SHAPES

-(void)front3QPicked{
    self.frontShape.hidden = false;
    self.rearShape.hidden = true;
    [shapePicker chiudi:nil];
}

-(void)rear3QPicked{
    
    self.frontShape.hidden = true;
    self.rearShape.hidden = false;
    
    [shapePicker chiudi:nil];
}

-(void)noShapePicked{
    
    self.frontShape.hidden = true;
    self.rearShape.hidden = true;
    
    [shapePicker chiudi:nil];
}

#pragma mark ROTATION

- (void)deviceOrientationDidChange:(NSNotification *)notification

{
    
    [self.view layoutIfNeeded];
    if (IS_IPAD) {
        
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        
        
        [UIView animateWithDuration:0
                         animations:^{
                             
        switch (deviceOrientation) {
                
            case UIDeviceOrientationPortrait:
                
                orientation = deviceOrientation;
                
                //portrait
                
                
                self.cameraButtonView.transform = CGAffineTransformMakeRotation(0);
                self.cameraButtonView.frame = CGRectMake(0, self.view.frame.size.height - 144, self.view.frame.size.width, 144);
                
                //self.cameraButton.frame = CGRectMake(self.view.frame.size.width-174, (self.view.frame.size.height / 2) - 30, 60, 60 );
                self.cameraButton.frame = CGRectMake( (self.view.frame.size.width / 2) - 30 ,self.view.frame.size.height-174, 60, 60 );
                
                self.cameraButtonImage.transform = CGAffineTransformMakeRotation(0);
                self.cameraButtonImage.frame = CGRectMake( (self.view.frame.size.width / 2) - 20, self.cameraButton.frame.origin.y + 10,
                                                          40, 40);
                
                
                
                self.logo.transform = CGAffineTransformMakeRotation(0);
                self.logo.frame = CGRectMake(self.view.frame.size.width - 38, 28, 30, 107);
                
                
                self.shapesButtonImage.frame = CGRectMake(self.view.frame.size.width - 48, self.view.frame.size.height - 192, 40, 40);
                
                self.shapesButton.frame = CGRectMake(self.view.frame.size.width-192, 0, 44, 44);
                
                self.frontShape.transform = CGAffineTransformMakeRotation(0);
                self.frontShape.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 144);
                
                self.rearShape.transform = CGAffineTransformMakeRotation(0);
                self.rearShape.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 144);
                
                self.prova.frame =CGRectMake(28, 8, 100, 100);
                if(shapePicker)
                    shapePicker.view.transform = CGAffineTransformMakeRotation(0);
                break;
                
            case UIDeviceOrientationPortraitUpsideDown:
                
                orientation = deviceOrientation;
                
                
                self.cameraButtonView.transform = CGAffineTransformMakeRotation(M_PI);
                self.cameraButtonView.frame = CGRectMake(0, 0, self.view.frame.size.width, 144);
                
                self.cameraButton.frame = CGRectMake( (self.view.frame.size.width / 2) - 30 ,114, 60, 60 );
                
                self.cameraButtonImage.transform = CGAffineTransformMakeRotation(M_PI);
                self.cameraButtonImage.frame = CGRectMake( (self.view.frame.size.width / 2) - 20, 124,
                                                          40, 40);
                
                
                
                self.logo.transform = CGAffineTransformMakeRotation(M_PI);
                self.logo.frame = CGRectMake(4, self.view.frame.size.height - 107 - 28, 30, 107);
                
                
                self.shapesButtonImage.frame = CGRectMake(8, 152, 40, 40);
                
                self.frontShape.transform = CGAffineTransformMakeRotation(M_PI);
                self.frontShape.frame = CGRectMake(0, 144, self.view.frame.size.width, self.view.frame.size.height - 144);
                
                self.rearShape.transform = CGAffineTransformMakeRotation(M_PI);
                self.rearShape.frame = CGRectMake(0, 144, self.view.frame.size.width, self.view.frame.size.height - 144);
                
                if(shapePicker)
                    shapePicker.view.transform = CGAffineTransformMakeRotation(M_PI);
                break;
                
            case UIDeviceOrientationLandscapeLeft:
                
                orientation = deviceOrientation;
                
                
                self.cameraButtonView.transform = CGAffineTransformMakeRotation(-M_PI_2);
                self.cameraButtonView.frame = CGRectMake(self.view.frame.size.width-144, 0, 144, self.view.frame.size.height);
                
                self.cameraButton.frame = CGRectMake(self.view.frame.size.width-174, (self.view.frame.size.height / 2) - 30, 60, 60 );
                
                self.cameraButtonImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
                self.cameraButtonImage.frame = CGRectMake(self.cameraButton.frame.origin.x + 10, (self.view.frame.size.height / 2) - 20,
                                                          40, 40);
                
                self.logo.transform = CGAffineTransformMakeRotation(-M_PI_2);
                self.logo.frame = CGRectMake(28, 8, 107, 30);
                
                self.prova.frame =CGRectMake(28, 8, 100, 100);
                
                self.shapesButtonImage.frame = CGRectMake(self.view.frame.size.width-188, 4, 40, 40);
                
                self.frontShape.transform = CGAffineTransformMakeRotation(-M_PI_2);
                self.frontShape.frame = CGRectMake(0, 0, self.view.frame.size.width-144, self.view.frame.size.height);
                
                self.rearShape.transform = CGAffineTransformMakeRotation(-M_PI_2);
                self.rearShape.frame = CGRectMake(0, 0, self.view.frame.size.width-144, self.view.frame.size.height);
                
                if(shapePicker)
                    shapePicker.view.transform = CGAffineTransformMakeRotation(-M_PI_2);
                
                break;
                
            case UIDeviceOrientationLandscapeRight:
                
                orientation = deviceOrientation;
                self.prova.frame =CGRectMake(28, 8, 100, 100);
                
                self.cameraButtonView.transform = CGAffineTransformMakeRotation(M_PI_2);
                self.cameraButtonView.frame = CGRectMake(0, 0, 144, self.view.frame.size.height);
                
                self.cameraButton.frame = CGRectMake(114, (self.view.frame.size.height / 2) - 30, 60, 60 );
                
                self.cameraButtonImage.transform = CGAffineTransformMakeRotation(M_PI_2);
                self.cameraButtonImage.frame = CGRectMake(self.cameraButton.frame.origin.x + 10, (self.view.frame.size.height / 2) - 20,
                                                          40, 40);
                
                self.logo.transform = CGAffineTransformMakeRotation(M_PI_2);
                self.logo.frame = CGRectMake(self.view.frame.size.width - 28 - 107, self.view.frame.size.height - 8 - 30, 107, 30);
                
                self.shapesButtonImage.frame = CGRectMake(152, self.view.frame.size.height - 48, 40, 40);
                
                self.shapesButton.frame = CGRectMake(0, 0, 44, 44);
                
                
                self.shapesButton2.frame = CGRectMake(0, 0, 44, 44);
                self.shapesButton2.backgroundColor = [UIColor redColor];
                
                self.frontShape.transform = CGAffineTransformMakeRotation(M_PI_2);
                self.frontShape.frame = CGRectMake(144, 0, self.view.frame.size.width-144, self.view.frame.size.height);
                
                self.rearShape.transform = CGAffineTransformMakeRotation(M_PI_2);
                self.rearShape.frame = CGRectMake(144, 0, self.view.frame.size.width-144, self.view.frame.size.height);
                
                if(shapePicker)
                    shapePicker.view.transform = CGAffineTransformMakeRotation(M_PI_2);
                
                
                break;
                
            case UIDeviceOrientationFaceDown:
                
            case UIDeviceOrientationFaceUp:
                
            case UIDeviceOrientationUnknown:
                
            default:{}
                
        }
                         }];
        
         [self.view layoutIfNeeded];
        
        
    }
    
}





@end
