//
//  CarDataItemCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 09/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarDataItemCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *itemField;
@property (strong, nonatomic) IBOutlet UILabel *itemLabel;


@end
