//
//  FileSystemInfo.h
//  theananke
//
//  Created by Mirko Ravaioli on 11/09/13.
//  Copyright (c) 2013 Mirko Ravaioli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileSystemInfo : NSObject


+ (NSString *) applicationDocumentsDirectory;
+ (NSString *) getWritablePathWithoutCopy:(NSString *)file;
+ (id)loadDataFromPath:(NSString*)path;
+ (void) writeData:(id)object toPath:(NSString*)path;
+ (void) deleteFile:(NSString*)file;

@end
