//
//  UserInfoCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "UserInfoCell.h"
#import "ColorUtil.h"

@implementation UserInfoCell

- (void)awakeFromNib {
    
    [self initTextField:self.nameField];
    [self initTextField:self.surnameField];
    [self initTextField:self.emailField];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setUser:(User*) user{
    self.nameField.text = user.name;
    self.surnameField.text = user.surname;
    self.emailField.text = user.email;
    
}

-(void) initTextField: (UITextField*) view{
    
    CALayer* bottomBorder = [CALayer layer];
    
    UIColor* bordersColor = [ColorUtil getRgbColorRed:150 green:150 blue:150 alpha:1];
    bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height-1.0f, view.frame.size.width, 1.0);
    bottomBorder.backgroundColor = bordersColor.CGColor;
    [view.layer addSublayer:bottomBorder];
    
    view.enabled = NO;
}
@end
