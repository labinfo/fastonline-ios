//
//  DamageFormTopCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamageFormTopCell.h"

@implementation DamageFormTopCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDamage:(Damage *)damage{
    self.detailText.text = damage.detail.name;
    self.damageTypeText.text = damage.type.name;
    self.gravityLevelText.text = [damage stringForSeriousnessLevel];
    self.notesText.text = damage.notes;
}
@end
