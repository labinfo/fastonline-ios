//
//  SSUDetail.h
//  FastOnline
//
//  Created by Nico Sordoni on 07/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CarData.h"

#define STATUS_DRAFT 1
#define STATUS_COMPLETED 2
#define STATUS_SENT 3

@interface SSUDetail : NSObject<NSCoding>

@property (nonatomic,strong)  NSNumber *companyID, *serverID;
@property (nonatomic,strong)  NSString *localID;
@property (nonatomic,strong)  NSString *title, *carMakerName, *carModelName, *carVersionName;
//@property (nonatomic)  BOOL isCompleted, isSynchronized, isSynchronizationIncomplete;
@property (nonatomic) int completionLevel;
@property (nonatomic) int status;

- (id) initWithID:(NSString*) localID;
- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

-(void) setCarData:(CarData*) data;

@end
