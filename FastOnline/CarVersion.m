//
//  CarVersion.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarVersion.h"
#import "NilToDefaultConverter.h"

@implementation CarVersion

-(id)initWithData:(id)data{
    self = [super init];
    
    if(self){
        
        //id data = [[NilToDefaultConverter alloc] data andDefaultValue:@""];
        
        self->_versionId = [data objectForKey:@"id"];
        self->_name = [data objectForKey:@"name"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self->_versionId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
}

-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_versionId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
    }
    return self;
}

@end
