//
//  UIKeyboardListener.h
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIKeyboardListener : NSObject

@property (nonatomic, readonly) BOOL isKeyboardVisible;
@end
