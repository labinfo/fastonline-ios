//
//  DamagesMainController.h
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"
#import "DamagesMainCell.h"
#import "DamageConfirmController.h"
#import "DamageFormController.h"
#import "DamagesListController.h"


@interface DamagesMainController : BasePageController<UITableViewDataSource, UITableViewDelegate, DamagePickerDelegate, DamageInsertionConfirmDelegate, DamageNotifiedDelegate, DamagesListDelegate>

@property (nonatomic) BOOL isViewMode;

-(void) launchDamageCreationControllerForMacro: (MacroDamage*)macro withExistingDamage: (Damage*) damage;
@end
