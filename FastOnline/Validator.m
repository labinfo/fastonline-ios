//
//  Validator.m
//  MyFirstFlash
//
//  Created by Nico Sordoni on 14/01/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import "Validator.h"

@implementation Validator

+(BOOL) stringNotEmpty: (NSString*) string{
    if(string && string.length>0)
        return true;
    return false;
}

+(BOOL) stringNotBlank: (NSString*) string{
    return [Validator stringNotEmpty:[string stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}

+(BOOL)isValidEmail: (NSString*) string{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:string];
}

@end
