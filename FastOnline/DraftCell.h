//
//  DraftCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 07/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSUDetail.h"
#import "NSBoolean.h"

@protocol DetailCellDelegate;

@interface DraftCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *checkBoxImage;
@property (strong, nonatomic) IBOutlet UIImageView *circleImage;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *makerLabel;
@property (strong, nonatomic) IBOutlet UILabel *modelLabel;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;

@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *menuMargin;
@property (nonatomic) int menuMarginValue;
@property (strong, nonatomic) SSUDetail *SSUdetail;

@property (strong, nonatomic) id<DetailCellDelegate> delegate;
@property (strong, nonatomic) NSBoolean *isSelected;


-(IBAction)toggleSelection:(id)sender;
-(IBAction)openMenu:(id)sender;
-(IBAction)closeMenu:(id)sender;
-(IBAction)doModify:(id)sender;
-(IBAction)doDelete:(id)sender;
-(IBAction)doSync:(id)sender;

@end

@protocol DetailCellDelegate <NSObject>

-(void)detail: (SSUDetail*) detail selected:(BOOL)selected;
-(void) modifySSU:(SSUDetail*) detail;
-(void) deleteSSU:(SSUDetail*) detail;
-(void) syncSSU:(SSUDetail*) detail;
-(void) CellOpened:(DraftCell*) cell;

@end