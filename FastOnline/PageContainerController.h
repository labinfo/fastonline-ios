//
//  PageContainerController.h
//  mff
//
//  Created by Nico Sordoni on 09/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageMenuIcon.h"
#import "BasePageController.h"
#import "BaseController.h"

@class BasePageController;

@protocol PageContainerDelegate <NSObject>

@required
-(BasePageController*) createControllerForPosition: (int) position;
@required
-(NSArray*) getMenuVoices;

@end

@interface PageContainerController : BaseController<UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIActionSheetDelegate>


@property (readonly, nonatomic) int currentPosition;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) IBOutlet UIView *menu_cointainer;
@property (weak) id <PageContainerDelegate> delegate;


@property (strong, nonatomic)  NSArray* menuVoices;

-(void) onBack: (id) sender;
-(void) goToPage: (int) position;
-(void) moveForward;
-(void) moveBack;
-(void) end;
-(void) showVoiceAsSelected: (int) position;
-(BasePageController *) getControllerForPosition: (int) position;

-(void)clearControllers:(NSArray*) controllers;
-(void)leavingPageController;

//Invoked when app exit from a controller. Useful for get and use the data 
-(void) leavingController:(BasePageController*) controller;

-(void) setMaxAllowedPosition: (int) maxPosition;

-(IBAction)voiceClicked:(id)sender;
@end
