//
//  JSONDataManager.m
//  iAce
//
//  Created by Matteo Corradin on 19/09/13.
//  Copyright (c) 2013 Matteo Corradin. All rights reserved.
//

#import "StorageManager.h"
#import "FileSystemInfo.h"

@implementation StorageManager

+ (NSString*)fileForTipoDato:(NSString*)tipoDato{
    return [NSString stringWithFormat:@"%@.data",tipoDato];
}

+ (id) loadFromFile:(NSString*)filePath {
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self absoluteFilePath:filePath]]
        ) {
        NSData *data = [[NSMutableData alloc]
                        initWithContentsOfFile:[FileSystemInfo getWritablePathWithoutCopy:filePath]];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc]
                                         initForReadingWithData:data];
        id obj = [unarchiver decodeObjectForKey:@"data"];
        [unarchiver finishDecoding];
        return obj;
    }
    return nil;
}

+ (void) storeToFile:(NSString*)filePath object:(id)obj {
    NSMutableData *data = [[NSMutableData alloc] init];
    
    
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc]
                                 initForWritingWithMutableData:data];
    [archiver encodeObject:obj forKey:@"data"];
    [archiver finishEncoding];
    BOOL res = [data writeToFile:[self absoluteFilePath:filePath] atomically:YES];
    if (res == true) {
        
    }
}

+(void)deleteFile:(NSString *)path{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* filePath = [self absoluteFilePath:path];
    NSError *error;
    BOOL fileExists = [fileManager fileExistsAtPath:filePath];
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        if (!success)
            NSLog(@"While deleting file @ %@ occurred folliwing error: %@",filePath, [error localizedDescription]);
    }
}

+(NSString *)absoluteFilePath:(NSString *)filePath{
    return [FileSystemInfo getWritablePathWithoutCopy:filePath];
}

@end
