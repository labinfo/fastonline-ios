//
//  DamageFormTopCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Damage.h"

@interface DamageFormTopCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *title;

@property (nonatomic, strong) IBOutlet UITextField *detailText;
@property (nonatomic, strong) IBOutlet UITextField *gravityLevelText;
@property (nonatomic, strong) IBOutlet UITextField *damageTypeText;
@property (nonatomic, strong) IBOutlet UITextField *notesText;

-(void)setDamage: (Damage*) damage;

@end
