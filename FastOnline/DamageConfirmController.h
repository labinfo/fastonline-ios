//
//  DamageConfirmController.h
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"
#import "MacroDamage.h"

@protocol DamageInsertionConfirmDelegate <NSObject>

-(void)damageInsertionConfirmed: (MacroDamage*) macro;

@end

@interface DamageConfirmController : TrasparentebaseController

@property (nonatomic, strong) id<DamageInsertionConfirmDelegate> confirmDelegate;

@property (nonatomic, strong) IBOutlet UILabel *macroNameLabel;
@property (nonatomic, strong) MacroDamage *macro;
@property (nonatomic, strong) UIViewController *pageController;

-(IBAction)insertDamage:(id)sender;

@end
