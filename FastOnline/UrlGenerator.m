//
//  UrlGenerator.m
//  BuonApp iPhone
//
//  Created by Matteo Corradin on 13/09/12.
//  Copyright (c) 2012 stefano giorgi. All rights reserved.
//

#import "UrlGenerator.h"
#import "Config.h"
#import "ImpostazioniManager.h"

@implementation UrlGenerator

+ (NSString*)getUrlForJsonWithString:(NSString*)indirizzo{
    NSString *strUrl = [NSString stringWithFormat:@"%@/%@",SERVER_BASE_URL,indirizzo];
    return strUrl;
}

+ (NSString*)getUrlForPush:(NSString*)indirizzo{
    return @"";
}
+ (NSString*)getUrlForImmagine:(NSString*)nomeImmagine ofSSU:(SSUDetail *)SSU{
    //NSString *strUrl = [NSString stringWithFormat:@"%@/%@/%d/%@",SERVER_BASE_URL,SYNC_URL,[SSU.serverID intValue], nomeImmagine];

    return @"http://www.fastonline.eu/fastonline/app_uploader/upload.php";
}

+ (NSString*)getUrlForAllegato:(NSString*)nomeAllegato{
    NSString *strUrl = [NSString stringWithFormat:@"%@/%@",SERVER_BASE_URL, nomeAllegato];
    
    return strUrl;
}

@end
