//
//  DamagesMainCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#define FUNCTION_LOAD @"onLoad"
#define FUNCTION_DAMAGE_SELECTED @"blockSelected"

#import "DamagesMainCell.h"

@implementation DamagesMainCell

- (void)awakeFromNib {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];

    [self.webView loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSURL *URL = [request URL];
    if ([[URL scheme] isEqualToString:@"bridge"]) {
        NSString* host = [URL host];
        if([host isEqualToString:FUNCTION_LOAD]){
            [self.delegate loadCompleted];
        }
        else if([host isEqualToString:FUNCTION_DAMAGE_SELECTED]){
            NSString* damageId = [[URL path] stringByReplacingOccurrencesOfString:@"/" withString:@""];
            [self.delegate damagePickedWithId:damageId];
        }
        return NO;
    }
    return YES;
}

@end
