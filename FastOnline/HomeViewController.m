//
//  ViewController.m
//  FastOnline
//
//  Created by Nico Sordoni on 13/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//


#import "HomeViewController.h"
#import "LocalizationSystem.h"
#import "ColorUtil.h"
#import "ImpostazioniManager.h"
#import "AppDataManager.h"
#import "CompaniesDownloader.h"


@interface HomeViewController ()

@end

@implementation HomeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

    NSString *copyText = AMLocalizedString(@"Copyright", @"");
    NSString *label = [NSString stringWithFormat:copyText, version];
    self.copyrightLabel.text = label;
    
    self.navigationController.navigationBarHidden = true;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    //Bugsense log test
    //id i = [NSArray new];
    //[i setObject:@"" forKey:@""];
    
    self.loginButton.layer.borderWidth= 2;
    self.loginButton.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.loginButton.layer.cornerRadius = 5;
    self.loginButton.backgroundColor = [[ImpostazioniManager get] getAppColor];
    
    //Check if user logged yet
    //Check if user logged yet
    AppDataManager *appDataManager = [AppDataManager get];
    NSString* SID = [appDataManager getSID];
    if(SID != nil)
    {
        CompaniesDownloader *downloader =[CompaniesDownloader new];
        downloader.controller = self;
        [downloader downloadCompaniesAndGoAhead];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
        [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    



}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)loginClicked:(id)sender{
    
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"] animated:YES];
}

@end
