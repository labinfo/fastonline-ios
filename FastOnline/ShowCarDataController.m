//
//  ShowCarDataController.m
//  FastOnline
//
//  Created by Nico Sordoni on 09/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "ShowCarDataController.h"
#import "LocalizationSystem.h"
#import "CarData.h"
#import "CarDataItemCell.h"

#define LB(X) AMLocalizedString(X,@"")

@interface ShowCarDataController ()

@end

@implementation ShowCarDataController{
    NSArray *labels;
    NSMutableArray *values;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    labels = @[LB(@"lbMaker"), LB(@"lbModel"), LB(@"lbVersion"), LB(@"lbPlate"), LB(@"lbKw"),
               LB(@"lbFuel"), LB(@"lbDoors"), LB(@"lbSeats"), LB(@"lbColor"), LB(@"lbKm"), LB(@"lbImmDate"),
               LB(@"lbCc"), LB(@"lbRevDate"), LB(@"lbPark"), LB(@"lbArrDate"), LB(@"lbFuelLevel")];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)loadData:(id)data{
    CarData *carData = data;
    
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd/MM/yyyy"];
    
    NSDateFormatter *dateTimeformatter = [[NSDateFormatter alloc] init];
    [dateTimeformatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    values = [NSMutableArray new];
    
    [values addObject: carData.maker ? carData.maker.name : @""];
    [values addObject: carData.model ? carData.model.name : @""];
    [values addObject: carData.version ? carData.version.name : @""];
    
    [self addStringValue:carData.plateNumber];
    [self addIntValue:carData.kw];
    [self addStringValue:carData.fuelType];
    [self addIntValue:carData.doorsNumber];
    [self addIntValue:carData.seatsNumber];
    [self addStringValue:carData.color];
    [self addIntValue:carData.kmCrossed];
    
    [values addObject: carData.matriculationDate ? [dateformatter stringFromDate:carData.matriculationDate] : @"" ];
    
    [self addIntValue:carData.cc];
    
    [values addObject: carData.revisionDate ? [dateformatter stringFromDate:carData.revisionDate] : @"" ];
    
    [self addStringValue:carData.park];
    
    [values addObject: carData.arrivalDate ? [dateTimeformatter stringFromDate:carData.arrivalDate] : @"" ];
    
    [self addStringValue:carData.fuelLevel];
    
}

//Util method
-(void) addStringValue:(NSString*) value{
    [values addObject: value ? value : @""];
}

//Util method
-(void) addIntValue:(int) value{
    [values addObject: value>0 ? [NSString stringWithFormat:@"%d", value] : @""];
}

#pragma mark - TABLE

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    tableView.allowsSelection = NO;
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 17;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return [tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
    }
    else
    {
        CarDataItemCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
        
        cell.itemField.placeholder = labels[indexPath.row-1];
        cell.itemLabel.text = values[indexPath.row-1];
        
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0)
        return 90;
    return 50;
}


@end
