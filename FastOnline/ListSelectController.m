//
//  CompanySelectController.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "ListSelectController.h"
#import "ListSelectCell.h"
#import "Device.h"

@interface ListSelectController ()

@end

@implementation ListSelectController{
    //Used to indicate if is necessary to resize the view on keyboard opening
    BOOL highView;
    
    NSMutableDictionary* dataKeys;
    NSMutableArray* allData;
    NSMutableArray* filteredData;
    
    
    NSString* searchFilterText;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(IS_IPAD)
        self.viewWidth.constant = 450;
    
    // Do any additional setup after loading the view.searchBar.layer.borderWidth = 1;
    self.searchBar.layer.borderWidth = 1;
    self.searchBar.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    self.adaptToKeyboard = YES;
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //Close modal only if keyboard is hidden
    //if(!listener.isKeyboardVisible)
    
    UITouch* touch = touches.allObjects[0];
    CGPoint touchLocation = [touch locationInView:self.view];
    if(!CGRectContainsPoint(self.containerView.frame, touchLocation))
        [self chiudi:nil];
}

-(void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    
    if(searchText.length>0){
        filteredData = [[NSMutableArray alloc] init];
        
        for(NSString* value in allData){
            if([value rangeOfString:searchText options:NSCaseInsensitiveSearch].location!=NSNotFound)
                //if([value containsString:searchText])
                [filteredData addObject:value];
        }
    }
    else{
        filteredData = allData;
    }
    
    [self.table reloadData];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    
    //Header height + cells height
    int totalHeight = 116 + [self.listDataSource getListLengthForRequest:self.requestId]*44;
    int maxHeight = SCREEN_HEIGHT - 150;
    
    if(totalHeight>maxHeight){
        self.viewHeight.constant = maxHeight;
        highView = YES;
    }
    else{
        self.viewHeight.constant = totalHeight;
        highView = NO;
    }
    
}


-(void)loadData{
    
    dataKeys = [[NSMutableDictionary alloc] init];
    allData = [[NSMutableArray alloc] init];
    
    for(int i=0; i<[self.listDataSource getListLengthForRequest:self.requestId];i++){
        id value=[self.listDataSource getTextAtPosition:i forRequest:self.requestId];
        [dataKeys setObject:[NSNumber numberWithInt:i] forKey: value];
        [allData addObject:value];
    }
    
    filteredData = allData;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return [self.listDataSource getListLengthForRequest:self.requestId];
    return filteredData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"ListCell";
    ListSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.itemTextLabel.text = [filteredData objectAtIndex:indexPath.row];
    //[data objectForKey:[NSNumber numberWithInt:indexPath.row]];
    
    //[self.listDataSource getTextAtPosition:(int)indexPath.row forRequest:self.requestId];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int originalPosition = [allData indexOfObject:filteredData[indexPath.row]];
    [self.listDelegate didSelectItemAtPosition:originalPosition forRequest:self.requestId];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self chiudi:nil];
}

-(void)keyboardWillShow:(NSNotification *)notification{
    if(highView)
        self.viewHeight.constant -=80;
    [super keyboardWillShow:notification];
}

-(void)keyboardWillHide:(NSNotification *)notification{
    
    if(highView)
        self.viewHeight.constant +=80;
    [super keyboardWillHide:notification];
}
@end
