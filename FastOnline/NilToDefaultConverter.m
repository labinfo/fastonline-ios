//
//  EmptyToNilConverter.m
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "NilToDefaultConverter.h"

@implementation NilToDefaultConverter{
    id wrapped;
    id defValue;
}

-(id)initWithId:(id)wrappedObject andDefaultValue:(id)defaultValue{
    self = [super init];
    if(self){
        wrapped = wrappedObject;
        defValue = defaultValue;
        
    }
    return self;
}

- (id)objectForKey:(id)aKey{
    id obj = [wrapped objectForKey:aKey];
    
    if(!obj || obj == (id)[NSNull null]){
        obj=[defValue copy];
    }
    return obj;
}

@end
