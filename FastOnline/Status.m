//
//  Status.m
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "Status.h"

@implementation Status

-(id)initWithName:(NSString *)name{
    self =[super init];
    
    if(self){
        self->_name = name;
        self->_statusCode = NO_STATUS;
    }
    return self;
}

-(id) init{
    self =[super init];
    
    if(self){
        self->_name = @"";
        self->_statusCode = NO_STATUS;
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self =[super init];
    
    if(self){
        
        self->_name = [aDecoder decodeObjectForKey:@"name"];
        self->_statusCode = [aDecoder decodeIntForKey:@"status"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self->_name forKey:@"name"];
    [aCoder encodeInt:self->_statusCode forKey:@"status"];
}
-(NSString *)stringForStatus{
    if(self.statusCode == STATUS_BAD)
    return @"Scarso";
    else if(self.statusCode == STATUS_MEDIUM)
    return @"Medio";
    else if(self.statusCode == STATUS_GOOD)
    return @"Buono";
    else
    return @"";
}
@end
