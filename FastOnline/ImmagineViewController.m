//
//  ImmagineViewController.m
//  iAce
//
//  Created by Matteo Corradin on 02/10/13.
//  Copyright (c) 2013 Matteo Corradin. All rights reserved.
//

#import "ImmagineViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UrlGenerator.h"
#import "Config.h"
#define TAG_IMMAGINE 5

@interface ImmagineViewController ()

@end

@implementation ImmagineViewController{
    BOOL hiddenHud;
    NSMutableArray * imageArr;
    UITapGestureRecognizer * hideMenuTap;
    UIScrollView* scroll;
        UIImageView * imageView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    hideMenuTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleHud)];
    self.pagingView.delegate = self;
    self.pagingView.recyclingEnabled = false;
    self.pagingView.pagesToPreload = 0;
    [self.pagingView addGestureRecognizer:hideMenuTap];
    
    [self setFastOnlineNavBar];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    self.pagingView.backgroundColor = [UIColor whiteColor];
    [self.pagingView reloadData];
}


- (NSInteger)numberOfPagesInPagingView:(ATPagingView *)pagingView {
    return 1;
}

- (UIView *)viewForPageInPagingView:(ATPagingView *)pagingView atIndex:(NSInteger)index {

    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.pagingView.bounds.size.width, self.pagingView.bounds.size.height)];
        scroll.minimumZoomScale = 1.0;
        scroll.maximumZoomScale = 3.0;
        scroll.zoomScale=1.0;
        scroll.backgroundColor = [UIColor whiteColor];
        scroll.contentSize = CGSizeMake(self.pagingView.bounds.size.width, self.pagingView.bounds.size.height);
        scroll.clipsToBounds = YES;
        scroll.autoresizesSubviews = YES;
        scroll.delegate = self;
        scroll.tag = 1;
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.pagingView.bounds.size.width, self.pagingView.bounds.size.height)];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.tag = TAG_IMMAGINE;
        imageView.backgroundColor = [UIColor whiteColor];
        imageView.image = self.image;
        [scroll addSubview:imageView];

    scroll.tag = 1;
    imageView.tag = TAG_IMMAGINE;
    scroll.contentSize = imageView.frame.size;
    
    return scroll;
}

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}

- (void)currentPageDidChangeInPagingView:(ATPagingView *)pagingView {
    //  return [[[ImageDispatcher shared] getImageAt:self.pagingView.currentPageIndex]setZoomScale:1.0];
}

- (void)showHud{
    if (hiddenHud) {
        [self.navigationController setNavigationBarHidden:false animated:true];
        hiddenHud=false;
    }
}

- (void)hideHud{
    if (!hiddenHud) {
        [self.navigationController setNavigationBarHidden:true animated:true];
        hiddenHud=true;
    }
}

- (void)toggleHud{
    if (hiddenHud) {
        [self showHud];
    }else{
        [self hideHud];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale{
    [self.pagingView willAnimateRotation];
    [self.pagingView didRotate];
}


@end
