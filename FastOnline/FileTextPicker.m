//
//  FileTextPicker.m
//  mff
//
//  Created by Nico Sordoni on 12/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileTextPicker.h"

@implementation FileTextPicker {
    NSArray* data;
    NSString* file;
    NSString* dataList;
    BaseController *controller;
    NSString* shownTitle;
    UITextField* field;
}

-(id) initWithFileName: (NSString*) fileName dataListName: (NSString*) listName textField: (UITextField*) textField  containerController: (BaseController*) baseController title:(NSString*) title{
    self = [super init];
    
    if(self){
        
        self->_selectedId = -1;
        
        file = fileName;
        dataList = listName;
        field = textField;
        controller = baseController;
        shownTitle = title;
    }
    return self;
}

-(void)pickValue{
    
    [self loadData];
}

-(void)loadData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:file ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    id allValues = [[NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil] objectForKey:dataList];
    
    NSMutableArray* list=[[NSMutableArray alloc] init];
    for (id value in allValues) {
        [list addObject : value];
    }
    
    data = [[NSArray alloc] initWithArray:list];
    
    ListSelectController* listController=[controller.storyboard instantiateViewControllerWithIdentifier:@"ListSelect"];
    
    listController.listDelegate=self;
    listController.listDataSource = self;

    [controller presentTransparentModalViewController:listController animated:true withAlpha:1.0];
    listController.title = shownTitle;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return data.count;
}

-(int) getListLengthForRequest:(int)requestId{
    return (int)data.count;
}

-(NSString*) getTextAtPosition: (int) position forRequest:(int)requestId{

    return [data objectAtIndex:position];
}

-(void)didSelectItemAtPosition:(int)position forRequest:(int) requestId{
    
    field.text = [data objectAtIndex:position];
    self->_selectedId = position;
    self->_selectedText = [data objectAtIndex:position];
    
}
@end
