//
//  DamageDetail.m
//  FastOnline
//
//  Created by Nico Sordoni on 03/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamageDetail.h"

@implementation DamageDetail

-(id)initWithData:(id)data{
    self = [super init];
    
    if(self){
        self->_detailId = [[data objectForKey:@"id"] stringValue];
        self->_name = [data objectForKey:@"label"];
        self->_types = [NSMutableArray new];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)decoder{
    self =  [super init];
    if (self) {
        self->_detailId = [decoder decodeObjectForKey:@"id"];
        self->_name = [decoder decodeObjectForKey:@"name"];
        self->_types = [decoder decodeObjectForKey:@"types"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self->_detailId forKey:@"id"];
    [encoder encodeObject:self->_name forKey:@"name"];
    [encoder encodeObject:self->_types forKey:@"types"];
    
}
-(void)addType:(DamageType *)type{
    [self->_types addObject:type];
}

@end
