//
//  ImmagineViewController.h
//  iAce
//
//  Created by Matteo Corradin on 02/10/13.
//  Copyright (c) 2013 Matteo Corradin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATPagingView.h"
#import "BaseController.h"

@interface ImmagineViewController : BaseController <ATPagingViewDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet ATPagingView *pagingView;
@property (strong,nonatomic) UIImage* image;

@end
