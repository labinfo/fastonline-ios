//
//  ImpostazioniManager.m
//  theananke
//
//  Created by Mirko Ravaioli on 11/09/13.
//  Copyright (c) 2013 Mirko Ravaioli. All rights reserved.
//

#import "ImpostazioniManager.h"
#import "FileSystemInfo.h"
#import "Config.h"
#import "ColorUtil.h"
#import "SSKeychain.h"

static ImpostazioniManager* shared = nil;

@implementation ImpostazioniManager {
    NSMutableDictionary * impostazioni;
    NSString *path;
}

+ (ImpostazioniManager*)get {
    @synchronized(self) {
        if (shared == nil) {
            shared = [[ImpostazioniManager alloc] init];
        }
    }
    return shared;
}

- (id)init{
    self = [super init];
    if (self) {
        
        path = [FileSystemInfo getWritablePathWithoutCopy:@"impostazioni.plist"];
        impostazioni = [NSMutableDictionary dictionaryWithContentsOfFile:path];
        if (impostazioni.count == 0){
            impostazioni = [[NSMutableDictionary alloc]init];
        }
        [impostazioni setObject:[ColorUtil getRgbColorRed:48 green:152 blue:253 alpha:1] forKey:@"mainColor"];
    }
    return self;
}

- (id)getObjectForKey:(NSString*)key{
    return [impostazioni objectForKey:key];
}

- (void)setObject:(id)valore forKey:(NSString*)key{
    if (valore == nil) {
        [impostazioni removeObjectForKey:key];
    }
    else {
        [impostazioni setObject:valore forKey:key];
    }
    [impostazioni writeToFile:path atomically:YES];
}

-(void)setLingua:(NSString*)lingua {
    [self setObject:lingua forKey:@"lingua"];
}

-(NSString*)getLingua {
    NSString* lingua = [self getObjectForKey:@"lingua"];
    if (lingua == nil || lingua.length == 0) {
        return LINGUA_DEFAULT;
    }
    return lingua;
}
-(NSString*) getDeviceLangCode{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language isEqualToString:@"it"] || [language isEqualToString:@"it-IT"])
        return @"it";
    
    
    return @"en";
    
}

-(NSString*) getListDataFileName{
    return [NSString stringWithFormat:@"lists_%@",[self getDeviceLangCode]];
}


-(void)setPosizioneManualeLatitudine:(NSNumber*)latitudine longitudine:(NSNumber*)longitudine{
    [self setObject:latitudine forKey:@"PMANLatitudine"];
    [self setObject:longitudine forKey:@"PMANLongitudine"];
}
-(NSDictionary*)getPosizioneManuale{
    return [NSDictionary dictionaryWithObjectsAndKeys:[self getObjectForKey:@"PMANLatitudine"],@"latitudine",[self getObjectForKey:@"PMANLongitudine"],@"longitudine", nil];
}

-(void)setPosizioneManualeAttiva:(BOOL)stato{
    if (stato) {
        [self setObject:[NSNumber numberWithBool:TRUE] forKey:@"PMANAttiva"];
    }else{
        [self setObject:[NSNumber numberWithBool:FALSE] forKey:@"PMANAttiva"];
    }
}

-(BOOL)getPosizioneManualeAttiva{
    if ([self getObjectForKey:@"PMANAttiva"] == nil) {
        return false;
    }
    return [[self getObjectForKey:@"PMANAttiva"] boolValue];
}
-(UIColor*)getAppColor{
    return [impostazioni objectForKey:@"mainColor"];
}

@end
