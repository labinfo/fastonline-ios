//
//  CarDataCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 20/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CarDataCell.h"
#import "OnlyNumberInputValidator.h"

@implementation CarDataCell{
    OnlyNumberInputValidator *numberValidator;
}


- (void)awakeFromNib {
    
    numberValidator =[[OnlyNumberInputValidator alloc] init];
    
    //Numeric inputs
    [self restrictToNumbers:self.kmCrossedField];
    [self restrictToNumbers:self.kwField];
    [self restrictToNumbers:self.doorsField];
    [self restrictToNumbers:self.seatsField];
    [self restrictToNumbers:self.ccField];
    
    //Rating bar
    self.rateView.notSelectedImage = [UIImage imageNamed:@"star_empty"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"star_filled"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"star_filled"];
    self.rateView.rating = 0;
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    //self.rateView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setCarData:(CarData *)carData{
    
    if(carData.maker)
        self.makerField.text = carData.maker.name;
    if(carData.model)
        self.modelField.text = carData.model.name;
    if(carData.version)
        self.versionField.text = carData.version.name;
    
    
    if(carData.plateNumber)
        self.plateField.text = carData.plateNumber;
    if(carData.fuelLevel)
        self.fuelField.text=carData.fuelLevel;
    if(carData.color)
        self.colorField.text = carData.color;
    if(carData.park)
        self.parkField.text = carData.park;
    if(carData.fuelLevel)
        self.fuelLevelField.text = carData.fuelLevel;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    if(carData.matriculationDate)
        self.matriculationDateField.text = [formatter stringFromDate:carData.matriculationDate];
    if(carData.revisionDate)
        self.revisionDateField.text = [formatter stringFromDate:carData.revisionDate];
    
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    if(carData.arrivalDate)
        self.arrivalDateField.text = [formatter stringFromDate:carData.arrivalDate];
    
    if(carData.kmCrossed > 0)
        self.kmCrossedField.text = [NSString stringWithFormat:@"%d", carData.kmCrossed];
    if(carData.kw > 0)
        self.kwField.text = [NSString stringWithFormat:@"%d", carData.kmCrossed];
    if(carData.doorsNumber > 0)
        self.doorsField.text = [NSString stringWithFormat:@"%d", carData.doorsNumber];
    if(carData.seatsNumber > 0)
        self.seatsField.text = [NSString stringWithFormat:@"%d", carData.seatsNumber];
    if(carData.cc > 0)
        self.ccField.text = [NSString stringWithFormat:@"%d", carData.cc];
    
    if(carData.userRate > 0)
        self.rateView.rating = carData.userRate;
}

-(void) restrictToNumbers:(UITextField*) field{
    field.keyboardType = UIKeyboardTypeDecimalPad;
    field.delegate = numberValidator;
    
}
@end
