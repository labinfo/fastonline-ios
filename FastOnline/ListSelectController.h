//
//  CompanySelectController.h
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "TrasparentebaseController.h"

@protocol ListDataSource <NSObject>

@required
-(int) getListLengthForRequest:(int) requestId;
-(NSString*) getTextAtPosition: (int) position forRequest: (int) requestId;

@end

@protocol ListDelegate <NSObject>

@required
-(void) didSelectItemAtPosition: (int) position forRequest: (int)requestId;

@end

@interface ListSelectController : TrasparentebaseController<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *viewHeight;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *viewWidth;
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) NSString* title;
@property (nonatomic) int requestId;

@property (nonatomic, strong) id<ListDataSource> listDataSource;
@property (nonatomic, strong) id<ListDelegate> listDelegate;
@end
