//
//  FOCarImageContainer.m
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "FOCarImageContainer.h"
#import "ColorUtil.h"
#import "ImmagineViewController.h"

#define ACTIONS_SHOW_TIME 5.0

@implementation FOCarImageContainer{
    int index;
    UIImageView *mainImageView;
    
    UIView* actionsView;
    //Actions are shown only for a fixed time amount
    NSTimer *actionsTimer;
}


- (void)drawRect:(CGRect)rect {
    
    int greyScale = 100;
    self.backgroundColor = [ColorUtil getRgbColorRed:greyScale green:greyScale blue:greyScale alpha:1];
    
    self.layer.borderWidth= 2;
    self.layer.borderColor= [ColorUtil getRgbColorRed:4 green:95 blue:206 alpha:1].CGColor;
    self.clipsToBounds = YES;
    
    //Create and add the main imageView, the one which will show the taken picture
    mainImageView = [self createImageView];
    [self addSubview: mainImageView];
    if(self->_image!=nil)
        mainImageView.image = self->_image;
    
    [self addSubview:[self createMainButton]];
    
    index = self.tag;
    
}

#pragma mark VIEWS CREATION

-(UIImageView*) createImageView{
    
    UIImageView* imageView =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width, self.layer.frame.size.height)];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    return imageView;
}

-(UIButton*) createMainButton{
    UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width, self.layer.frame.size.height)];
    
    [btn addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

-(UIView*) createActionButtonsView{
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width, self.layer.frame.size.height)];
    
    if(!self.isViewMode){
        //Add retake button and image to container
        UIButton* retakeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width/2, self.layer.frame.size.height)];
        retakeBtn.backgroundColor = [ColorUtil getRgbColorRed:0 green:0 blue:0 alpha:0.5];
        [retakeBtn addTarget:self action:@selector(takePicture:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView* retakeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width/2, self.layer.frame.size.height)];
        retakeImage.contentMode = UIViewContentModeScaleAspectFit;
        
        retakeImage.image = [UIImage imageNamed:@"ic_photo_riscatta.png"];
        
        [view addSubview:retakeBtn];
        [view addSubview:retakeImage];
    }
    
    //Add FullScreen button and image to container
    UIButton* fullScreenBtn;
    UIImageView* fullScreenImage;
    if(!self.isViewMode){
       fullScreenBtn  = [[UIButton alloc] initWithFrame:CGRectMake(self.layer.frame.size.width/2, 0, self.layer.frame.size.width/2, self.layer.frame.size.height)];
        
        fullScreenImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.layer.frame.size.width/2, 0, self.layer.frame.size.width/2, self.layer.frame.size.height)];
    }else{
        fullScreenBtn  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width, self.layer.frame.size.height)];
        fullScreenImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.layer.frame.size.width, self.layer.frame.size.height)];

    }
    fullScreenBtn.backgroundColor = [ColorUtil getRgbColorRed:0 green:0 blue:0 alpha:0.5];
    [fullScreenBtn addTarget:self action:@selector(showFullScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    fullScreenImage.contentMode = UIViewContentModeScaleAspectFit;
    
    fullScreenImage.image = [UIImage imageNamed:@"ic_photo_more"];
    
    [view addSubview: fullScreenBtn];
    [view addSubview: fullScreenImage];
    
    return view;
}


#pragma mark ACTIONS

-(void) btnPressed: (id) sender{
    //If image is not yet present, take picture
    if(self.image == nil){
        if(!self.isViewMode)
            //[self setImage:[UIImage imageNamed: @"login_background.png"]];
             [self takePicture:sender];
    }
    //If image exists, show actions
    else{
        
        //Init actions view if not set yet
        if(actionsView == nil){
            actionsView = [self createActionButtonsView];
            [self addSubview:actionsView];
        }
        
        //Show acions if hidden
        actionsView.hidden = NO;
        
        if(actionsTimer == nil)
            actionsTimer = [NSTimer scheduledTimerWithTimeInterval:ACTIONS_SHOW_TIME target:self selector:@selector(hideActions:) userInfo:nil repeats:YES];
        
    }
}

-(void) hideActions: (id) sender{
    [actionsTimer invalidate];
    actionsTimer = nil;
    
    actionsView.hidden = YES;
}

-(void) takePicture: (id) sender{
    [self.pictureController takePictureAtIndex:(int)self.tag];
}

-(void) showFullScreen: (id) sender{
    
    ImmagineViewController *controller = [self.pictureController.storyboard instantiateViewControllerWithIdentifier:@"ImageVC"];
    controller.image = self.image;
    
    [self.pictureController.navigationController pushViewController:controller animated:YES];
}

-(void)setImage:(UIImage *)image{
    if(image == (id)[NSNull null])
        self->_image = nil;
    else{
        self->_image = image;
        mainImageView.image = image;
    }
}

@end
