//
//  Maintenance.m
//  FastOnline
//
//  Created by Nico Sordoni on 06/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "Maintenance.h"

@implementation Maintenance

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self =  [super init];
    if (self) {
        self->_date = [aDecoder decodeObjectForKey:@"date"];
        self->_desc = [aDecoder decodeObjectForKey:@"desc"];
        self->_kmCrossed = [aDecoder decodeIntForKey:@"km"];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self->_date forKey:@"date"];
    [aCoder encodeObject:self->_desc forKey:@"desc"];
    [aCoder encodeInt:self->_kmCrossed forKey:@"km"];
}
@end
