//
//  SSUSynchronizer.m
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "SSUSynchronizer.h"
#import "LIRequestComm.h"
#import "LocalizationSystem.h"
#import "SSUDataConverter.h"
#import "Config.h"
#import "AFNetworking.h"
#import "AppDataManager.h"
#import "UrlGenerator.h"
#import "NSString+MD5.h"

#define IMAGE_REQUEST_TIMEOUT 60 * 3;
#define APP_SECRET @"23jnasrcSOAP40o349!%ADSFG£&I&$IDFAGas313vabj_as3463hgju690679jnbvxc256736adfkasvan5720946mogigoiqe3239857vxcmnsgjertzxcse572teoihsgfnmkdlfg958728WERgew4234£&7FDG_Sde556"

@implementation SSUSynchronizer{
    SSUDetail *currentSSU;
    
    //Number of images which are still not sent
    int unsentImages;
    BOOL imagesErrors;
}

-(id)initWithSSU:(SSUDetail *)SSU{
    self = [super init];
    
    if(self){
        currentSSU = SSU;
    }
    return self;
}

-(void)synchronizeSSU{
    LIRequestComm *request = [[LIRequestComm alloc] init];
    
    SSUDataConverter *dataConverter = [SSUDataConverter new];
    
    //Needed to use request inside block
    
    
    [request setSuccess:^(id data){
        currentSSU.serverID =[data objectForKey:@"id"];
        //Save new SSU State
        [[AppDataManager get] setSSUCompleted:currentSSU];
        
        
        
        //Generate, just one time, the string containing the list of images name
        NSMutableString* imagesList = [NSMutableString new];
        for(NSString *fileName in [dataConverter.imagesQueue  allKeys]){
            
            //All image names, except the first one, must follow a comma
            if(imagesList.length > 0){
                [imagesList appendString:[NSString stringWithFormat:@",%@",fileName]];
            }else{
                [imagesList appendString:fileName];
            }
            
        }
        
        if(dataConverter.imagesQueue.count == 0){
            currentSSU.status= STATUS_SENT;
            [[AppDataManager get] setSSUCompleted:currentSSU];
            
            [self.delegate syncronizationCompletedForSSU:currentSSU];
        }
        else{
            
            imagesErrors = NO;
            unsentImages = dataConverter.imagesQueue.count;
            
            for(NSString *fileName in [dataConverter.imagesQueue  allKeys]){
                [self sendImage:[dataConverter.imagesQueue objectForKey:fileName] withFileName:fileName andSSU:currentSSU imagesString:imagesList];
            }
        }
        
    }];
    
    [request setFailure:^(id responseObject){
        currentSSU.status = STATUS_COMPLETED;
        //Force the persistent data storage with the new updated information
        [[AppDataManager get] setSSUCompleted:currentSSU];
        
        NSString* errorMessage = [responseObject objectForKey:@"message"];
        if(errorMessage.length > 0){
            [self.delegate syncronizationFailedForSSU:currentSSU withMessage:errorMessage];
        }else{
            [self.delegate syncronizationFailedForSSU:currentSSU];
        }
    }];
    
    dataConverter.SSU = currentSSU;
    
    NSMutableDictionary* headers = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"ios:%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]],@"AppVersion",
                                    nil];
    
    if(currentSSU.serverID != nil && [currentSSU.serverID intValue] != 0)
        [request put: [NSString stringWithFormat:@"%@/%d", SYNC_URL, [currentSSU.serverID intValue]] params:[dataConverter getSSUDataDictionary] HTTPheaders:headers jsonBody:YES];
    else{
        [headers setObject:[NSString stringWithFormat:@"%d",[currentSSU.localID  intValue]] forKey:@"ClientCode"];
        [request post: SYNC_URL params:[dataConverter getSSUDataDictionary] HTTPheaders:headers jsonBody:YES];
    }
}

-(void) notifyImageSent{
    @synchronized(self) {
        unsentImages--;
        
        //If all imeges has been sent, the synchronization is completed
        if(unsentImages == 0){
            
            //The synchronization is considered succeded only if all images has been sent
            if(!imagesErrors)
                currentSSU.status = STATUS_SENT;
            
            [[AppDataManager get] setSSUCompleted:currentSSU];
            [self.delegate syncronizationCompletedForSSU:currentSSU];
        }
    }
}

-(void)stopSync{
    
}

-(void) posdtImage:(UIImage*) image fileName:(NSString*) fileName{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[UrlGenerator getUrlForImmagine:fileName ofSSU:currentSSU]]];
    NSInputStream *stream = [[NSInputStream alloc] initWithData:UIImageJPEGRepresentation(image, 0.5)];// initWithFileAtPath:filePath];
    [request setHTTPBodyStream:stream];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:[[AppDataManager get] getSID] forHTTPHeaderField:@"sessionid"];
    [request setValue:[[AppDataManager get] getSID] forHTTPHeaderField:@"id_ssu"];
    [request setValue:[[AppDataManager get] getSID] forHTTPHeaderField:@"nome_foto"];
    [request setValue:[[AppDataManager get] getSID] forHTTPHeaderField:@"elenco_foto"];
    [request setValue:[[AppDataManager get] getSID] forHTTPHeaderField:@"token"];
    
    //?[request setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                           }];
}

-(void)sendImage:(UIImage*)image withFileName: (NSString*) fileName andSSU:(SSUDetail*) ssu imagesString: (NSString*) images{
    //NSBundle* myBundle = [NSBundle mainBundle];
    //NSString* myImage = [myBundle pathForResource:@"image" ofType:@"png"];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer * requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    requestSerializer.timeoutInterval = IMAGE_REQUEST_TIMEOUT;
    
    //[requestSerializer setValue:[[AppDataManager get] getSID] forHTTPHeaderField:@"sessionid"];
    
    AFHTTPResponseSerializer * responseSerializer = [AFJSONResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain", @"application/json", nil];
    
    manager.requestSerializer = requestSerializer;
    manager.responseSerializer = responseSerializer;
    
    
    NSString* token = [[NSString stringWithFormat:@"%@%@%@",ssu.serverID, fileName, APP_SECRET] MD5];
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ssu.serverID, @"id_ssu",
                            fileName, @"nome_foto",
                            images, @"elenco_foto",
                            token, @"token",
                            nil];
    
    
    [manager POST:[UrlGenerator getUrlForImmagine:fileName ofSSU:currentSSU] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpeg"];
        
    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Risultato: %@", responseObject);
              
              
              
              //Notify that the processing of an image has been completed
              [self notifyImageSent];
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              currentSSU.status = STATUS_COMPLETED;
              imagesErrors = YES;
              
              //Force data storage
              [[AppDataManager get] setSSUCompleted:currentSSU];
              
              //Notify that the processing of an image has been completed
              [self notifyImageSent];
              
              NSLog(@"Error: %@", error);
          }];
    
}
@end
