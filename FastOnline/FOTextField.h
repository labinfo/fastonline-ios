//
//  FOTextField.h
//  FastOnline
//
//  Created by Nico Sordoni on 20/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface FOTextField : UITextField

@end
