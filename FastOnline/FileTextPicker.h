//
//  FileTextPicker.h
//  mff
//
//  Created by Nico Sordoni on 12/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "ListSelectController.h"



@interface FileTextPicker : NSObject<ListDataSource, ListDelegate>

@property (nonatomic,readonly) int selectedId;
@property (nonatomic,strong) NSString *selectedText;


-(id) initWithFileName: (NSString*) fileName dataListName: (NSString*) listName textField: (UITextField*) textField containerController: (BaseController*) controller
                 title:(NSString*) title;
-(void) pickValue;
@end


