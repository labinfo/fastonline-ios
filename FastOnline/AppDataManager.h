//
//  AppDataManager.h
//  FastOnline
//
//  Created by Nico Sordoni on 17/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "DamagesDataManager.h"
#import "SSU.h"

@interface AppDataManager : NSObject

+ (AppDataManager*)get;


-(void) storeData:(id<NSCoding>) data forKey: (NSString*) key;
-(id<NSCoding>) loadDataForKey: (NSString*) key;

//USER DATA
-(void) setUserData: (id) data;
-(User*) getUser;
-(void) addCompanies: (id)data;

-(void) logout;

-(NSString*) getSID;
-(void) setSID: (NSString*) SID;

//VEHICLE DATA

-(void)loadVehicleData;
-(NSArray*) getCarMakers;

//SSU


//-(NSNumber*) newSSUID;
-(NSString*) newSSUIDforCompany: (Company*) company;
-(NSString*) keyForSection: (int) sectionIndex ofSSUWithId:(NSString*) localID;

//SSU Drafts

-(void) addSSUDetail:(SSUDetail*) detail;
-(void)removeSSU:(SSUDetail*) detail;
-(void)removeAllSSUInList:(NSArray*) SSUDetailList;

-(void) modifyCompletedSSU:(SSUDetail*) SSU;


-(NSArray*) getSSUDetailsList;

//SSU Archive


-(void) setSSUCompleted:(SSUDetail *) detail;
//-(void) addCompletedSSU:(SSUDetail*) detail;
-(void)removeCompletedSSU:(SSUDetail*) detail;
-(void)removeAllCompletedSSUIn:(NSArray*) SSUDetailList;

-(NSArray*) getCompletedSSUList;

//Damages
-(DamagesDataManager*) getDamagesDataManager;


-(int) getUnsentItemsCount;


@end
