//
//  MaintenanceCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 24/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"
#import "DateFieldPicker.h"
#import "Maintenance.h"

@interface MaintenanceCell : UITableViewCell

@property (strong, nonatomic)  BaseController *mainController;

@property (strong, nonatomic) IBOutlet UITextField *kmField;
@property (strong, nonatomic) IBOutlet UITextField *dateField;
@property (strong, nonatomic) IBOutlet UITextField *descField;
@property (strong, nonatomic) DateFieldPicker *datePicker;

-(void)setMaintentance:(Maintenance*) maintenance;

-(IBAction)selectDate:(id)sender;
@end
