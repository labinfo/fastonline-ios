//
//  DamageConfirmController.m
//  FastOnline
//
//  Created by Nico Sordoni on 04/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "DamageConfirmController.h"
#import "DamageFormController.h"

@interface DamageConfirmController ()

@end

@implementation DamageConfirmController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)insertDamage:(id)sender{
    [self chiudi:sender];
    
    [self.confirmDelegate damageInsertionConfirmed:self.macro];
}


@end
