//
//  LeftMenuController.m
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "LeftMenuController.h"
#import "HomeNavigationController.h"
#import "LocalizationSystem.h"
#import "LeftMenuCell.h"
#import "AppDataManager.h"
#import "DraftsController.h"

static LeftMenuController *instance = nil;

@interface LeftMenuController ()

@end

@implementation LeftMenuController{
    NSArray* menuTitles;
    NSArray* menuIcons;
    int currentScreenIndex;
    BOOL firstLoad;
    
    SSUListSynchronizer *synchronizer;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentScreenIndex = -1;
    firstLoad=true;
    //Create 2 arrays, one for sections titles and another for icon names.
    //White strings are used as separators for various sections
    menuTitles=@[
                 AMLocalizedString(@"LeftTitleNewSSU", @""),AMLocalizedString(@"LeftTitleArchive", @""),
                 AMLocalizedString(@"LeftTitleDrafts", @""), AMLocalizedString(@"LeftTitleSettings", @""),
                 //AMLocalizedString(@"LeftTitleSync", @""),
                 AMLocalizedString(@"LeftTitleLogout", @"")];
    
    menuIcons=@[@"new_ssu", @"archive-ssu", @"new_ssu", @"settings", /*@"sync",*/ @"logout"];
    
    instance = self;
}


+(LeftMenuController*)shared{
    return instance;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated]; //Left menu init the deckController placeing the start center viewController
    if(firstLoad){
        [self setCenterController:0];
        firstLoad=false;
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.table selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setDisabled:(BOOL)disabled{
    self->_disabled = disabled;
    if(disabled)
        [self.viewDeckController closeLeftViewAnimated:YES];
}

#pragma mark TABLE VIEW

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"LeftMenuCell";
    LeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.backgroundColor = [UIColor clearColor];
    
    NSString* text = menuTitles[indexPath.row];
    [cell.text setText: [text uppercaseString]];
    [cell.image setImage:[UIImage imageNamed:menuIcons[indexPath.row]]];
    
    if(indexPath.row == 0)
        [cell setSelected:YES animated:YES];
    
    
    return cell;
    
}

//If is disabled, avoid menu opening
-(BOOL)viewDeckControllerWillOpenLeftView:(IIViewDeckController *)viewDeckController animated:(BOOL)animated{
    
    return !self.disabled;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if(indexPath.row == 4){
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [self.table selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentScreenIndex inSection:0] animated:YES scrollPosition:0];
        
        
    }
    
    
    [self setCenterController:(int)indexPath.row];
}

#pragma mark CENTER CONTROLLERS

//Set the right controller on the viewDeck, basing on the selected item index
-(void) setCenterController:(int) selectedIndex{
    
    //The new controller that will replace the current one
    HomeNavigationController * center;
    
    
    //Show selected screen, but only if different from the current one
    if(currentScreenIndex!=selectedIndex)
    {
        
        switch (selectedIndex) {
            case 0:
                center=[self.storyboard instantiateViewControllerWithIdentifier:@"NewSSUHomeVC"];
                break;
            case 1:
            {
                center=[self.storyboard instantiateViewControllerWithIdentifier:@"DraftsVC"];
                DraftsController * draftsVC = (DraftsController*)center.topViewController;
                draftsVC.isArchive = YES;
                break;
            }
            case 2:{
                center=[self.storyboard instantiateViewControllerWithIdentifier:@"DraftsVC"];
                DraftsController * draftsVC = (DraftsController*)center.topViewController;
                draftsVC.isArchive = NO;
                break;
            }
            case 3:{
                center=[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
                break;
            }
            case 1000:
            {
                [self syncAll];
                center=nil;
                
                break;
            }
            case 4:
            {
                int unsentCount = [[AppDataManager get] getUnsentItemsCount];
                if(unsentCount > 0){
                    [self.viewDeckController closeLeftViewAnimated:YES];
                    BaseController * baseController = [self getCenterBaseController];
                    AlertController* alertController=[baseController.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
                    NSString *errorMessage = AMLocalizedString(@"BlockLogoutMessage", @"");
    
                    [baseController presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
                    alertController.messageLabel.text = errorMessage;
                    return;
                }

                
                //[self.viewDeckController dismissViewControllerAnimated:YES completion:nil];
                [[AppDataManager get] logout];
                [self.viewDeckController.navigationController popToRootViewControllerAnimated:TRUE];
                center = nil;
                //center=[self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
                break;
            }
                
            default:{
                center = [self.storyboard instantiateViewControllerWithIdentifier:@"NewSSUHomeVC"];
                break;
            }
        }
        if(center!=nil){
            
            //HomeNavigation need the reference to ViewDeck, to open menu on item click
            center.viewDeckController=self.viewDeckController;
            
            [self.viewDeckController setCenterController:center];
            [self.viewDeckController closeLeftViewAnimated:YES];
            
            currentScreenIndex=selectedIndex;
        }
        
    }
    
    
    
    [self.viewDeckController closeLeftViewAnimated:true];
    
    
}

-(BaseController*) getCenterBaseController{
    
    UIViewController* center = self.viewDeckController.centerController;

    
    if([center class] == [BaseController class])
        return (BaseController*) center;
    else if([center class] == [HomeNavigationController class]){
        HomeNavigationController * homeNav = (HomeNavigationController*)center;
       return (BaseController*)  homeNav.topViewController;
    }
    return nil;
    
}

#pragma mark SYNC

-(void) syncAll{
    NSArray *allSSUs = [[AppDataManager get] getCompletedSSUList];
    
    
    BaseController * baseController = [self getCenterBaseController];
    [baseController showProgressWithMessage:AMLocalizedString(@"Synchronizing", @"") detailText:AMLocalizedString(@"TapToDelete", @"") callbackOnTap:^{
        [self stopSync];
    }];
    
    
    synchronizer = [[SSUListSynchronizer alloc] initWithSSUs:allSSUs];
    synchronizer.delegate = self;
    [synchronizer synchronizeSSUs];
    
    
    
}

-(void) stopSync{
    if(synchronizer){
        [synchronizer stopSync];
    }
    synchronizer = nil;
    
    BaseController * baseController = [self getCenterBaseController];
    [baseController hideProgress];
    
}

-(void)synchronizationCompletedFrom:(id)listSynchronizer{
    
    BaseController * baseController = [self getCenterBaseController];
    
    [baseController hideProgress];
    
    [baseController showErrorAlertWithMessage:AMLocalizedString(@"SyncOk", @"")];
    synchronizer = nil;
}

@end

