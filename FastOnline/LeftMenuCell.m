//
//  LeftMenuCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "LeftMenuCell.h"
#import "UIImage+Overlay.h"
#import "ColorUtil.h"
#import "Config.h"
#import "Device.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    
    self.rightMargin.constant = SCREEN_WIDTH - LEFT_MENU_WIDTH + 12;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setSelected:true animated:true];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    UIColor* itemsColor, *bgColor;
    if(!selected){
       itemsColor = [ColorUtil getRgbColorRed:180 green:180 blue:180 alpha:1];
        bgColor = [UIColor clearColor];
    }
    else{
        itemsColor = [UIColor whiteColor];
        bgColor = [ColorUtil getRgbColorRed:4 green:97 blue:210 alpha:1];
    }
    
    self.image.image =  [self.image.image imageWithColor:itemsColor];
    self.arrow.image =  [self.arrow.image imageWithColor:itemsColor];
    self.text.textColor = itemsColor;
    self.backgroundColor = bgColor;
    
}

@end
