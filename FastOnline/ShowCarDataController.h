//
//  ShowCarDataController.h
//  FastOnline
//
//  Created by Nico Sordoni on 09/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "BasePageController.h"

@interface ShowCarDataController : BasePageController<UITableViewDataSource, UITableViewDelegate>


@end
