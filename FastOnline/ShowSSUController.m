//
//  ShowSSUController.m
//  FastOnline
//
//  Created by Nico Sordoni on 09/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "ShowSSUController.h"
#import "AppDataManager.h"
#import "CarPicturesController.h"
#import "OptionalsController.h"
#import "StatusController.h"
#import "DamagesMainController.h"
#import "UIImage+Overlay.h"
#import "NewSSUPageController.h"
#import "LocalizationSystem.h"
#import "SynchronizationHelper.h"

#define REQUEST_DELETE_SSU 10

@interface ShowSSUController ()

@end

@implementation ShowSSUController{
    NSMutableArray* controllers;
    int selectedIconPosition;
    NSArray* iconImagesNames;
    SSUSynchronizer *synchronizer;
}

-(void)dealloc{
}

- (void)viewDidLoad {
    
    self.delegate = self;
    
    
    [self setFastOnlineNavBar];
    
    UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    editButton.frame = CGRectMake(0, 0, 25, 25); // custom frame
    [editButton setImage:[[UIImage imageNamed:@"penna.png"] imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *trashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    trashButton.frame = CGRectMake(0, 0, 25, 25); // custom frame
    [trashButton setImage:[[UIImage imageNamed:@"cestino.png"] imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [trashButton addTarget:self action:@selector(discard) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.detail.status == STATUS_COMPLETED){
        UIButton *syncButton = [UIButton buttonWithType:UIButtonTypeCustom];
        syncButton.frame = CGRectMake(0, 0, 25, 25); // custom frame
        [syncButton setImage:[[UIImage imageNamed:@"cloud.png"] imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [syncButton addTarget:self action:@selector(sync) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItems =@[[[UIBarButtonItem alloc] initWithCustomView:syncButton],[[UIBarButtonItem alloc] initWithCustomView:trashButton],[[UIBarButtonItem alloc] initWithCustomView:editButton]];
    }
    else
    {
        self.navigationItem.rightBarButtonItems =@[[[UIBarButtonItem alloc] initWithCustomView:trashButton],[[UIBarButtonItem alloc] initWithCustomView:editButton]];
    }
    
    
    selectedIconPosition = -1;
    
    controllers = [NSMutableArray new];
    for(int i = 0; i < 5; i++){
        [controllers addObject:[self createControllerForPosition:i]];
    }
    
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = @"";
    
    iconImagesNames = @[@"info-auto",@"gallery-foto",@"equip",@"stato",@"danni"];
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)leavingPageController{
    
    [self clearControllers:controllers];
    self.delegate = nil;
    self.detail = nil;
}

#pragma mark SYNC
-(void)sync{
    
    if(self.detail.status == STATUS_SENT){
        [self showErrorAlertWithMessage:AMLocalizedString(@"SsuAlreadySynchronized", @"")];
    }
    else{
        [self showProgressWithMessage:[NSString stringWithFormat:AMLocalizedString(@"SendingSSU", @""),1,1] detailText:@"" callbackOnTap:nil];
        
        //Sync only first element
        [self doSync:self.detail];
    }
}

-(void) doSync:(SSUDetail *) detail{
    
    synchronizer = [[SSUSynchronizer alloc] initWithSSU:detail];
    synchronizer.delegate = self;
    [synchronizer synchronizeSSU];
}

-(void)syncronizationCompletedForSSU:(SSUDetail *)SSUdetail{
    synchronizer.delegate = nil;
    synchronizer = nil;
    [self hideProgress];
    [SynchronizationHelper showSynchronizationAlertWithCompleted:1 andFailed:0 inController:self];
}

-(void)syncronizationFailedForSSU:(SSUDetail *)SSUdetail{
    synchronizer.delegate = nil;
    synchronizer = nil;
    [self hideProgress];
    [SynchronizationHelper showSynchronizationAlertWithCompleted:0 andFailed:1 inController:self];
}

-(void)synchronizationAbortedForSSU:(SSUDetail *)SSUdetail{
    
}
-(void)stopSync{
    [self hideProgress];
}

#pragma mark EDIT

-(void) edit{
    
    NewSSUPageController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SSUCreationVC"];
    
    
    
    controller.detail = self.detail;
    
    if(self.detail.status != STATUS_DRAFT) {
        [[AppDataManager get] modifyCompletedSSU:self.detail];
    }
    
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark DISCARD

-(void) discard{
    
    AlertController* alertController=[self.storyboard instantiateViewControllerWithIdentifier:@"Alert"];
    
    [alertController askConfirmForRequest:REQUEST_DELETE_SSU delegate:self];
    
    [self presentTransparentModalViewController:alertController animated:true withAlpha:1.0];
    alertController.messageLabel.text = AMLocalizedString(@"SSUDeleteConfirmMessage", @"");
}

-(void)requestConfirmed:(int)requestId{
    
    if(requestId == REQUEST_DELETE_SSU){
        
        if(self.detail.status != STATUS_DRAFT)
            [[AppDataManager get] removeCompletedSSU:self.detail];
        else
            [[AppDataManager get] removeSSU:self.detail];
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - PAGE MANAGEMENT


-(void)leavingController:(BasePageController *)controller{
    controller.pageController = nil;
}

-(NSArray *)getMenuVoices{
    return @[self.voice_1,self.voice_2,self.voice_3,self.voice_4,self.voice_5];
}

-(BasePageController *)getControllerForPosition:(int)position{
    return controllers[position];
}

-(BasePageController *)createControllerForPosition:(int)position{
    BasePageController* controller;
    
    NSString* key = [[AppDataManager get] keyForSection:position ofSSUWithId:self.detail.localID];
    
    id data = [[AppDataManager get] loadDataForKey:key];
    
    switch (position) {
        case 0:
            controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowCarDataVC"];
            
            
            break;
        case 1:{
            CarPicturesController* picVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CarPicturesVC"];
            picVC.isViewMode = YES;
            controller = picVC;
            
            break;
        }
        case 2:{
            OptionalsController *optVc =[self.storyboard instantiateViewControllerWithIdentifier:@"OptionalsVC"];
            optVc.isViewMode = true;
            controller = optVc;
            break;
        }
        case 3:{
            StatusController *statusVC =[self.storyboard instantiateViewControllerWithIdentifier:@"StatusVC"];
            statusVC.isViewMode = true;
            controller = statusVC;
            break;
        }
        case 4:{
            DamagesMainController *damagesVC =[self.storyboard instantiateViewControllerWithIdentifier:@"DamagesMainVC"];
            damagesVC.isViewMode = true;
            controller = damagesVC;
            break;
        }
        default:
            break;
    }
    if(data && controller){
        [controller loadData:data];
    }
    controller.position = position;
    
    controller.pageController = self;
    
    
    return controller;
}


-(void)iconClicked:(id)sender{
    UIButton* btn = sender;
    int position = btn.tag;
    
    [self goToPage:position];
}
-(void)showVoiceAsSelected:(int)position{
    
    if(selectedIconPosition > -1){
        //Deselect previous selected
        PageMenuIcon* selectedIcon = [self getMenuVoices][selectedIconPosition];
        selectedIcon.backgroundColor = [UIColor whiteColor];
        selectedIcon.image.image = [UIImage imageNamed: iconImagesNames[selectedIconPosition]];
    }
    
    PageMenuIcon *icon = [self getMenuVoices][position];
    icon.backgroundColor = [UIColor clearColor];
    icon.image.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-active.png", iconImagesNames[position]]];
    selectedIconPosition = position;
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    BasePageController *baseVC = (BasePageController*) viewController;
    
    if(baseVC.position < 4){
        return controllers[baseVC.position + 1];
    }
    return nil;
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    BasePageController *baseVC = (BasePageController*) viewController;
    
    if(baseVC.position > 0){
        return controllers[baseVC.position - 1];
    }
    return nil;
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    if(completed){
        BasePageController* controller = [pageViewController.viewControllers objectAtIndex:0];
        int position = controller.position;
        [self showVoiceAsSelected:position];
    }
}
@end
