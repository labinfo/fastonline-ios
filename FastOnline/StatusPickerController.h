//
//  StatusPickerController.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "TrasparentebaseController.h"
#import "Status.h"

@protocol StatusPickerDelegate <NSObject>

@required
-(void) statusPicked:(int)statusCode forId: (int) requestId;

@end

@interface StatusPickerController : TrasparentebaseController

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) id<StatusPickerDelegate> pickerDelegate;
@property (nonatomic) int requestId;



-(IBAction)statusGood:(id)sender;
-(IBAction)statusMedium:(id)sender;
-(IBAction)statusBad:(id)sender;

@end
