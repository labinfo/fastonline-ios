//
//  UrlGenerator.h
//  BuonApp iPhone
//
//  Created by Matteo Corradin on 13/09/12.
//  Copyright (c) 2012 stefano giorgi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSU.h"


@interface UrlGenerator : NSObject

+ (NSString*)getUrlForJsonWithString:(NSString*)indirizzo;
+ (NSString*)getUrlForPush:(NSString*)indirizzo;

+ (NSString*)getUrlForAllegato:(NSString*)nomeAllegato;
+ (NSString*)getUrlForImmagine:(NSString*)nomeImmagine ofSSU:(SSUDetail*) SSU;

@end
