//
//  AddMaintenanceCell.h
//  FastOnline
//
//  Created by Nico Sordoni on 23/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMaintenanceCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btn;

@end
