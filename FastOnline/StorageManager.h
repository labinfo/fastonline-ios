//
//  JSONDataManager.h
//  iAce
//
//  Created by Matteo Corradin on 19/09/13.
//  Copyright (c) 2013 Matteo Corradin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StorageManager : NSObject

+ (id)loadFromFile:(NSString*)filePath;
+ (void)storeToFile:(NSString*)filePath object:(id)obj;
+ (void)deleteFile:(NSString*)filePath;
+(NSString*) absoluteFilePath:(NSString*) filePath;
@end
