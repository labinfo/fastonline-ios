//
//  LoginSettingsController.h
//  FastOnline
//
//  Created by Nico Sordoni on 17/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginSettingsController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *oldPwdInput;
@property (strong, nonatomic) IBOutlet UITextField *updatedPwdInput;
@property (strong, nonatomic) IBOutlet UITextField *confirmPwdInput;

@property (strong, nonatomic) IBOutlet UIButton *updateButton;
@end
