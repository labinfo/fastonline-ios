//
//  PageContainerController.m
//  mff
//
//  Created by Nico Sordoni on 09/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import "PageContainerController.h"
#import "ColorUtil.h"
#import "UIImage+Overlay.h"
#import "LocalizationSystem.h"
#import "BasePageController.h"

@interface PageContainerController ()

@end

@implementation PageContainerController{
    NSMutableArray* controllers;
    PageMenuIcon* selectedIcon;
    int maxAllowedPosition;
    UIColor* completedColor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    controllers = [[NSMutableArray alloc] init];
    self->_currentPosition = 0;
    maxAllowedPosition=0;
    
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageVC"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = false;
    
    UIViewController* controller = [self getControllerForPosition:0];
    [self.pageViewController setViewControllers:@[controller] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    // Change the size of page view controller
    self.pageViewController.view.frame = self.menu_cointainer.bounds;
    
    [self addChildViewController:self.pageViewController];
    [self.menu_cointainer addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    [self initMenu];
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        [self leavingPageController];
    }
    [super viewWillDisappear:animated];
}

-(void)leavingPageController{
    
    [self clearControllers:controllers];
    
    self.pageViewController.delegate = nil;
    self.pageViewController.dataSource = nil;
    
    [self.pageViewController removeFromParentViewController];
    self.pageViewController = nil;
    
    self.menuVoices = nil;
    self.menu_cointainer = nil;
    
    [controllers removeAllObjects];
    controllers = nil;
    
    self.delegate = nil;
    [self.parentViewController removeFromParentViewController];
    [self removeFromParentViewController];
}


-(void)clearControllers:(NSArray *)baseControllers{
    
    for(int i = 0 ; i < baseControllers.count; i++){
        //for(BasePageController* basePageController in controllers){
        BasePageController* basePageController = baseControllers[i];
        [basePageController pageControllerDidExit];
        basePageController.pageController = nil;
        [basePageController removeFromParentViewController];
    }
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
}

-(void) onBack: (id) sender{
    
}

-(void)setMaxAllowedPosition:(int)maxPosition{
    
    maxAllowedPosition = maxPosition;
    
    for(int i = self->_currentPosition + 1 ; i <= maxPosition ; i++){
        BasePageController *newController = [self.delegate createControllerForPosition:i];
        
        [controllers addObject:newController];
        
        newController.pageController = self;
        newController.position = i;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            break;
    }
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    actionSheet.delegate = nil;
}
-(void) initMenu{
    
    completedColor = [ColorUtil getRgbColorRed:61 green:167 blue:255 alpha:1];
    self.menuVoices = [self getMenuVoices];
    
    [self showVoiceAsSelected:0];
    
}
-(NSArray*) getMenuVoices{
    if(!self.delegate)
    return @[];
    
    return [self.delegate getMenuVoices];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

//Method which returns the controller for the required position, using ADT to ensure performance
-(BasePageController *) getControllerForPosition: (int) position{
    
    if(position<0 || position> (self.menuVoices.count - 1))
        return nil;
    
    //If selected controller doesn't exists, create it (ASSUMING that controllers are scrolled sequentially)
    if(controllers.count==position){
        BasePageController *newController = [self.delegate createControllerForPosition:position];
        
        if(newController==nil)
            return nil;
        
        [controllers addObject:newController];
        
        newController.pageController = self;
        newController.position = position;
    }
    
    return controllers[position];
}


-(IBAction)voiceClicked:(id)sender{
    int position = (int)[sender tag];
    
    if(position<=maxAllowedPosition){
        [self goToPage:position];
    }
}


-(BOOL) canGoToPage: (int) position {
    
    //Out of indexes not allowed
    if(position<0 || position>=self.menuVoices.count)
        return NO;
    
    //Can always navigate backward (or remain in the current position)
    if(position <= self->_currentPosition)
        return YES;
    else
    {
        //Can navigate forward only if current form has been correctly validated
        BasePageController* controller = [self getControllerForPosition:self->_currentPosition];
        return [controller validationOk];
    }
    
    return NO;
}

-(void) goToPage: (int) position{
    
    //Go to page, after having checked
    if([self canGoToPage:position]){
        
        [self leavingController:[self getControllerForPosition:self.currentPosition]];
        
        BasePageController* controller = [self getControllerForPosition:position];
        
        UIPageViewControllerNavigationDirection direction=UIPageViewControllerNavigationDirectionForward;
        if(position < self->_currentPosition)
            direction = UIPageViewControllerNavigationDirectionReverse;
        
        
        [self.pageViewController setViewControllers:@[controller] direction: direction animated:NO completion:nil];
        
        self->_currentPosition = position;
        
        if(maxAllowedPosition < position)
            maxAllowedPosition = position;
        
        [self showVoiceAsSelected:position];
    }
}



-(void) moveForward{
    
    if(self.currentPosition == self.menuVoices.count - 1)
        [self end];
    else
        [self goToPage:self->_currentPosition+1];
}

-(void)end{
    
}

-(void) moveBack{
        [self goToPage:self->_currentPosition-1];
    
    
}
-(void)dealloc{
    
}
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    
    BasePageController *controller = (BasePageController *) [[self.pageViewController viewControllers] objectAtIndex:0];
    
    self->_currentPosition = controller.position;
  


}

-(void) showVoiceAsSelected: (int) position{
    
    PageMenuIcon* menuIcon = self.menuVoices[position];
    
    menuIcon.image.image = [menuIcon.image.image imageWithColor:completedColor];
    
    if(selectedIcon){
        [selectedIcon deselect];
    }
    
    [menuIcon setSelectedWithColor:completedColor];
    
    //menuIcon.border.backgroundColor = completedColor;
    selectedIcon=menuIcon;

    
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    
    
    
   /* BasePageController* baseController = (BasePageController*) viewController;
    
    
    //if([self canGoToPage:baseController.position+1]){
    if(maxAllowedPosition<baseController.position)
        return [self getControllerForPosition:baseController.position+1];
        //return nil;
    
    */
    return nil;
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    
    //User can go back, only if not at the first page
   /* BasePageController* baseController = (BasePageController*) viewController;
    
    //if([self canGoToPage:baseController.position-1]){
    if(baseController.position>0)
        return [self getControllerForPosition:baseController.position-1];
    */
    
    return nil;
}


@end
