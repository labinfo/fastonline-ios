//
//  CompanyCell.m
//  FastOnline
//
//  Created by Nico Sordoni on 18/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "CompanyCell.h"
#import "ColorUtil.h"


@implementation CompanyCell

- (void)awakeFromNib {
    
    [self initTextField:self.nameField];
    [self initTextField:self.addressField];
    [self initTextField:self.townField];
    [self initTextField:self.provinceField];
    [self initTextField:self.capField];
    [self initTextField:self.emailField];
    [self initTextField:self.phoneField];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) initTextField: (UITextField*) view{
    
    CALayer* bottomBorder = [CALayer layer];
    
    UIColor* bordersColor = [ColorUtil getRgbColorRed:150 green:150 blue:150 alpha:1];
    bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height-1.0f, view.frame.size.width, 1.0);
    bottomBorder.backgroundColor = bordersColor.CGColor;
    [view.layer addSublayer:bottomBorder];
    
    view.enabled = NO;
}

-(void)setCompany:(Company *)company{
    self.nameField.text = company.name;
    self.addressField.text = company.address.street;
    self.townField.text = company.address.town;
    self.provinceField.text = company.address.state;
    self.capField.text = company.address.zip;
    self.emailField.text = company.email;
    self.phoneField.text = company.phone;
    
    
}


@end
