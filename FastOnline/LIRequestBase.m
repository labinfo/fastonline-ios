//
//  LIRequestBase.m
//
//  Created by Laboratorio Informatico on 04/09/14.
//  Copyright (c) 2014 LabInfo. All rights reserved.
//

#import "LIRequestBase.h"
#import "AFNetworking.h"
#import "AppDataManager.h"

@implementation LIRequestBase {
    NSString *contentType;
    AFHTTPRequestOperationManager *manager;
    NSString* requestUrl;
    NSDictionary* requestParams;
}

- (id)init {
    return [self initWithContentType:@"application/json"];
}

- (id)initWithContentType:(NSString*)ct {
    self = [super init];
    if (self) {
        contentType = ct;
    }
    return self;
}

-(void)abortAllOperation{
    [manager.operationQueue cancelAllOperations];
    
}
- (void)post:(NSString*)url {
    [self post:url params:nil];
}
- (void)post:(NSString*)url params:(NSDictionary*)params{
    [self post:url params:nil HTTPheaders:nil jsonBody:false];
}

- (void)post:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody: (BOOL) json{
    manager = [AFHTTPRequestOperationManager manager];
    
    AFHTTPRequestSerializer * requestSerializer;
    if(!json)
        requestSerializer= [AFHTTPRequestSerializer serializer];
    else
        requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFHTTPResponseSerializer * responseSerializer;
    responseSerializer = [AFJSONResponseSerializer serializer];
    
    if ([contentType isEqualToString:@"application/json"] || [contentType isEqualToString:@"text/plain"]) {
        responseSerializer = [AFJSONResponseSerializer serializer];
    }
    else if ([contentType isEqualToString:@"text/html"]) {
        responseSerializer =[AFHTTPResponseSerializer serializer];
    }

    
    if(headers!=nil){
        for(NSString* key in headers){
            [requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
             }
        //[requestSerializer.HTTPRequestHeaders setValue:@"1eaeed85-38da-44ce-b0cd-5a7ed582e1c7" forKey:@"sessionid"];
    }
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:contentType, nil];
    manager.responseSerializer = responseSerializer;
    manager.requestSerializer = requestSerializer;
    
    
    //params = [NSDictionary dictionaryWithObjectsAndKeys:@"06/02/2014 00:00:00", @"version",nil];
    
    NSLog(@"Url %@",[self parseUrl:url]);
    NSLog(@"params %@",[self parseParams:params]);
    
    
    [manager POST:[self parseUrl:url]
       parameters:[self parseParams:params]
     
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              NSInteger error = [self validateResponse:responseObject];
              if (error) {
                  [self callbackValidateResponseError:error withObject:responseObject];
              }
              else {
                  if([responseObject objectForKey:@"sessionid"] != nil && [[AppDataManager get] getSID].length <= 0)
                      [[AppDataManager get] setSID: [responseObject objectForKey:@"sessionid"]];
                      
                  [self callbackSuccess:[responseObject objectForKey:@"data"]];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              [self callbackFailure: operation.responseObject];
              NSLog(@"Errore LIREQUEST %@",error);
          }];
}

- (void)get:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody:(BOOL) json{
    [self prepareRequest:url params:params HTTPheaders:headers jsonBody:json];
    [self doGet];
}

- (void)put:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody:(BOOL) json{
    [self prepareRequest:url params:params HTTPheaders:headers jsonBody:json];
    [self doPut];
}

-(void) prepareRequest:(NSString*)url params:(NSDictionary*)params HTTPheaders:(NSDictionary*) headers jsonBody: (BOOL) json{
   
    requestParams=params;
    requestUrl=url;
    
    manager = [AFHTTPRequestOperationManager manager];
    
    
    AFHTTPRequestSerializer * requestSerializer;
    
    if(!json)
        requestSerializer= [AFHTTPRequestSerializer serializer];
    else
        requestSerializer = [AFJSONRequestSerializer serializer];
    
    AFHTTPResponseSerializer * responseSerializer;
    responseSerializer = [AFJSONResponseSerializer serializer];
    
    if ([contentType isEqualToString:@"application/json"] || [contentType isEqualToString:@"text/plain"]) {
        responseSerializer = [AFJSONResponseSerializer serializer];
    }
    else if ([contentType isEqualToString:@"text/html"]) {
        responseSerializer =[AFHTTPResponseSerializer serializer];
    }
    
    if(headers!=nil){
        for(NSString* key in headers){
            [requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
        //[requestSerializer.HTTPRequestHeaders setValue:@"1eaeed85-38da-44ce-b0cd-5a7ed582e1c7" forKey:@"sessionid"];
    }
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:contentType, nil];
    manager.responseSerializer = responseSerializer;
    manager.requestSerializer = requestSerializer;
}

-(void) doPost{
    
    [manager POST:[self parseUrl:requestUrl]
       parameters:[self parseParams:requestParams]
     
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              NSInteger error = [self validateResponse:responseObject];
              if (error) {
                  [self callbackValidateResponseError:error withObject:responseObject];
              }
              else {
                  if([responseObject objectForKey:@"sessionid"] != nil && [[AppDataManager get] getSID].length <= 0)
                      [[AppDataManager get] setSID: [responseObject objectForKey:@"sessionid"]];
                  
                  [self callbackSuccess:[responseObject objectForKey:@"data"]];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              [self callbackFailure: operation.responseObject];
              NSLog(@"Errore LIREQUEST %@",error);
          }];
}
-(void) doGet{
    
    [manager GET:[self parseUrl:requestUrl]
       parameters:[self parseParams:requestParams]
     
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              NSInteger error = [self validateResponse:responseObject];
              if (error) {
                  [self callbackValidateResponseError:error withObject:responseObject];
              }
              else {
                  [self callbackSuccess:[responseObject objectForKey:@"data"]];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              [self callbackFailure: operation.responseObject];
              NSLog(@"Errore LIREQUEST %@",error);
          }];
}

-(void) doPut{
    [manager PUT:[self parseUrl:requestUrl]
      parameters:[self parseParams:requestParams]
     
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             NSInteger error = [self validateResponse:responseObject];
             if (error) {
                 [self callbackValidateResponseError:error withObject:responseObject];
             }
             else {
                 [self callbackSuccess:[responseObject objectForKey:@"data"]];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             [self callbackFailure: operation.responseObject];
             NSLog(@"Errore LIREQUEST %@",error);
         }];
}

-(void)caricaDaLocale{
    [self callbackSuccess:[self jsonDatiLocali]];
}

- (NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileLocation stringByDeletingPathExtension] ofType:[fileLocation pathExtension]];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    // Be careful here. You add this as a category to NSDictionary
    // but you get an id back, which means that result
    // might be an NSArray as well!
    if (error != nil) return nil;
    return result;
}

- (NSDictionary*)jsonDatiLocali{
    return [self dictionaryWithContentsOfJSONString:@"dati.json"];
}

- (NSString*)parseUrl:(NSString*)url {
    return url;
}

- (NSDictionary*)parseParams:(NSDictionary*)params {
    return params;
}

- (NSInteger)validateResponse:(id)response{
    return 0;
}

- (void)callbackFailure:(id)response {
    
}
- (void)callbackSuccess:(id)response {
    
}
- (void)callbackValidateResponseError:(NSInteger)error withObject:(id)responseObj {
    
}

@end
