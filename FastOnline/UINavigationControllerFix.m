//
//  UINavigationControllerFix.m
//  theananke
//
//  Created by Mirko Ravaioli on 25/10/13.
//  Copyright (c) 2013 Mirko Ravaioli. All rights reserved.
//

#import "UINavigationControllerFix.h"

@interface UINavigationControllerFix ()

@end

@implementation UINavigationControllerFix

-(BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}

@end
