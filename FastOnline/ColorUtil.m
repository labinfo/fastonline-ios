//
//  ColorUtil.m
//  BuonApp iPhone
//
//  Created by Mirko Ravaioli on 21/09/12.
//  Copyright (c) 2012 stefano giorgi. All rights reserved.
//

#import "ColorUtil.h"

@implementation ColorUtil

+(CGFloat)toCGFloat:(CGFloat)value {
    return value / 255.0f;
}

+(UIColor*)getRgbColorRed:(int)red green:(int)green blue:(int)blue alpha:(CGFloat)alpha {
    return [[UIColor alloc] initWithRed:[ColorUtil toCGFloat:red] green:[ColorUtil toCGFloat:green] blue:[ColorUtil toCGFloat:blue] alpha:alpha];
}

+ (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color {
    
    UIGraphicsBeginImageContext(image.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(context, 1, -1);
    CGContextTranslateCTM(context, 0, -area.size.height);
    
    CGContextSaveGState(context);
    CGContextClipToMask(context, area, image.CGImage);
    
    [color set];
    CGContextFillRect(context, area);
    
    CGContextRestoreGState(context);
    
    CGContextSetBlendMode(context, kCGBlendModeMultiply);
    
    CGContextDrawImage(context, area, image.CGImage);
    
    UIImage *colorizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return colorizedImage;
}
+ (void)colorizeImageView:(UIImageView *)imageView withColor:(UIColor *)color{
    imageView.image = [ColorUtil colorizeImage:imageView.image withColor:color];
}
@end
