//
//  CropperController.h
//  FastOnline
//
//  Created by Nico Sordoni on 10/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECropView.h"

@protocol CropperDelegate <NSObject>

-(void) viewCropped: (UIImage*) image forId: (int) imageId;

@end
@interface CropperController : UIViewController

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cropViewHeight;
@property (strong, nonatomic) IBOutlet PECropView *cropView;
@property (strong, nonatomic)  UIImage *image;
@property (nonatomic)  int rotationsCount;

@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *rotateButton;
@property (nonatomic)  int requestId;

@property (strong, nonatomic) id<CropperDelegate> cropDelegate;

-(IBAction)deleteCrop:(id)sender;
-(IBAction)confirmCrop:(id)sender;
-(IBAction)rotate:(id)sender;
@end
