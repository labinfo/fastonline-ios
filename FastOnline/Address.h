//
//  Address.h
//  MyFirstFlash
//
//  Created by Nico Sordoni on 14/01/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject<NSCoding>

@property (nonatomic,strong) NSString *street;
@property (nonatomic,strong) NSString *town;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *zip;

-(id)initWithStreet:(NSString*)street town:(NSString*) town state:(NSString*)state zip: (NSString*) zip;
-(id)initWithData: (id) data;

- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

@end
