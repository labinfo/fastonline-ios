//
//  PageMenuIcon.h
//  mff
//
//  Created by Nico Sordoni on 10/02/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageMenuIcon : UIView

@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (nonatomic) BOOL showBottomBorder;

-(void) setSelectedWithColor: (UIColor*) color;
-(void) deselect;
@end
