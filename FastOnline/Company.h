//
//  Company.h
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Address.h"

@interface Company : NSObject<NSCoding>

@property (nonatomic, strong, readonly) NSNumber *companyId;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *email;
@property (nonatomic, strong, readonly) NSString *phone;
@property (nonatomic, strong, readonly) NSString *picture;
@property (nonatomic, strong, readonly) NSString *vatNumber;
@property (nonatomic, strong, readonly) Address *address;

@property (nonatomic, readonly) BOOL isUserAdmin;
@property (nonatomic, readonly) BOOL isUserCollaborator;
@property (nonatomic, readonly) BOOL isUserManager;

-(id) initWithData:(id)dataObject;

- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;
@end
