//
//  ColorUtil.h
//  BuonApp iPhone
//
//  Created by Mirko Ravaioli on 21/09/12.
//  Copyright (c) 2012 stefano giorgi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorUtil : NSObject

+(CGFloat)toCGFloat:(CGFloat)value;
+(UIColor*)getRgbColorRed:(int)red green:(int)green blue:(int)blue alpha:(CGFloat)alpha;
+ (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color;
+ (void)colorizeImageView:(UIImageView *)imageView withColor:(UIColor *)color;
@end
