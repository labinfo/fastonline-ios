//
//  Validator.h
//  MyFirstFlash
//
//  Created by Nico Sordoni on 14/01/15.
//  Copyright (c) 2015 LabInfo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validator : NSObject
//Check if string contains at least one char
+(BOOL)stringNotEmpty: (NSString*) string;

//Check if string contains at least one not blank char
+(BOOL)stringNotBlank: (NSString*) string;

+(BOOL)isValidEmail: (NSString*) string;


@end
