//
//  UIKeyboardListener.m
//  FastOnline
//
//  Created by Nico Sordoni on 19/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "UIKeyboardListener.h"

@implementation UIKeyboardListener

-(id) init {
    self = [super init];
    
    if ( self ) {
        NSNotificationCenter		*center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(noticeShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
        [center addObserver:self selector:@selector(noticeHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    }
    
    return self;
}

-(void) noticeShowKeyboard:(NSNotification *)inNotification {
    self->_isKeyboardVisible = true;
}

-(void) noticeHideKeyboard:(NSNotification *)inNotification {
    self->_isKeyboardVisible = false;
}
@end
