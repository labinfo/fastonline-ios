//
//  User.h
//  FastOnline
//
//  Created by Nico Sordoni on 16/02/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Address.h"
#import "Company.h"

@interface User : NSObject<NSCoding>

@property (nonatomic, readonly) BOOL admin;
@property (nonatomic, strong, readonly) NSString *id;
@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *surname;
@property (nonatomic, strong, readonly) NSString *email;
@property (nonatomic, strong, readonly) NSString *phone;
@property (nonatomic, strong, readonly) NSString *picture;
@property (nonatomic, strong, readonly) NSDate *lastLogin;
@property (nonatomic, strong, readonly) Address *address;
@property (nonatomic, strong, readonly) NSArray *companies;

-(id) initWithData:(id)data;

- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;

-(void) addCompanies:(id) data;
@end

