//
//  SSUDetail.m
//  FastOnline
//
//  Created by Nico Sordoni on 07/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "SSUDetail.h"
#import "LocalizationSystem.h"

@implementation SSUDetail


-(id)initWithID:(NSString*)localID{
    
    self =  [super init];
    if (self) {
        self.localID = localID;
        self.serverID = nil;
        self.status = STATUS_DRAFT;
        
    }
    return self;
}
-(void)setCarData:(CarData *)data{
    self.title = data.plateNumber ? data.plateNumber : AMLocalizedString(@"Unknown", @"");
    
    self.carMakerName = data.maker ? data.maker.name : AMLocalizedString(@"Unknown", @"");
    self.carModelName = data.model ? data.model.name : AMLocalizedString(@"Unknown", @"");
    self.carVersionName = data.version ? data.version.name : AMLocalizedString(@"Unknown", @"");
    
}
-(id)initWithCoder:(NSCoder *)decoder{
    
    self =  [super init];
    if (self) {
        self->_localID = [decoder decodeObjectForKey:@"id"];
        if([[self->_localID class] isSubclassOfClass:[NSNumber class]]){
            self->_localID = [NSString stringWithFormat:@"%d", [self->_localID intValue]];
        }
        
        self->_companyID = [decoder decodeObjectForKey:@"companyId"];
        self->_serverID = [decoder decodeObjectForKey:@"serverId"];
        self->_completionLevel = [decoder decodeIntForKey:@"completion"];
        self->_title = [decoder decodeObjectForKey:@"title"];
        self->_carMakerName = [decoder decodeObjectForKey:@"maker"];
        self->_carModelName = [decoder decodeObjectForKey:@"model"];
        self->_carVersionName = [decoder decodeObjectForKey:@"version"];
        
        BOOL synchronized  =[decoder decodeBoolForKey:@"sync"];
        if(synchronized){
            self->_status = STATUS_SENT;
        }
        else{
            BOOL completed  = [decoder decodeBoolForKey:@"completed"];
            self->_status = completed ? STATUS_COMPLETED : STATUS_DRAFT;
            

        }
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    
    [encoder encodeObject:self->_localID forKey:@"id"];
    [encoder encodeObject:self->_companyID forKey:@"companyId"];
    [encoder encodeObject:self->_serverID forKey:@"serverId"];
    [encoder encodeObject:self->_title forKey:@"title"];
    [encoder encodeObject:self->_carMakerName forKey:@"maker"];
    [encoder encodeObject:self->_carModelName forKey:@"model"];
    [encoder encodeObject:self->_carVersionName forKey:@"version"];
    
    [encoder encodeInt:self->_completionLevel forKey:@"completion"];
    
    if(self->_status == STATUS_SENT)
        [encoder encodeBool:YES forKey:@"sync"];
    else{
        
        [encoder encodeBool:FALSE forKey:@"sync"];
        
        BOOL completed = self->_status == STATUS_COMPLETED;
        [encoder encodeBool:completed forKey:@"completed"];
        
    }

}

@end


















