//
//  AddOptionalAlertController.h
//  FastOnline
//
//  Created by Nico Sordoni on 02/03/15.
//  Copyright (c) 2015 Joins-Us. All rights reserved.
//

#import "TrasparentebaseController.h"

@protocol AddOptionalDelegate <NSObject>

@required
-(void) optionalSelected: (NSString*) name;

@end

@interface AddOptionalAlertController : TrasparentebaseController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) id<AddOptionalDelegate> optionalDelegate;

-(IBAction)saveButtonClicked:(id)sender;

@end
